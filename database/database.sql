/*
SQLyog Professional v12.09 (64 bit)
MySQL - 10.1.34-MariaDB : Database - f88fake
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0: hien; 1: an',
  `createdate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT '0',
  `updatedate` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

/*Table structure for table `collaterals` */

DROP TABLE IF EXISTS `collaterals`;

CREATE TABLE `collaterals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0:hien;1:an',
  `createdate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT '0',
  `updatedate` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

/*Table structure for table `configs` */

DROP TABLE IF EXISTS `configs`;

CREATE TABLE `configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL COMMENT 'ten chu so huu',
  `logo` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `about` text COLLATE utf8_vietnamese_ci COMMENT 'gioi thieu',
  `createdate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT '0',
  `updatedate` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

/*Table structure for table `loans` */

DROP TABLE IF EXISTS `loans`;

CREATE TABLE `loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loan_money` float DEFAULT NULL COMMENT 'vay bao nhieu (so tien)',
  `loan_long` int(11) DEFAULT NULL COMMENT 'vay bao lau (so ngay)',
  `collateral_id` int(11) DEFAULT NULL COMMENT 'tai san the chap',
  `collateral_trademark` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL COMMENT 'thuong hieu tai san',
  `collateral_year` int(11) DEFAULT NULL COMMENT 'nam san xuat',
  `collateral_description` text COLLATE utf8_vietnamese_ci COMMENT 'mo ta tai san the chap',
  `customer_name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `customer_phone` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `customer_mail` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `sex` tinyint(1) DEFAULT '0' COMMENT '0:nu; 1:nam',
  `location_id` int(11) DEFAULT NULL COMMENT 'tinh thanh pho',
  `status` tinyint(1) DEFAULT '0' COMMENT '0:chua xu ly; 1:da xu ly',
  `createdate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT '0',
  `updatedate` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0:hien;1:an',
  `createdate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT '0',
  `updatedate` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `type` tinyint(1) DEFAULT '0' COMMENT '0:news;1:page',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `short_content` text COLLATE utf8_vietnamese_ci,
  `content` text COLLATE utf8_vietnamese_ci,
  `status` tinyint(1) DEFAULT '0' COMMENT '0:hien; 1:an',
  `createdate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT '0',
  `updatedate` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `fullname` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT '0',
  `updatedate` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
