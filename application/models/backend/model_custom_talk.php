<?php
class model_custom_talk extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }
    
	function get_all_custom_talk($page = 1){
		$page_size = default_pagesize;
        $offset = ($page - 1) * $page_size;
		$this->db->limit($page_size, $offset);	
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('custom_talk');
		$result = $query->result();
		return $result;
	}

	function count_all_custom_talk(){
		$query = $this->db->count_all_results('custom_talk');
        return $query;
	}
	
	function get_custom_talk($id){
		$this->db->where('id',$id);
		$query = $this->db->get('custom_talk');
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
	
	function save($data){
		if($data['id']) {
			return $this->update_custom_talk($data);
		} else {
			return $this->insert_custom_talk($data);
		}
	}
	
	function update_custom_talk($data){
		$this->db->where('id',$data['id']);
		$this->db->update('custom_talk',$data);
		return $data['id'];
	}
	
	function insert_custom_talk($data){
		$this->db->insert('custom_talk', $data);
		return $this->db->insert_id();
	}

	function delete_custom_talk($id){
		$this->db->where('id',$id);
		$this->db->delete('custom_talk');
		return true;
	}

	function save_resource($resource_data){
        $this->db->insert_batch('gallery', $resource_data);
    }

    function get_gallery($pid){
    	$this->db->where('parent_id', $pid);
		$query = $this->db->get('gallery');
		$result = $query->result();
		if (count($result)) {
			return $result;
		} else {
			return null;
		}
    }
}