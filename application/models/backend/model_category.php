<?php
class model_category extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }
    
	function get_all_category($page = 1){
		$page_size = default_pagesize;
        $offset = ($page - 1) * $page_size;
		$this->db->limit($page_size, $offset);	
		$this->db->order_by('id', 'desc');
		$query = $this->db->get("categories");
		$result = $query->result();
		return $result;
	}

	function count_all_category(){
		$query = $this->db->count_all_results("categories");
        return $query;
	}
	
	function get_category($id){
		$this->db->where('id',$id);
		$query = $this->db->get("categories");
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
	
	function save($data){
		if($data['id']) {
			return $this->update_category($data);
		} else {
			return $this->insert_category($data);
		}
	}
	
	function update_category($data){
		$this->db->where('id',$data['id']);
		$this->db->update("categories",$data);
		return $data['id'];
	}
	
	function insert_category($data){
		$this->db->insert("categories", $data);
		return $this->db->insert_id();
	}

	function delete_category($id){
		$this->db->where('id',$id);
		$this->db->delete("categories");
		return true;
	}
}