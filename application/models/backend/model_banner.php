<?php
class model_banner extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }
    
	function get_all_banner($page = 1){
		$page_size = default_pagesize;
        $offset = ($page - 1) * $page_size;
		$this->db->limit($page_size, $offset);	
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('banners');
		$result = $query->result();
		return $result;
	}

	function count_all_banner(){
		$query = $this->db->count_all_results('banners');
        return $query;
	}
	
	function get_banner($id){
		$this->db->where('id',$id);
		$query = $this->db->get('banners');
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
	
	function save($data){
		if($data['id']) {
			return $this->update_banner($data);
		} else {
			return $this->insert_banner($data);
		}
	}
	
	function update_banner($data){
		$this->db->where('id',$data['id']);
		$this->db->update('banners',$data);
		return $data['id'];
	}
	
	function insert_banner($data){
		$this->db->insert('banners', $data);
		return $this->db->insert_id();
	}

	function delete_banner($id){
		$this->db->where('id',$id);
		$this->db->delete('banners');
		return true;
	}

	function save_resource($resource_data){
        $this->db->insert_batch('gallery', $resource_data);
    }

    function get_gallery($pid){
    	$this->db->where('parent_id', $pid);
		$query = $this->db->get('gallery');
		$result = $query->result();
		if (count($result)) {
			return $result;
		} else {
			return null;
		}
    }
}