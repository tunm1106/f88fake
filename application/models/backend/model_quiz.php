<?php
class model_quiz extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }
    
	function get_all_quiz($page = 1){
		$page_size = default_pagesize;
        $offset = ($page - 1) * $page_size;
		$this->db->limit($page_size, $offset);	
		$this->db->order_by('id', 'desc');
		$query = $this->db->get("quiz");
		$result = $query->result();
		return $result;
	}

	function count_all_quiz(){
		$query = $this->db->count_all_results('quiz');
        return $query;
	}
	
	function get_quiz($id){
		$this->db->where('id',$id);
		$query = $this->db->get('quiz');
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
	
	function get_all_category(){
		$this->db->where('type',2);
		$this->db->where('status',0);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get("categories");
		$result = $query->result();
		return $result;
	}
	
	function save($data,$table){
		if($data['id']) {
			return $this->update_quiz($data,$table);
		} else {
			return $this->insert_quiz($data,$table);
		}
	}
	
	function update_quiz($data,$table){
		$this->db->where('id',$data['id']);
		$this->db->update($table,$data);
		return $data['id'];
	}
	
	function insert_quiz($data,$table){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function delete_quiz($id){
		$this->db->where('id',$id);
		$this->db->delete('quiz');
		return true;
	}
}