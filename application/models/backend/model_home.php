<?php
class model_home extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }

    function get_all_user(){
        $query = $this->db->get('fmc_user');
        $result = $query->result();
        return $result;
    }
    
    function save($data){
        if($this->check_exist($data)) {
            return false;
        }
        if($data['id']) {
            return $this->update($data);
        } else {
            return $this->insert($data);
        }
    }
    
    function check_exist($data) {
        $this->db->where('username', $data['username']);
        if($data['id']) {
            $this->db->where('id !=', $data['id']);
        }
        return ($this->db->count_all_results('users')>0);
    }
    
    function update($data){
        $this->db->where('id',$data['id']);
        $this->db->update('users',$data);
        return $data['id'];
    }
    
    function insert($data){
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }
    
    function check_login($username, $password) {
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $this->db->where('status', '1');
        $this->db->where('type', '1');
        $query = $this->db->get('fmc_user');
        $result = $query->result();
        if(!$result){
            return false;
        } else {
            $this->session->set_userdata($result[0]);
            return true;
        }
    }
    
    function get_user_by_username($username){
        $this->db->where('username', $username);
        $this->db->where('status', '1');
        $query = $this->db->get('fmc_user');
        $result = $query->result();
        if (count($result)) {
            return $result[0];          
        }
        return NULL;
    }
    
    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('users');
        return true;
    }

    function get_user_by_id($id = 0){
        $this->db->where('id',$id);
        $query = $this->db->get('users');
        $result = $query->result();
        if (count($result)) {
            return $result[0];
        }
        return NULL;
    }
}