<?php
class model_config extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }
	
	function get_config($id = 0){
		if($id){
			$this->db->where('id',$id);
		}
		$query = $this->db->get("config");
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
	
	function save($data){
		if($data['id']) {
			return $this->update($data);
		} else {
			return $this->insert($data);
		}
	}
	
	function update($data){
		$this->db->where('id',$data['id']);
		$this->db->update("config",$data);
		return $data['id'];
	}
	
	function insert($data){
		$this->db->insert("config", $data);
		return $this->db->insert_id();
	}

}