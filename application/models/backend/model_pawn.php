<?php
class model_pawn extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }
    
	function get_all_pawn($page = 1){
		$page_size = default_pagesize;
        $offset = ($page - 1) * $page_size;
        $this->db->select('l.*,c.name as collateral_name,p.title as province_name');
        $this->db->from('loans as l');
        $this->db->join('collaterals as c', 'c.id = l.collateral_id');
        $this->db->join('provinces as p', 'p.id = l.province_id');
		$this->db->limit($page_size, $offset);	
		$this->db->order_by('l.id', 'l.desc');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function count_all_pawn(){
		$query = $this->db->count_all_results('loans');
        return $query;
	}
	
	function get_pawn($id){
		$this->db->where('id',$id);
		$query = $this->db->get('loans');
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
	
	function save($data){
		if($data['id']) {
			return $this->update_pawn($data);
		} else {
			return $this->insert_pawn($data);
		}
	}
	
	function update_pawn($data){
		$this->db->where('id',$data['id']);
		$this->db->update('loans',$data);
		return $data['id'];
	}
	
	function insert_pawn($data){
		$this->db->insert('loans', $data);
		return $this->db->insert_id();
	}

	function delete_pawn($id){
		$this->db->where('id',$id);
		$this->db->delete('loans');
		return true;
	}
}