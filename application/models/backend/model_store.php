<?php
class model_store extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }
    
	function get_all_store($page = 1){
		$page_size = default_pagesize;
        $offset = ($page - 1) * $page_size;
		$this->db->limit($page_size, $offset);	
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('store');
		$result = $query->result();
		return $result;
	}

	function count_all_store(){
		$query = $this->db->count_all_results('store');
        return $query;
	}
	
	function get_store($id){
		$this->db->where('id',$id);
		$query = $this->db->get('store');
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
	
	function save($data){
		if($data['id']) {
			return $this->update_store($data);
		} else {
			return $this->insert_store($data);
		}
	}
	
	function update_store($data){
		$this->db->where('id',$data['id']);
		$this->db->update('store',$data);
		return $data['id'];
	}
	
	function insert_store($data){
		$this->db->insert('store', $data);
		return $this->db->insert_id();
	}

	function delete_store($id){
		$this->db->where('id',$id);
		$this->db->delete('store');
		return true;
	}
}