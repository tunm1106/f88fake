<?php
class model_news extends CI_Model{
 	
	function __construct(){
        parent::__construct();
    }
    
	function get_all_news($page = 1){
		$page_size = default_pagesize;
        $offset = ($page - 1) * $page_size;
		$this->db->limit($page_size, $offset);	
		$this->db->order_by('id', 'desc');
		$query = $this->db->get("news");
		$result = $query->result();
		return $result;
	}

	function count_all_news(){
		$query = $this->db->count_all_results('news');
        return $query;
	}
	
	function get_all_funfact($page = 1){
		$page_size = default_pagesize;
        $offset = ($page - 1) * $page_size;
		$this->db->select('a.*,b.name as cate_name')
				 ->from('news as a')
				 ->join('categories as b','a.category_id = b.id','left')
				 ->where('category_id IS NULL', null, false)
				 ->or_where('category_id =', 0)
				 ->order_by('a.id','DESC');
		$this->db->limit($page_size, $offset);	
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function count_all_funfact(){
		$this->db->where('category_id IS NULL', null, false);
		$this->db->where('category_id =', 0);
		$query = $this->db->count_all_results('news');
        return $query;
	}
	
	function get_news($id){
		$this->db->where('id',$id);
		$query = $this->db->get('news');
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}
	

	function get_category($id){
		$this->db->where('id',$id);
		$query = $this->db->get('categories');
		$result = $query->result();
		if (count($result)) {
			return $result[0];
		} else {
			return null;
		}
	}

	function get_all_category(){
		$this->db->where('status',0);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get("categories");
		$result = $query->result();
		return $result;
	}
	
	function save($data,$table){
		if($data['id']) {
			return $this->update_news($data,$table);
		} else {
			return $this->insert_news($data,$table);
		}
	}
	
	function update_news($data,$table){
		$this->db->where('id',$data['id']);
		$this->db->update($table,$data);
		return $data['id'];
	}
	
	function insert_news($data,$table){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function delete_news($id){
		$this->db->where('id',$id);
		$this->db->delete('news');
		return true;
	}

	function delete_category($id){
		$this->db->where('id',$id);
		$this->db->delete('categories');
		return true;
	}
}