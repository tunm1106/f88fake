<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
class model_home extends CI_Model{
	function __construct() {
		parent::__construct();
	}

    function get_config() {
        $query = $this->db->get("config");
        $result = $query->result();
        if (count($result)) {
            return $result[0];
        }
        return NULL;
    }

    function get_content_about(){
        $this->db->select('content');
        $this->db->where('slug', 'gioi-thieu');
        $this->db->where('status', 0);
        $this->db->where('type', 1);
        $query = $this->db->get("news");
        $result = $query->result();
        if (count($result)) {
            return $result[0]->content;
        }
        return NULL;
    }

    function get_content_huong_dan(){
        $this->db->select('content');
        $this->db->where('slug', 'huong-dan-cam-do');
        $this->db->where('status', 0);
        $this->db->where('type', 1);
        $query = $this->db->get("news");
        $result = $query->result();
        if (count($result)) {
            return $result[0]->content;
        }
        return NULL;
    }

    function get_banner(){
        $this->db->where('status', 0);
        $query = $this->db->get("banners");
        $result = $query->result();
        if (count($result)) {
            return $result;
        }
        return NULL;
    }

    function get_all_store(){
        $this->db->where('status', 0);
        $query = $this->db->get("store");
        $result = $query->result();
        if (count($result)) {
            return $result;
        }
        return NULL;
    }
	
    function get_custom_talk(){
        $this->db->where('status', 0);
        $query = $this->db->get("custom_talk");
        $result = $query->result();
        if (count($result)) {
            return $result;
        }
        return NULL;
    }

    function get_quiz_category(){
        $this->db->select('id,name,slug');
        $this->db->where('type', 2);
        $this->db->where('status', 0);
        $query = $this->db->get("categories");
        $result = $query->result();
        if (count($result)) {
            return $result;
        }
        return NULL;
    }

    function get_quiz_by_cate_id($cate_id){
        $this->db->select('id,title,content');
        $this->db->where('category_id',$cate_id);
        $this->db->where('status', 0);
        $query = $this->db->get("quiz");
        $result = $query->result();
        if (count($result)) {
            return $result;
        }
        return NULL;
    }
}
