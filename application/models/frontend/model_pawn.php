<?php
class model_pawn extends CI_Model{
 	function __construct(){
        parent::__construct();
    }
	function get_collaterals() {
    	$this->db->where('status',0);
    	$query = $this->db->get ("collaterals");
    	$result = $query->result();
        
		if (count($result)) {
			return $result;
		}
		return NULL;
    }

    function get_province(){
    	$this->db->select('code,title');
    	$this->db->where('status',0);
    	$query = $this->db->get ("provinces");
    	$result = $query->result();
		if (count($result)) {
			return $result;
		}
		return NULL;
    }
    
	function save_info($data){
		$this->db->insert('loans', $data);
		return $this->db->insert_id();
	}
}