<?php
class model_news extends CI_Model{
 	function __construct(){
        parent::__construct();
    }
	function get_all_news ($page = 0,$cate='') {
    	$page_size = default_pagesize;
    	$offset = ($page - 1) * $page_size;
        $this->db->select('n.*,c.slug as cate_slug');
        $this->db->from('news as n');
        $this->db->join('categories as c', 'c.id = n.category_id');
        if($cate){
            $this->db->where('c.slug',$cate);
        }
    	$this->db->where('n.status',0);
    	$this->db->order_by ('n.create_time' , 'desc');
    	$this->db->limit($page_size, $offset);
    	$query = $this->db->get ();
    	$result = $query->result();
        
		if (count($result)) {
			return $result;
		}
		return NULL;
    }
    
	function count_all_news($cate=''){
		$this->db->select('n.*,c.slug as cate_slug');
        $this->db->from('news as n');
        $this->db->join('categories as c', 'c.id = n.category_id');
        if($cate){
            $this->db->where('c.slug',$cate);
        }
        $this->db->where('n.status',0);
    	$query = $this->db->count_all_results();
    	return $query;
    }

    function get_news_by_slug($slug){
        $this->db->select('n.*,c.slug as cate_slug');
        $this->db->from('news as n');
        $this->db->join('categories as c', 'c.id = n.category_id');
    	$this->db->where('n.status',0);
    	$this->db->where('n.slug',$slug);
    	$query = $this->db->get();
    	$result = $query->result();
    	if (count($result)) {
    		return $result[0];
    	}
    	return NULL;
    }

    function get_list_category($page = 0){
        $this->db->where('status',0);
        $query = $this->db->get("categories");
        $result = $query->result();
        if (count($result)) {
            return $result;
        }
        return NULL;
    }

    function get_talk_to_us($limit = 25){
        $this->db->select('n.*,c.slug as cate_slug');
        $this->db->from('news as n');
        $this->db->join('categories as c', 'c.id = n.category_id');
        $this->db->where('n.status',0);
        $this->db->order_by ('n.create_time' , 'desc');
        $this->db->limit($limit);
        $query = $this->db->get("news");
        $result = $query->result();
        
        if (count($result)) {
            return $result;
        }
        return NULL;
    }
}