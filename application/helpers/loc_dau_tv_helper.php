<?php


	function loc_dau_tv($str){
	 	$marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă",
			"ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề"
			,"ế","ệ","ể","ễ",
			"ì","í","ị","ỉ","ĩ",
			"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
			,"ờ","ớ","ợ","ở","ỡ",
			"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
			"ỳ","ý","ỵ","ỷ","ỹ",
			"đ",
			"À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
			,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
			"È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
			"Ì","Í","Ị","Ỉ","Ĩ",
			"Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
			,"Ờ","Ớ","Ợ","Ở","Ỡ",
			"Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
			"Ỳ","Ý","Ỵ","Ỷ","Ỹ",
			"Đ");
			
			$marKoDau=array("a","a","a","a","a","a","a","a","a","a","a"
			,"a","a","a","a","a","a",
			"e","e","e","e","e","e","e","e","e","e","e",
			"i","i","i","i","i",
			"o","o","o","o","o","o","o","o","o","o","o","o"
			,"o","o","o","o","o",
			"u","u","u","u","u","u","u","u","u","u","u",
			"y","y","y","y","y",
			"d",
			"A","A","A","A","A","A","A","A","A","A","A","A"
			,"A","A","A","A","A",
			"E","E","E","E","E","E","E","E","E","E","E",
			"I","I","I","I","I",
			"O","O","O","O","O","O","O","O","O","O","O","O"
			,"O","O","O","O","O",
			"U","U","U","U","U","U","U","U","U","U","U",
			"Y","Y","Y","Y","Y",
			"D");
			return str_replace($marTViet,$marKoDau,$str);
	 }

	 function convert_horoscope($birthday = 0){
		// $birthday = "1991-04-11";
		$zodiac[356] = "10"; //Ma kết
		$zodiac[326] = "9";  //Nhân mã
		$zodiac[296] = "8";  //Bọ cạp
		$zodiac[266] = "7";  //Thiên bình
		$zodiac[235] = "6";  //Xử nữ
		$zodiac[203] = "5";  //Sư tử
		$zodiac[172] = "4";  //Cự giải
		$zodiac[141] = "3";  //Song tử
		$zodiac[110] = "2";  //Kim ngưu
		$zodiac[79]  = "1";  //Bạch Dương
		$zodiac[50]  = "12"; //Song ngư
		$zodiac[21]  = "11"; //Bảo bình
		$zodiac[0]   = "10"; //Ma kết

		$date = strtotime($birthday);
		$dayOfTheYear = date("z",$date);
		$isLeapYear = date("L",$date);
		if ($isLeapYear && ($dayOfTheYear > 59)){
			$dayOfTheYear = $dayOfTheYear - 1;
		} 
		foreach($zodiac as $day => $sign) 
			if ($dayOfTheYear >= $day) break;
		return $sign;
	}

	
	 
	function resize_image($image_name, $folder=''){
		
	 	preg_match ( '/\.([a-zA-Z]+?)$/', $image_name, $matches );
  		$ext = $matches [1];
		$source_pic = 'upload/'.$folder .'/'. $image_name; // file ảnh nguồn
		$destination_pic = 'upload/'.$folder.'/'. $image_name; // file ảnh mới
		$max_width = 200; // độ rộng tối đa
		$max_height = 200; // chiều cao tối đa

		// đọc file ảnh vào bộ nhớ
		if (! strcmp ( "jpg", $ext ) || ! strcmp ( "jpeg", $ext ) || ! strcmp ( "JPG", $ext ) || ! strcmp ( "JPEG", $ext ))
			$src = imagecreatefromjpeg ( $source_pic );
		
		if (! strcmp ( "png", $ext ) || ! strcmp ( "PNG", $ext ))
			$src = imagecreatefrompng ( $source_pic );
		
		if (! strcmp ( "gif", $ext ) || ! strcmp ( "GIF", $ext ))
			$src = imagecreatefromgif ( $source_pic );
			
		list($width,$height) = getimagesize($source_pic); // lấy kích thước ảnh
		
		$x_ratio = $max_width / $width; // tỷ lệ theo chiều rộng
		$y_ratio = $max_height / $height; // tỷ lệ thoe chiều cao
		
		//dựa vào tỷ lệ theo từng chiều mà tính ra kích thước phù hợp
		// để bảo đảm giữ nguyên tỷ lệ hình
		if( ($width <= $max_width) && ($height <= $max_height) ){
		$tn_width = $width;
		$tn_height = $height;
		}elseif (($x_ratio * $height) < $max_height){
		
		$tn_height = ceil($x_ratio * $height);
		$tn_width = $max_width;
		}else{
		$tn_width = ceil($y_ratio * $width);
		$tn_height = $max_height;
		}
		// tạo một hình mới trong bộ nhớ với kích thước đã tính
		$tmp=imagecreatetruecolor($tn_width,$tn_height);
		// copy ảnh nguồn sang ảnh đích
		imagecopyresampled($tmp,$src,0,0,0,0,$tn_width, $tn_height,$width,$height);
		// ghi ảnh mới tạo từ bộ nhớ ra server
		imagejpeg($tmp,$destination_pic,100);
		// Xong
		
	}
	
	function splitText($text, $maxLength){
		if(strlen($text) > $maxLength){
			$text = substr($text, 0, $maxLength - 1);
			$position=strripos($text,'.');
			if($position>0){
				$text = substr($text, 0, $position+1);
			}else{
				$position=strripos($text,' ');
				$text = substr($text, 0, $position);
			}
		}
		return $text;
	}

	function subWord($text, $maxLength = 40,$more = ' ...'){
		$arr_word = explode(' ',$text);
		if(count($arr_word) > $maxLength){
			$text = '';
			foreach ($arr_word as $key => $val) {
				if ($key < $maxLength) {
					$text .= $val.' ';
				} 
			}
			$text = rtrim($text, ' ,.;:');
			$text .= $more;
		}
		return $text;
	}
	
	function getStartAndEndDate($week, $year){
		$time = strtotime("1 January $year", time());
		$day = date('w', $time);
		$time += ((7*$week)+1-$day)*24*3600;
		$return[0] = date('Y-n-j', $time);
		$time += 6*24*3600;
		$return[1] = date('Y-n-j', $time);
		return $return;
	}
