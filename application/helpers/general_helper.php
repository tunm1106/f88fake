<?php
if ( ! function_exists('get_config_meta')){
    function get_config_meta($page,$meta_seo=array(),$data_short=array()){
        $CI =& get_instance();
        $CI->data['meta_seo'] = array();
        if($meta_seo!=null){
            foreach (json_decode($meta_seo) as $key => $value) {
                if($value==""||$value==null){
                    if(isset($CI->data['configs'][$page.'_meta_seo'])){
                        $CI->data['meta_seo'][$key] = check_is(json_decode($CI->data['configs'][$page.'_meta_seo']),$key);
                    }else{
                        if(isset($CI->data['configs']['general_meta_seo'])){
                            $CI->data['meta_seo'][$key] = check_is(json_decode($CI->data['configs']['general_meta_seo']),$key);
                        }else{
                            $CI->data['meta_seo'][$key] = "";
                        }
                    }
                }else{
                    $CI->data['meta_seo'][$key] = $value;
                }
            }
        }else{
            if(isset($CI->data['configs'][$page.'_meta_seo'])){
                foreach (json_decode($CI->data['configs'][$page.'_meta_seo']) as $key => $value) {
                    if($value==""||$value==null){
                        if(isset($CI->data['configs']['general_meta_seo'])){
                            $CI->data['meta_seo'][$key] = check_is(json_decode($CI->data['configs']['general_meta_seo']),$key);
                        }else{
                            $CI->data['meta_seo'][$key] = "";
                        }
                    }else{
                        $CI->data['meta_seo'][$key] = $value;
                    }
                }
            }else{
                if(isset($CI->data['configs']['general_meta_seo'])){
                    foreach (json_decode($CI->data['configs']['general_meta_seo']) as $key => $value) {
                        $CI->data['meta_seo'][$key] = $value;
                    }
                }else{
                    $CI->data['meta_seo'] = array();
                }
            }
        }
        $CI->data['meta_seo'] =  shortcode($CI->data['meta_seo'],$data_short);
    }
}
if ( ! function_exists('checkPermission')){
    function checkPermission($functionCurrent='',$controllerCurrent=''){
        $CI =& get_instance();
        $controllerCurrent = $controllerCurrent ? $controllerCurrent : ucfirst(strtolower(str_replace('_controller','',$CI->router->fetch_class())));
        $functionCurrent = $functionCurrent ? $functionCurrent : $CI->router->fetch_method();
        // check_is(check_is($CI->data,'userLogin'),'is_super_admin');
        $userSuperAdmin = check_is(check_is($CI->data,'userLogin'),'is_super_admin');
        $userPermission = json_decode(check_is(check_is($CI->data,'userLogin'),'permission'));
        if($userSuperAdmin){
            return true;
        }
        if(is_array(check_is($userPermission,$controllerCurrent))){
            if(in_array($functionCurrent,check_is($userPermission,$controllerCurrent))){
                return true;
            }else{ 
                return false;
            }
        }else{
            return false;
        }
    }
}
if ( ! function_exists('generate_private_key')){
    function generate_private_key($new_uid='') {
        $num = 6 - strlen($new_uid);
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $private_key = $new_uid;
        for ($i = 0; $i < $num; $i++) {$private_key.=$characters[rand(0, $charactersLength - 1)];
        }
        return $private_key;
    }
}
if ( ! function_exists('array_random')){
    function array_random($arr, $num = 1) {
        shuffle($arr);
        $r = array();
        for ($i = 0; $i < $num; $i++) {
            $r[] = $arr[$i];
        }
        return $num == 1 ? $r[0] : $r;
    }
}
if ( ! function_exists('remvoveAllInput')){
    function remvoveAllInput(){
        $bots = array("Indy", "Blaiz", "Java", "libwww-perl", "Python", "OutfoxBot", "User-Agent", "PycURL", "AlphaServer", "T8Abot", "Syntryx", "WinHttp", "WebBandit", "nicebot", "Teoma", "alexa", "froogle", "inktomi", "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory", "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot", "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz");
        foreach($bots as $bot){
            if(stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false)
                return false;
            if(empty($_SERVER['HTTP_USER_AGENT']) || $_SERVER['HTTP_USER_AGENT'] == " ")
                return false;
        }

        $CI =& get_instance();
        $input = $CI->input->post();
        if(!$input){
            $input = $CI->input->get();
        }
        if($input){
            if(is_array($input) || is_object($input)){
                foreach ($input as &$value) {
                    $value = removeDomNodes($value);
                }
            }else{
                $input = removeDomNodes($input);
            }
        }
        return $input;
    }
}
if ( ! function_exists('removeDomNodes')){
    function removeDomNodes($html, $xpathString= '//script'){
        return trim(preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html));
    }
}
if ( ! function_exists('get_dates_of_quarter')){
    /**
    * Compute the start and end date of some fixed o relative quarter in a specific year.
    * @param mixed $quarter  Integer from 1 to 4 or relative string value:
    *                        'this', 'current', 'previous', 'first' or 'last'.
    *                        'this' is equivalent to 'current'. Any other value
    *                        will be ignored and instead current quarter will be used.
    *                        Default value 'current'. Particulary, 'previous' value
    *                        only make sense with current year so if you use it with
    *                        other year like: get_dates_of_quarter('previous', 1990)
    *                        the year will be ignored and instead the current year
    *                        will be used.
    * @param int $year       Year of the quarter. Any wrong value will be ignored and
    *                        instead the current year will be used.
    *                        Default value null (current year).
    * @param string $format  String to format returned dates
    * @return array          Array with two elements (keys): start and end date.
    */
    function get_dates_of_quarter($quarter = 'current', $year = null, $format = 'd/m/Y'){
        if ( !is_int($year) ) {        
           $year = (new DateTime)->format('Y');
        }
        $current_quarter = ceil((new DateTime)->format('n') / 3);
        switch (  strtolower($quarter) ) {
        case 'this':
        case 'current':
           $quarter = ceil((new DateTime)->format('n') / 3);
           break;

        case 'previous':
           $year = (new DateTime)->format('Y');
           if ($current_quarter == 1) {
              $quarter = 4;
              $year--;
            } else {
              $quarter =  $current_quarter - 1;
            }
            break;

        case 'first':
            $quarter = 1;
            break;

        case 'last':
            $quarter = 4;
            break;

        default:
            $quarter = (!is_int($quarter) || $quarter < 1 || $quarter > 4) ? $current_quarter : $quarter;
            break;
        }
        if ( $quarter === 'this' ) {
            $quarter = ceil((new DateTime)->format('n') / 3);
        }
        $start = new DateTime($year.'-'.(3*$quarter-2).'-1 00:00:00');
        $end = new DateTime($year.'-'.(3*$quarter).'-'.($quarter == 1 || $quarter == 4 ? 31 : 30) .' 23:59:59');

        return array(
            'start' => $format ? $start->format($format) : $start,
            'end' => $format ? $end->format($format) : $end,
        );
    }
}

if ( ! function_exists('cvWeed')){
    function cvWeed($ar){
        $arR = array();
        foreach ($ar as $value) {
            $arDate = explode(' to ', $value);
            $d1 = date('d/m/Y',strtotime(explode(' ', $arDate[0])[0]));
            $d2 = date('d/m/Y',strtotime(explode(' ', $arDate[1])[0]));
            $arH = array();
            $arH['full'] = $value;
            $arH['from'] = $arDate[0];
            $arH['to'] = $arDate[1];
            $arH['from_s'] = explode(' ', $arDate[0])[0];
            $arH['to_s'] = explode(' ', $arDate[1])[0];
            $arH['title'] = $d1.' đến '.$d2;
            array_push($arR, $arH);
        }
        return $arR;
    }
}

if ( ! function_exists('rtWeekRange')){
    function rtWeekRange($date){
        $dateFrom = $date['date_from'];
        $dateTo = $date['date_to'];
        $mF = grk_Week_Range($dateFrom)[0];
        $sF = grk_Week_Range($dateFrom)[1];
        $arRt = array();
        $f = $dateFrom.' to ';
        if(datediff('d',$sF,$dateTo) < 1 && datediff('d',$sF,$dateTo) > 0){
            $f  = $f.$dateTo;
            array_push($arRt ,$f);
        }else{
            if(datediff('d',$dateFrom,$sF) >= 0){
                $f  = $f.date('Y-m-d 23:59:59',strtotime($sF));
                array_push($arRt ,$f);
            }
            while (datediff('d',$sF,$dateTo) >= 0) {
                $next = date('Y-m-d H:i:s',strtotime($sF.' +1 week'));
                $mF = grk_Week_Range($next)[0];
                $sF = grk_Week_Range($next)[1];
                if(datediff('d',$next,$dateTo) > 1){
                    $f = $mF.' to ';
                    if(datediff('d',$sF,$dateTo) < 1 && datediff('d',$sF,$dateTo) > 0){
                        $f  = $f.$dateTo;
                    }else{
                        $f  = $f.date('Y-m-d 23:59:59',strtotime($sF));
                    }
                    array_push($arRt ,$f);
                }
            }
        }
        return $arRt;
    }
}

if ( ! function_exists('grk_Week_Range')){
    function grk_Week_Range($DateString, $FirstDay=0){
        #   Valeur par défaut si vide
        if(empty($DateString) === TRUE){
            $DateString = date('Y-m-d H:i:s');
        }

        #   On va aller chercher le premier jour de la semaine qu'on veut
        $Days = array(
            0 => 'monday',
            1 => 'tuesday',
            2 => 'wednesday',
            3 => 'thursday',
            4 => 'friday',
            5 => 'saturday',
            6 => 'sunday'
        );

        #   On va définir pour de bon le premier jour de la semaine     
        $DT_Min = new DateTime('last '.(isset($Days[$FirstDay]) === TRUE ? $Days[$FirstDay] : $Days[6]).' '.$DateString);
        $DT_Max = clone($DT_Min);

        #   On renvoie les données
        return array(
            $DT_Min->format('Y-m-d H:i:s'),
            $DT_Max->modify('+6 days')->format('Y-m-d H:i:s')
        );
    }
}

if ( ! function_exists('DateToArray')){
    function DateToArray($dateTimeBegin, $dateTimeEnd){
        $dateToArray = DateUtils::GetDateListDateRange($dateTimeBegin, $dateTimeEnd);
        $total_weeks = array('1', '2', '3', '4','5','6');
        $week = array();
        foreach($total_weeks as $week_no):
            //intialize with null always but it is safer to initialize string when you output directly.  My Personal Preference
            $week[$week_no] = array('Sunday' => '','Monday' => '','Tuesday' => '','Wednesday' => '','Thursday' => '','Friday' => '','Saturday' => '' );
        endforeach;
        $current_week = 1;
        foreach ($dateToArray as $cdate):
            $day_of_week = date('l', strtotime( $cdate ) );
            if ($day_of_week == 'Sunday' and Date('j', strtotime($cdate)) !== '1'  ):
                $current_week ++;
            endif;
            $week[$current_week][ $day_of_week ] = date('j', strtotime( $cdate ) );
        endforeach;
        foreach($total_weeks as $week_no):
            if ($week_no > $current_week) unset ($week[$week_no]);
        endforeach;

        echo '<pre>';
        print_r($week);
        echo '</pre>';
        exit();
    }
}


if ( ! function_exists('check_is')){
    function check_is($data,$key,$data_return=''){
        if(is_object($data) && isset($data->$key)){
            return $data->$key==''?$data_return:$data->$key;
        }else if(is_array($data) && isset($data[$key])){
            return $data[$key]==''?$data_return:$data[$key];
        }
        return $data_return;
    }
}

if ( ! function_exists('cv_config')){
    function cv_config($array){
        $ar_return = null;
        if($array!=null){
            if(isset($array[0])){    
                foreach ($array as $key => $value) {  
                    $ar_return[$value->key] = $value->value;    
                }
            }else{    
                $ar_return[$array->key] = $array->value;
            }
        }
        return $ar_return;
    }
}
if ( ! function_exists('replace_array')){
    function replace_array(array $replace, $subject) { 
       return str_replace(array_keys($replace), array_values($replace), $subject);    
    } 
}

if ( ! function_exists('replace_content')){
    function replace_content($content,$replace=array()) {
        $replace1 = array('[icon-1]'=>'<i class="fa fa-check" style="color:#4267b2"></i>','[icon-2]'=>'<i class="fa fa-hand-o-right" style="color:#4267b2"></i>','[icon-3]'=>'<i class="fa fa-lightbulb-o" style="color:#4267b2"></i>','[icon-4]'=>'');
        if($replace){
            $replace = array_merge($replace1,$replace);
        }else{
            $replace = $replace1;
        }
        $content = str_replace(array_keys($replace), array_values($replace), $content);
        $content = str_replace('<p></p>','',$content);
        $content = str_replace('<p>&nbsp;</p>','',$content);
        return $content;
    } 
}

if ( ! function_exists('genQueryString')){
    function genQueryString($key='',$value='',$type='add'){
        $CI =& get_instance();
        $get = $CI->input->get();
        if($key){
            if($type!='add'){
                if(isset($get[$key])){
                    unset($get[$key]);
                }
            }else{
                $get[$key] = $value;
            }
        }
        if($key!='page'){
            if(isset($get['page'])){
                unset($get['page']);
            }
        }
        return current_url().'?'.http_build_query($get);
    }
}


if ( ! function_exists('getFieldTable')){
    function getFieldTable($table){
        $CI =& get_instance();
        if (($query = $CI->db->query('SHOW COLUMNS FROM '.$CI->db->protect_identifiers($table, TRUE, NULL, FALSE))) === FALSE){
            return FALSE;
        }
        $query = $query->result_object();
        $retval = array();
        for ($i = 0, $c = count($query); $i < $c; $i++){
            $retval[$i]         = new stdClass();
            $retval[$i]->name       = $query[$i]->Field;
            sscanf($query[$i]->Type, '%[a-z](%d)',$retval[$i]->type,$retval[$i]->max_length);
            $retval[$i]->default        = $query[$i]->Default;
            $retval[$i]->allow_null     = (int) ($query[$i]->Null === 'YES');
            $retval[$i]->primary_key    = (int) ($query[$i]->Key === 'PRI');
        }
        return $retval;
    }
}

if ( ! function_exists('checkFieldNull')){
    function checkFieldNull($field,$table){
        $fields = getFieldTable($table);
        if($fields){
            foreach ($fields as $key => $value) {
                if($value->name == $field){
                    return $value->allow_null;
                }
            }
        }
        return false;
    }
}

if ( ! function_exists('checkTableNamePrefix')){
    function checkTableNamePrefix($table){
        $CI =& get_instance();
        $_prefix = $CI->db->dbprefix;
        if(strpos($table, $_prefix) === false){
            $table = $_prefix.$table;
        }
        return $table;
    }
}

if ( ! function_exists('genTableAlias')){
    function genTableAlias($table){
        $alias = substr($table, 0, 6);
        if(strpos($table,'_') !== false){
            $arTable = explode('_',$table);
            if($arTable){
                foreach ($arTable as $key => $value) {
                    if($key>0){
                        $alias .= substr($value, 0, 2);
                    }
                }
            }
        }
        return $alias;
    }
}



if ( ! function_exists('rebuild_date')){
    function rebuild_date( $format, $time = 0 ){
        if ( ! $time ) $time = time();

        $lang = array();
        $lang['sun'] = 'CN';
        $lang['mon'] = 'T2';
        $lang['tue'] = 'T3';
        $lang['wed'] = 'T4';
        $lang['thu'] = 'T5';
        $lang['fri'] = 'T6';
        $lang['sat'] = 'T7';
        $lang['sunday'] = 'Chủ nhật';
        $lang['monday'] = 'Thứ hai';
        $lang['tuesday'] = 'Thứ ba';
        $lang['wednesday'] = 'Thứ tư';
        $lang['thursday'] = 'Thứ năm';
        $lang['friday'] = 'Thứ sáu';
        $lang['saturday'] = 'Thứ bảy';
        $lang['january'] = 'Tháng Một';
        $lang['february'] = 'Tháng Hai';
        $lang['march'] = 'Tháng Ba';
        $lang['april'] = 'Tháng Tư';
        $lang['may'] = 'Tháng Năm';
        $lang['june'] = 'Tháng Sáu';
        $lang['july'] = 'Tháng Bảy';
        $lang['august'] = 'Tháng Tám';
        $lang['september'] = 'Tháng Chín';
        $lang['october'] = 'Tháng Mười';
        $lang['november'] = 'Tháng M. một';
        $lang['december'] = 'Tháng M. hai';
        $lang['jan'] = 'T01';
        $lang['feb'] = 'T02';
        $lang['mar'] = 'T03';
        $lang['apr'] = 'T04';
        $lang['may2'] = 'T05';
        $lang['jun'] = 'T06';
        $lang['jul'] = 'T07';
        $lang['aug'] = 'T08';
        $lang['sep'] = 'T09';
        $lang['oct'] = 'T10';
        $lang['nov'] = 'T11';
        $lang['dec'] = 'T12';

        $format = str_replace( "r", "D, d M Y H:i:s O", $format );
        $format = str_replace( array( "D", "M" ), array( "[D]", "[M]" ), $format );
        $return = date( $format, $time );

        $replaces = array(
            '/\[Sun\](\W|$)/' => $lang['sun'] . "$1",
            '/\[Mon\](\W|$)/' => $lang['mon'] . "$1",
            '/\[Tue\](\W|$)/' => $lang['tue'] . "$1",
            '/\[Wed\](\W|$)/' => $lang['wed'] . "$1",
            '/\[Thu\](\W|$)/' => $lang['thu'] . "$1",
            '/\[Fri\](\W|$)/' => $lang['fri'] . "$1",
            '/\[Sat\](\W|$)/' => $lang['sat'] . "$1",
            '/\[Jan\](\W|$)/' => $lang['jan'] . "$1",
            '/\[Feb\](\W|$)/' => $lang['feb'] . "$1",
            '/\[Mar\](\W|$)/' => $lang['mar'] . "$1",
            '/\[Apr\](\W|$)/' => $lang['apr'] . "$1",
            '/\[May\](\W|$)/' => $lang['may2'] . "$1",
            '/\[Jun\](\W|$)/' => $lang['jun'] . "$1",
            '/\[Jul\](\W|$)/' => $lang['jul'] . "$1",
            '/\[Aug\](\W|$)/' => $lang['aug'] . "$1",
            '/\[Sep\](\W|$)/' => $lang['sep'] . "$1",
            '/\[Oct\](\W|$)/' => $lang['oct'] . "$1",
            '/\[Nov\](\W|$)/' => $lang['nov'] . "$1",
            '/\[Dec\](\W|$)/' => $lang['dec'] . "$1",
            '/Sunday(\W|$)/' => $lang['sunday'] . "$1",
            '/Monday(\W|$)/' => $lang['monday'] . "$1",
            '/Tuesday(\W|$)/' => $lang['tuesday'] . "$1",
            '/Wednesday(\W|$)/' => $lang['wednesday'] . "$1",
            '/Thursday(\W|$)/' => $lang['thursday'] . "$1",
            '/Friday(\W|$)/' => $lang['friday'] . "$1",
            '/Saturday(\W|$)/' => $lang['saturday'] . "$1",
            '/January(\W|$)/' => $lang['january'] . "$1",
            '/February(\W|$)/' => $lang['february'] . "$1",
            '/March(\W|$)/' => $lang['march'] . "$1",
            '/April(\W|$)/' => $lang['april'] . "$1",
            '/May(\W|$)/' => $lang['may'] . "$1",
            '/June(\W|$)/' => $lang['june'] . "$1",
            '/July(\W|$)/' => $lang['july'] . "$1",
            '/August(\W|$)/' => $lang['august'] . "$1",
            '/September(\W|$)/' => $lang['september'] . "$1",
            '/October(\W|$)/' => $lang['october'] . "$1",
            '/November(\W|$)/' => $lang['november'] . "$1",
            '/December(\W|$)/' => $lang['december'] . "$1" );

        return preg_replace( array_keys( $replaces ), array_values( $replaces ), $return );
    }
}

if ( ! function_exists('sub_word')){
    function sub_word($string, $count = 3, $ellipsis = '...'){
        $words = explode(' ', $string);
        if($words){
            if (count($words) > $count){
                @array_splice($words, $count);
                $string = implode(' ', $words);
                if (is_string($ellipsis)){
                    $string .= ' '.$ellipsis;
                }
                elseif ($ellipsis){
                    $string .= '&hellip;';
                }
            }
        }
        return $string;
    }
}
if ( ! function_exists('sub_string')){
    function sub_string($string="", $limit=20,$limitword1 = 20) { 
        $words = explode(' ', $string);
        if($words){
            if(count($words) == 1){
                return substr($string,'0',$limitword1-1).'...'; 
            }else{
                if(strlen($string) > $limit){
                    return substr($string,'0',$limit-1).'...'; 
                }else{
                    return $string;
                }
            }
        }else{
            return substr($string,'0',$limit).'...'; 
        }
    } 
}

if ( ! function_exists('order_value_array')){
    function order_value_array($array,$value_com,$possion=1){
        $possion = $possion - 1 >= 0 ? $possion - 1 : 0;
        if($array!=null){
            if(in_array($value_com, $array)) {
                array_diff($array,[$value_com]);
                array_splice($array,$possion,0,$value_com);
            }
        }
        return $array;
    }
}

if ( ! function_exists('var_export_min')){
    function var_export_min($var, $return = false) {
        if (is_array($var)) {
            $toImplode = array();
            foreach ($var as $key => $value) {
                $toImplode[] = var_export($key, true).'=>'.var_export_min($value, true);
            }
            $code = 'array('.implode(',', $toImplode).')';
            if ($return) return $code;
            else echo $code;
        } else {
            return var_export($var, $return);
        }
    }
}

if ( ! function_exists('get_language')){
    function get_language(){
        $CI =& get_instance();
        return $CI->general->get_language();
    }
}

if ( ! function_exists('get_size_image')){
    function get_size_image(){
        $CI =& get_instance();
        return $CI->general->get_size_image();
    }
}

if ( ! function_exists('get_size_banner')){
    function get_size_banner(){
        $CI =& get_instance();
        return $CI->general->get_size_banner();
    }
}

if ( ! function_exists('sub_link_string')){
    function sub_link_string($str='',$number_fist=10,$number_last=5,$center='...'){
        if(strlen($str)>($number_fist+$number_last+strlen($center))){
            return substr($str,0,$number_fist).$center.substr($str,-$number_last);
        }
        return $str;
    }
}

if ( ! function_exists('money_to_text')){
    function money_to_text($amount){
        if($amount <=0){return $textnumber="Tiền phải là số nguyên dương lớn hơn số 0";}
        $Text=array("không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín");
        $TextLuythua =array("","nghìn", "triệu", "tỷ", "ngàn tỷ", "triệu tỷ", "tỷ tỷ");
        $textnumber = "";
        $length = strlen($amount);
        for ($i = 0; $i < $length; $i++)
        $unread[$i] = 0;
        for ($i = 0; $i < $length; $i++){$so = substr($amount, $length - $i -1 , 1);if ( ($so == 0) && ($i % 3 == 0) && ($unread[$i] == 0)){    for ($j = $i+1 ; $j < $length ; $j ++){  $so1 = substr($amount,$length - $j -1, 1);  if ($so1 != 0)      break;    }                           if (intval(($j - $i )/3) > 0){  for ($k = $i ; $k <intval(($j-$i)/3)*3 + $i; $k++)      $unread[$k] =1;    }}}
        for ($i = 0; $i < $length; $i++){$so = substr($amount,$length - $i -1, 1);if ($unread[$i] ==1)continue;if ( ($i% 3 == 0) && ($i > 0))$textnumber = $TextLuythua[$i/3].','. $textnumber;     if ($i % 3 == 2 )$textnumber = 'trăm '. $textnumber;if ($i % 3 == 1)$textnumber = 'mươi ' . $textnumber;$textnumber = $Text[$so] ." ". $textnumber ;}
        $textnumber = str_replace("không mươi", "lẻ", $textnumber);
        $textnumber = str_replace("lẻ không", "", $textnumber);
        $textnumber = str_replace("mươi không", "mươi", $textnumber);
        $textnumber = str_replace("một mươi", "mười", $textnumber);
        $textnumber = str_replace("mươi năm", "mươi lăm", $textnumber);
        $textnumber = str_replace("mươi một", "mươi mốt", $textnumber);
        $textnumber = str_replace("mười năm", "mười lăm", $textnumber);
        return ucfirst(trim($textnumber)." đồng");
    }
}

if ( ! function_exists('datediff')){
    function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {
        /*
        $interval can be:
        yyyy - Number of full years
        q - Number of full quarters
        m - Number of full months
        y - Difference between day numbers(eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
        d - Number of full days
        w - Number of full weekdays
        ww - Number of full weeks
        h - Number of full hours
        n - Number of full minutes
        s - Number of full seconds (default)
        */
        if (!$using_timestamps) {$datefrom = strtotime($datefrom, 0);$dateto = strtotime($dateto, 0);}
        $difference = $dateto - $datefrom;
        switch($interval) {
            case 'yyyy': $years_difference = floor($difference / 31536000);if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {    $years_difference--;}if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {    $years_difference++;}$datediff = $years_difference;break;
            case "q":$quarters_difference = floor($difference / 8035200);while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {    $months_difference++;}$quarters_difference--;$datediff = $quarters_difference;break;
            case "m":$months_difference = floor($difference / 2678400);while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {    $months_difference++;}$months_difference--;$datediff = $months_difference;break;
            case 'y': $datediff = date("z", $dateto) - date("z", $datefrom);break;
            case "d":$datediff = floor($difference / 86400);break;
            case "w":$days_difference = floor($difference / 86400);$weeks_difference = floor($days_difference / 7);$first_day = date("w", $datefrom);$days_remainder = floor($days_difference % 7);$odd_days = $first_day + $days_remainder;if ($odd_days > 7) {    $days_remainder--;}if ($odd_days > 6) {    $days_remainder--;}$datediff = ($weeks_difference * 5) + $days_remainder;break;
            case "ww":$datediff = floor($difference / 604800);break;
            case "h":$datediff = floor($difference / 3600);break;
            case "n":$datediff = floor($difference / 60);break;
            default:$datediff = $difference;break;
        }    
        return $datediff;
    } 
}

if ( ! function_exists('runCommand')){
    function runCommand($cmd = "") { 
        if (substr(php_uname(), 0, 7) == "Windows"){ pclose(popen("start /B ". 'start cmd.exe @cmd /k "'.$cmd.'"', "r"));} 
        else { exec('start cmd.exe @cmd /k "'.$cmd.'"' . " > /dev/null &");} 
    }
}

if ( ! function_exists('rt_color_type_no')){
    function rt_color_type_no($type){
        switch ($type) {case 'Add':    return  '#ed5565';    break;case 'Update':    return  'orange';    break;case 'Delete':    return  'red';    break;}
    }
}

if ( ! function_exists('get_date_range_by_month')){
    function get_date_range_by_month($daterange=''){
        $date = '';
        if($daterange != ''){$year = date("Y");if(count(explode('/',$daterange))>1){    $month =  explode('/',$daterange);    $month = $month[0];}else{    $month =  $daterange;    $daterange = $daterange.'/'.$year;}$month = intval($month);$from_date = '01/'.$daterange;if( $month == 1 || $month == 3 || $month == 5 || $month == 7 || $month == 8 || $month == 10 || $month == 12 ){    $to_date = '31/'.$daterange;}else if( $month == 2 ){    if(date('L', strtotime( $year . '-01-01'))){  $to_date = '29/'.$daterange;    } else {  $to_date = '28/'. $daterange;    }}else{    $to_date = '30/'.$daterange;}$date = $from_date.' - '.$to_date;
        }else{$month = date('m')==1?12:(date('m')-1);$month = intval($month);$year = date("Y");$daterange = $month.'/'.$year;$from_date = '01/'.$daterange;if( $month == 1 || $month == 3 || $month == 5 || $month == 7 || $month == 8 || $month == 10 || $month == 12 ){    $to_date = '31/'.$daterange;}else if( $month == 2 ){    if(date('L', strtotime( $year . '-01-01'))){  $to_date = '29/'.$daterange;    } else {  $to_date = '28/'. $daterange;    }}else{    $to_date = '30/'.$daterange;}$date = $from_date.' - '.$to_date;
        }
        return $date;
    }
}

if ( ! function_exists('formatnumber')){
    function formatnumber($floatcurr){
        if(is_numeric($floatcurr)){return number_format($floatcurr,0,'','.');}
    }
}
if ( ! function_exists('formatinr')){
    function formatinr($input){
        $dec = "";
        $pos = strpos($input, ".");
        if ($pos === false){
        } else {$dec = substr(round(substr($input,$pos),2),1);$input = substr($input,0,$pos);
        }
        $num = substr($input,-3);
        $input = substr($input,0, -3);
        while(strlen($input) > 0)
        {$num = substr($input,-2).",".$num;$input = substr($input,0,-2);
        }
        return $num . $dec;
    }
}
if ( ! function_exists('get_privatekey')){
    function get_privatekey(){
        return substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)),0,3).substr(str_shuffle(str_repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)),0,3).substr(str_shuffle(str_repeat("0123456789", 5)),0,3);
    }
}
if ( ! function_exists('get_parent_code')){
    function get_parent_code($string,$more_string){
        $len_more = strlen($more_string);
        $length = strlen($string);
        if($length>$len_more){$string = substr($string,0,-$len_more).$more_string;
        }
        $return = array();
        for ($i=0; $i<$length; $i++) {$return[$i] = $string[$i];
        }
        rsort($return);
        return implode('', $return);
    }
}

if (!function_exists('set_notifi_type')) {
    function set_notifi_type($type=0){
        switch ($type) {case 'Add':    return  '<span class="label label-success"><i class="icon-plus"></i></span>';    break;case 'Update':    return  '<span class="label label-warning"><i class="icon-bell"></i></span>';    break;case 'Delete':    return  '<span class="label label-important"><i class="icon-bolt"></i></span>';    break;case 0: '<span class="label label-warning"><i class="icon-bell"></i></span>';    break;
        }
    }
}

if (!function_exists('set_notifi_time')) {
    function set_notifi_time($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => mb_strtolower(lang('year')),
            'm' => mb_strtolower(lang('month')),
            'w' => mb_strtolower(lang('week')),
            'd' => mb_strtolower(lang('day')),
            'h' => mb_strtolower(lang('hour')),
            'i' => mb_strtolower(lang('minute')),
            's' => mb_strtolower(lang('second')),
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $CI =& get_instance();
                $lang = 'en';
                if ($CI->session->userdata('lang')){
                    $lang = $CI->session->userdata('lang');
                }
                $more = $lang == 'en' ? 's' : '';
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? $more : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' '.mb_strtolower(lang('ago')) : mb_strtolower(lang('justnow'));
    }
}

if (!function_exists('resize_image')) {
    function resize_image($source_pic, $destination_pic){
        $image_name = end(explode('/',$source_pic));
        preg_match ( '/\.([a-zA-Z]+?)$/',$image_name,$matches);
        $ext = $matches [1];
        // $source_pic = 'media/'.$folder .'/'. $image_name; // file ảnh nguồn
        // $destination_pic = 'media/'.$folder.'/'. $image_name; // file ảnh mới
        $max_width = 200; // độ rộng tối đa
        $max_height = 200; // chiều cao tối đa    // đọc file ảnh vào bộ nhớ
        if (! strcmp ( "jpg", $ext ) || ! strcmp ( "jpeg", $ext ) || ! strcmp ( "JPG", $ext ) || ! strcmp ( "JPEG", $ext ))$src = imagecreatefromjpeg ( $source_pic );
        
        if (! strcmp ( "png", $ext ) || ! strcmp ( "PNG", $ext ))$src = @imagecreatefrompng ( $source_pic );
        
        if (! strcmp ( "gif", $ext ) || ! strcmp ( "GIF", $ext ))$src = @imagecreatefromgif ( $source_pic );
        list($width,$height) = getimagesize($source_pic); // lấy kích thước ảnh
        
        $x_ratio = $max_width / $width; // tỷ lệ theo chiều rộng
        $y_ratio = $max_height / $height; // tỷ lệ thoe chiều cao
        
        //dựa vào tỷ lệ theo từng chiều mà tính ra kích thước phù hợp
        // để bảo đảm giữ nguyên tỷ lệ hình
        if( ($width <= $max_width) && ($height <= $max_height) ){
        $tn_width = $width;
        $tn_height = $height;
        }elseif (($x_ratio * $height) < $max_height){
        
        $tn_height = ceil($x_ratio * $height);
        $tn_width = $max_width;
        }else{
        $tn_width = ceil($y_ratio * $width);
        $tn_height = $max_height;
        }
        // tạo một hình mới trong bộ nhớ với kích thước đã tính
        $tmp=imagecreatetruecolor($tn_width,$tn_height);
        // copy ảnh nguồn sang ảnh đích
        @imagecopyresampled($tmp,$src,0,0,0,0,$tn_width, $tn_height,$width,$height);
        // ghi ảnh mới tạo từ bộ nhớ ra server
        @imagejpeg($tmp,$destination_pic,100);
        // Xong 
    }
}

if (!function_exists('get_country')) {
    function get_country(){
        $CI =& get_instance();
        return $CI->general->get_country();
    }
}

if (!function_exists('get_country_value')) {
    function get_country_value($keyset){
        $CI =& get_instance();
        foreach ($CI->general->get_country() as $key => $value) {if($key == $keyset){    return $value;}
        }
        return '';
        // return $CI->general->get_country()[$keyset];
    }
}

if (!function_exists('urlRawDecode')) {
    function urlRawDecode($raw_url_encoded){
        # Hex conversion table
        $hex_table = array(
            0 => 0x00,
            1 => 0x01,
            2 => 0x02,
            3 => 0x03,
            4 => 0x04,
            5 => 0x05,
            6 => 0x06,
            7 => 0x07,
            8 => 0x08,
            9 => 0x09,
            "A"=> 0x0a,
            "B"=> 0x0b,
            "C"=> 0x0c,
            "D"=> 0x0d,
            "E"=> 0x0e,
            "F"=> 0x0f
        );
        # Fixin' latin character problem
        if(preg_match_all("/\%C3\%([A-Z0-9]{2})/i", $raw_url_encoded,$res)){
            $res = array_unique($res = $res[1]);
            $arr_unicoded = array();
            foreach($res as $key => $value){
                $arr_unicoded[] = chr(
                        (0xc0 | ($hex_table[substr($value,0,1)]<<4)) 
                       | (0x03 & $hex_table[substr($value,1,1)])
                );
                $res[$key] = "%C3%" . $value;
            }

            $raw_url_encoded = str_replace(
                $res,
                $arr_unicoded,
                $raw_url_encoded
            );
        }
        
        # Return decoded  raw url encoded data 
        return rawurldecode($raw_url_encoded);
    }
}

if (!function_exists('percent_price_convert')) {
    function percent_price_convert($price, $old_price){
        $price = str_replace(",", "", $price);
        $price_old = str_replace(",", "", $old_price);
        $percent = ($price_old-$price)/$price_old*100;  
        return "-".round($percent, 0, PHP_ROUND_HALF_DOWN)."%";
    }
}

if (!function_exists('loc_dau_tv')) {
    function loc_dau_tv($str){
        $marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ","ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ","ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ","ỳ","ý","ỵ","ỷ","ỹ","đ","À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ","È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ","Ì","Í","Ị","Ỉ","Ĩ","Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ","Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ","Ỳ","Ý","Ỵ","Ỷ","Ỹ","Đ");$marKoDau=array("a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","e","e","e","e","e","e","e","e","e","e","e","i","i","i","i","i","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","u","u","u","u","u","u","u","u","u","u","u","y","y","y","y","y","d","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","E","E","E","E","E","E","E","E","E","E","E","I","I","I","I","I","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","U","U","U","U","U","U","U","U","U","U","U","Y","Y","Y","Y","Y","D");return str_replace($marTViet,$marKoDau,$str);
    }
}

if (!function_exists('cv_price')) {
    function cv_price($price=''){
        $price = preg_replace("/[^0-9,.]/", "", $price);
        if($price==""){
            $price = 0;
        }
        return $price;
    }
}

if (!function_exists('check_url_images')) {
    function check_url_images($images_url){$CI =& get_instance();if($images_url=='' || $images_url==null || $images_url=='0'){    $images_url = $CI->config->base_url('media/images/default.png');}else{    if(strpos($images_url, "http://") !== false || strpos($images_url, "https://")!== false){  $images_url = $images_url;    }else{  $images_url = $CI->config->base_url($images_url);    }}return $images_url;
    }
}

if (!function_exists('gen_slug')) {
    function gen_slug($strno,$more=true){
        $arrSrc  = array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ","ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ","ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ","ỳ","ý","ỵ","ỷ","ỹ","đ","À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ","È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ","Ì","Í","Ị","Ỉ","Ĩ","Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ","Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ","Ỳ","Ý","Ỵ","Ỷ","Ỹ","Đ"," ","&","'",">","<","!",":","#",".","~","@","$","%","^","*","(",")",",","}","{","]","[",";","?","/","\\","\"","“","”","＆","％","〜","！","＃","（","）","。","？","`");
        $arrDest = array("a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","e","e","e","e","e","e","e","e","e","e","e","i","i","i","i","i","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","u","u","u","u","u","u","u","u","u","u","u","y","y","y","y","y","d","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","E","E","E","E","E","E","E","E","E","E","E","I","I","I","I","I","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","U","U","U","U","U","U","U","U","U","U","U","Y","Y","Y","Y","Y","D","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-");
        $_more = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)),0,3).substr(str_shuffle(str_repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)),0,3).substr(str_shuffle(str_repeat("0123456789", 5)),0,3);
        $_more = $more ? '-'.$_more : '';
        return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/','/[ -]+/','/^-|-$/'), array('','-',''), str_replace($arrSrc, $arrDest, $strno))).$_more;
    }
}


if (!function_exists('hhb_curl_exec2')) {
    function hhb_curl_exec2($ch, $url, &$returnHeaders = array(), &$returnCookies = array(), &$verboseDebugInfo = ""){
        $returnHeaders    = array();
        $returnCookies    = array();
        $verboseDebugInfo = "";
        if (!is_resource($ch) || get_resource_type($ch) !== 'curl') {
            throw new InvalidArgumentException('$ch must be a curl handle!');
        }
        if (!is_string($url)) {
            throw new InvalidArgumentException('$url must be a string!');
        }
        $verbosefileh = tmpfile();
        $verbosefile  = stream_get_meta_data($verbosefileh);
        $verbosefile  = $verbosefile['uri'];
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_STDERR, $verbosefileh);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $html             = hhb_curl_exec($ch, $url);
        $verboseDebugInfo = file_get_contents($verbosefile);
        curl_setopt($ch, CURLOPT_STDERR, NULL);
        fclose($verbosefileh);
        unset($verbosefile, $verbosefileh);
        $headers       = array();
        $crlf          = "\x0d\x0a";
        $thepos        = strpos($html, $crlf . $crlf, 0);
        $headersString = substr($html, 0, $thepos);
        $headerArr     = explode($crlf, $headersString);
        $returnHeaders = $headerArr;
        unset($headersString, $headerArr);
        $htmlBody = substr($html, $thepos + 4); //should work on utf8/ascii headers... utf32? not so sure..
        unset($html);
        //I REALLY HOPE THERE EXIST A BETTER WAY TO GET COOKIES.. good grief this looks ugly..
        //at least it's tested and seems to work perfectly...
        $grabCookieName = function($str)
        {
            $ret = "";
            $i   = 0;
            for ($i = 0; $i < strlen($str); ++$i) {
                if ($str[$i] === ' ') {
                    continue;
                }
                if ($str[$i] === '=') {
                    break;
                }
                $ret .= $str[$i];
            }
            return urldecode($ret);
        };
        foreach ($returnHeaders as $header) {
            //Set-Cookie: crlfcoookielol=crlf+is%0D%0A+and+newline+is+%0D%0A+and+semicolon+is%3B+and+not+sure+what+else
            /*Set-Cookie:ci_spill=a%3A4%3A%7Bs%3A10%3A%22session_id%22%3Bs%3A32%3A%22305d3d67b8016ca9661c3b032d4319df%22%3Bs%3A10%3A%22ip_address%22%3Bs%3A14%3A%2285.164.158.128%22%3Bs%3A10%3A%22user_agent%22%3Bs%3A109%3A%22Mozilla%2F5.0+%28Windows+NT+6.1%3B+WOW64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F43.0.2357.132+Safari%2F537.36%22%3Bs%3A13%3A%22last_activity%22%3Bi%3A1436874639%3B%7Dcab1dd09f4eca466660e8a767856d013; expires=Tue, 14-Jul-2015 13:50:39 GMT; path=/
            Set-Cookie: sessionToken=abc123; Expires=Wed, 09 Jun 2021 10:18:14 GMT;
            //Cookie names cannot contain any of the following '=,; \t\r\n\013\014'
            //
            */
            if (stripos($header, "Set-Cookie:") !== 0) {
                continue;
                /**/
            }
            $header = trim(substr($header, strlen("Set-Cookie:")));
            while (strlen($header) > 0) {
                $cookiename                 = $grabCookieName($header);
                $returnCookies[$cookiename] = '';
                $header                     = substr($header, strlen($cookiename) + 1); //also remove the = 
                if (strlen($header) < 1) {
                    break;
                }
                ;
                $thepos = strpos($header, ';');
                if ($thepos === false) { //last cookie in this Set-Cookie.
                    $returnCookies[$cookiename] = urldecode($header);
                    break;
                }
                $returnCookies[$cookiename] = urldecode(substr($header, 0, $thepos));
                $header                     = trim(substr($header, $thepos + 1)); //also remove the ;
            }
        }
        unset($header, $cookiename, $thepos);
        return $htmlBody;
    }
}

if (!function_exists('hhb_curl_exec')) {
    function hhb_curl_exec($ch, $url){
        static $hhb_curl_domainCache = "";
        //$hhb_curl_domainCache=&$this->hhb_curl_domainCache;
        //$ch=&$this->curlh;
        if (!is_resource($ch) || get_resource_type($ch) !== 'curl') {
            throw new InvalidArgumentException('$ch must be a curl handle!');
        }
        if (!is_string($url)) {
            throw new InvalidArgumentException('$url must be a string!');
        }

        $tmpvar = "";
        if (parse_url($url, PHP_URL_HOST) === null) {
            if (substr($url, 0, 1) !== '/') {
                $url = $hhb_curl_domainCache . '/' . $url;
            } else {
                $url = $hhb_curl_domainCache . $url;
            }
        }
        ;

        curl_setopt($ch, CURLOPT_URL, $url);
        $html = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception('Curl error (curl_errno=' . curl_errno($ch) . ') on url ' . var_export($url, true) . ': ' . curl_error($ch));
            // echo 'Curl error: ' . curl_error($ch);
        }
        if ($html === '' && 203 != ($tmpvar = curl_getinfo($ch, CURLINFO_HTTP_CODE)) /*203 is "success, but no output"..*/ ) {
            throw new Exception('Curl returned nothing for ' . var_export($url, true) . ' but HTTP_RESPONSE_CODE was ' . var_export($tmpvar, true));
        }
        ;
        //remember that curl (usually) auto-follows the "Location: " http redirects..
        $hhb_curl_domainCache = parse_url(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), PHP_URL_HOST);
        return $html;
    }
}

if (!function_exists('trans')) {
    function trans($id, $parameters = array(), $language = null) {
        $array_index = explode('.',$id);
        $result_text = '';
        $CI = get_instance();
        if(count($array_index)>1){
            $line = $array_index[0];
            $array_text = $CI->lang->line($line);
            if(!is_array($array_text)) {   
                return $array_text;
            }
            array_splice($array_index,0,1);
            foreach ( $array_index as $index) {    
                $result_text = isset($array_text[$index]) ? $array_text[$index] : $result_text;    
                $array_text = $result_text;
            }
        }else{
            $result_text = $CI->lang->line($array_index[0]);
        }
        if(!is_array($result_text)){
            if(is_array($parameters)){    
                foreach ($parameters as $value) {  
                    $value = str_replace('_',' ',$value);  
                    $value = ucwords($value); 
                    $result_text = sprintf($result_text, $value);
                }
            }else{    
                $parameters = str_replace('_',' ',$parameters);    
                $parameters = ucwords($parameters);    
                $result_text = sprintf($result_text, $parameters);
            }
        }
        return $result_text ? $result_text : $id;
    }
}

if (!function_exists('hyphenize')) {
    function hyphenize($string) {
        $dict = array(
            "I'm"      => "I am",
            "thier"    => "their",
        );
        return strtolower(
            preg_replace(
              array( '#[\\s-]+#', '#[^A-Za-z0-9\. -]+#' ),
              array( '-', '' ),
              cleanString(
                  str_replace( // preg_replace to support more complicated replacements
                      array_keys($dict),
                      array_values($dict),
                      urldecode($string)
                  )
              )
            )
        );
    }
}

if (!function_exists('cleanString')) {
    function cleanString($text) {
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'    =>   '', // Literally a single quote
            '/[“”«»„.,]/u'  =>   '', // Double quote
            '/ /'           =>   '', // nonbreaking space (equiv. to 0x160)
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }
}

if (!function_exists('get_redirect_url')) {
    function get_redirect_url($url){
        $redirect_url = null; 
        $url_parts = @parse_url($url);
        if (!$url_parts) return false;
        if (!isset($url_parts['host'])) return false; //can't process relative URLs
        if (!isset($url_parts['path'])) $url_parts['path'] = '/';
        $sock = fsockopen($url_parts['host'], (isset($url_parts['port']) ? (int)$url_parts['port'] : 80), $errno, $errstr, 30);
        if (!$sock) return false;
        $request = "HEAD " . $url_parts['path'] . (isset($url_parts['query']) ? '?'.$url_parts['query'] : '') . " HTTP/1.1\r\n"; 
        $request .= 'Host: ' . $url_parts['host'] . "\r\n"; 
        $request .= "User-Agent: Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30\r\n";
        $request .= "Connection: Close\r\n\r\n"; 
        fwrite($sock, $request);
        $response = '';
        while(!feof($sock)) $response .= fread($sock, 8192);
        fclose($sock);
        if (preg_match('/^Location: (.+?)$/m', $response, $matches)){if ( substr($matches[1], 0, 1) == "/" )    return $url_parts['scheme'] . "://" . $url_parts['host'] . trim($matches[1]);else    return trim($matches[1]);
        } else {return false;
        }
    }
}

if (!function_exists('get_all_redirects')) {
    function get_all_redirects($url){
        $redirects = array();
        while ($newurl = get_redirect_url($url)){if (in_array($newurl, $redirects)){    break;}$redirects[] = $newurl;$url = $newurl;
        }
        return $redirects;
    }
}

if (!function_exists('get_final_url')) {
    function get_final_url($url){
        $redirects = get_all_redirects($url);
        if (count($redirects)>0){return array_pop($redirects);
        } else {return $url;
        }
    }
}

if (!function_exists('build_query')) {
    function build_query($encode=true,$array = array()){
        $array = $array == null ? $_GET : array();
        $return = "";
        if($array!=null){foreach ($array as $key => $value) {    $value = $encode ? rawurlencode($value) : $value;    $return .= "&".$key."=".$value;}$str = substr($return, 1);return $str;
        }else{return $return;
        }
    }
}

if (!function_exists('_utf8_decode')) {
    function _utf8_decode($string){
        $tmp = $string;
        $count = 0;
        while (mb_detect_encoding($tmp)=="UTF-8"){$tmp = utf8_decode($tmp);$count++;
        }
        for ($i = 0; $i < $count-1 ; $i++){$string = utf8_decode($string);
        
        }
        return $string;
    }
}

if (!function_exists('xcopy')) {
    function xcopy($source, $dest, $permissions = 0755){
        if (is_link($source)) {return symlink(readlink($source), $dest);
        }
        if (is_file($source)) {return copy($source, $dest);
        }
        if (!is_dir($dest)) {mkdir($dest, $permissions);
        }
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {if ($entry == '.' || $entry == '..') {    continue;}xcopy("$source/$entry", "$dest/$entry", $permissions);
        }
        $dir->close();
        return true;
    }
}

if (!function_exists('deleteDirectory')) {
    function deleteDirectory($dir) {
        if (!file_exists($dir)) {return true;
        }
        if (!is_dir($dir)) {return unlink($dir);
        }
        foreach (scandir($dir) as $item) {if ($item == '.' || $item == '..') {    continue;}if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {    return false;}
        }
        return rmdir($dir);
    }
}

if (!function_exists('full_copy')) {
    function full_copy( $source, $target ) {
        if(file_exists($source)){if(file_exists($target)){    if (is_dir($source)) {  @mkdir($target);  $d = dir($source);  while ( FALSE !== ( $entry = $d->read() ) ) {      if ( $entry == '.' || $entry == '..' ) {          continue;      }      $Entry = $source . '/' . $entry;       if ( is_dir( $Entry ) ) {          full_copy( $Entry, $target . '/' . $entry );          continue;      }      copy( $Entry, $target . '/' . $entry );  }  $d->close();    }else {  copy($source,$target);    }}else{    echo('Không tồn tại thư mục dich'); exit();exit();}
        }else{
            echo('Không tồn tại thư mục copy'); exit();
        }
    }
}

if (!function_exists('formatcurrency')) {
    function formatcurrency($floatcurr, $curr = "VND",$returnname = true){
        $currencies['ARS'] = array(2,',','.');          //  Argentine Peso
        $currencies['AMD'] = array(2,'.',',');          //  Armenian Dram
        $currencies['AWG'] = array(2,'.',',');          //  Aruban Guilder
        $currencies['AUD'] = array(2,'.',' ');          //  Australian Dollar
        $currencies['BSD'] = array(2,'.',',');          //  Bahamian Dollar
        $currencies['BHD'] = array(3,'.',',');          //  Bahraini Dinar
        $currencies['BDT'] = array(2,'.',',');          //  Bangladesh, Taka
        $currencies['BZD'] = array(2,'.',',');          //  Belize Dollar
        $currencies['BMD'] = array(2,'.',',');          //  Bermudian Dollar
        $currencies['BOB'] = array(2,'.',',');          //  Bolivia, Boliviano
        $currencies['BAM'] = array(2,'.',',');          //  Bosnia and Herzegovina, Convertible Marks
        $currencies['BWP'] = array(2,'.',',');          //  Botswana, Pula
        $currencies['BRL'] = array(2,',','.');          //  Brazilian Real
        $currencies['BND'] = array(2,'.',',');          //  Brunei Dollar
        $currencies['CAD'] = array(2,'.',',');          //  Canadian Dollar
        $currencies['KYD'] = array(2,'.',',');          //  Cayman Islands Dollar
        $currencies['CLP'] = array(0,'','.');           //  Chilean Peso
        $currencies['CNY'] = array(2,'.',',');          //  China Yuan Renminbi
        $currencies['COP'] = array(2,',','.');          //  Colombian Peso
        $currencies['CRC'] = array(2,',','.');          //  Costa Rican Colon
        $currencies['HRK'] = array(2,',','.');          //  Croatian Kuna
        $currencies['CUC'] = array(2,'.',',');          //  Cuban Convertible Peso
        $currencies['CUP'] = array(2,'.',',');          //  Cuban Peso
        $currencies['CYP'] = array(2,'.',',');          //  Cyprus Pound
        $currencies['CZK'] = array(2,'.',',');          //  Czech Koruna
        $currencies['DKK'] = array(2,',','.');          //  Danish Krone
        $currencies['DOP'] = array(2,'.',',');          //  Dominican Peso
        $currencies['XCD'] = array(2,'.',',');          //  East Caribbean Dollar
        $currencies['EGP'] = array(2,'.',',');          //  Egyptian Pound
        $currencies['SVC'] = array(2,'.',',');          //  El Salvador Colon
        $currencies['ATS'] = array(2,',','.');          //  Euro
        $currencies['BEF'] = array(2,',','.');          //  Euro
        $currencies['DEM'] = array(2,',','.');          //  Euro
        $currencies['EEK'] = array(2,',','.');          //  Euro
        $currencies['ESP'] = array(2,',','.');          //  Euro
        $currencies['EUR'] = array(2,',','.');          //  Euro
        $currencies['FIM'] = array(2,',','.');          //  Euro
        $currencies['FRF'] = array(2,',','.');          //  Euro
        $currencies['GRD'] = array(2,',','.');          //  Euro
        $currencies['IEP'] = array(2,',','.');          //  Euro
        $currencies['ITL'] = array(2,',','.');          //  Euro
        $currencies['LUF'] = array(2,',','.');          //  Euro
        $currencies['NLG'] = array(2,',','.');          //  Euro
        $currencies['PTE'] = array(2,',','.');          //  Euro
        $currencies['GHC'] = array(2,'.',',');          //  Ghana, Cedi
        $currencies['GIP'] = array(2,'.',',');          //  Gibraltar Pound
        $currencies['GTQ'] = array(2,'.',',');          //  Guatemala, Quetzal
        $currencies['HNL'] = array(2,'.',',');          //  Honduras, Lempira
        $currencies['HKD'] = array(2,'.',',');          //  Hong Kong Dollar
        $currencies['HUF'] = array(0,'','.');           //  Hungary, Forint
        $currencies['ISK'] = array(0,'','.');           //  Iceland Krona
        $currencies['INR'] = array(2,'.',',');          //  Indian Rupee
        $currencies['IDR'] = array(2,',','.');          //  Indonesia, Rupiah
        $currencies['IRR'] = array(2,'.',',');          //  Iranian Rial
        $currencies['JMD'] = array(2,'.',',');          //  Jamaican Dollar
        $currencies['JPY'] = array(0,'',',');           //  Japan, Yen
        $currencies['JOD'] = array(3,'.',',');          //  Jordanian Dinar
        $currencies['KES'] = array(2,'.',',');          //  Kenyan Shilling
        $currencies['KWD'] = array(3,'.',',');          //  Kuwaiti Dinar
        $currencies['LVL'] = array(2,'.',',');          //  Latvian Lats
        $currencies['LBP'] = array(0,'',' ');           //  Lebanese Pound
        $currencies['LTL'] = array(2,',',' ');          //  Lithuanian Litas
        $currencies['MKD'] = array(2,'.',',');          //  Macedonia, Denar
        $currencies['MYR'] = array(2,'.',',');          //  Malaysian Ringgit
        $currencies['MTL'] = array(2,'.',',');          //  Maltese Lira
        $currencies['MUR'] = array(0,'',',');           //  Mauritius Rupee
        $currencies['MXN'] = array(2,'.',',');          //  Mexican Peso
        $currencies['MZM'] = array(2,',','.');          //  Mozambique Metical
        $currencies['NPR'] = array(2,'.',',');          //  Nepalese Rupee
        $currencies['ANG'] = array(2,'.',',');          //  Netherlands Antillian Guilder
        $currencies['ILS'] = array(2,'.',',');          //  New Israeli Shekel
        $currencies['TRY'] = array(2,'.',',');          //  New Turkish Lira
        $currencies['NZD'] = array(2,'.',',');          //  New Zealand Dollar
        $currencies['NOK'] = array(2,',','.');          //  Norwegian Krone
        $currencies['PKR'] = array(2,'.',',');          //  Pakistan Rupee
        $currencies['PEN'] = array(2,'.',',');          //  Peru, Nuevo Sol
        $currencies['UYU'] = array(2,',','.');          //  Peso Uruguayo
        $currencies['PHP'] = array(2,'.',',');          //  Philippine Peso
        $currencies['PLN'] = array(2,'.',' ');          //  Poland, Zloty
        $currencies['GBP'] = array(2,'.',',');          //  Pound Sterling
        $currencies['OMR'] = array(3,'.',',');          //  Rial Omani
        $currencies['RON'] = array(2,',','.');          //  Romania, New Leu
        $currencies['ROL'] = array(2,',','.');          //  Romania, Old Leu
        $currencies['RUB'] = array(2,',','.');          //  Russian Ruble
        $currencies['SAR'] = array(2,'.',',');          //  Saudi Riyal
        $currencies['SGD'] = array(2,'.',',');          //  Singapore Dollar
        $currencies['SKK'] = array(2,',',' ');          //  Slovak Koruna
        $currencies['SIT'] = array(2,',','.');          //  Slovenia, Tolar
        $currencies['ZAR'] = array(2,'.',' ');          //  South Africa, Rand
        $currencies['KRW'] = array(0,'',',');           //  South Korea, Won
        $currencies['SZL'] = array(2,'.',', ');         //  Swaziland, Lilangeni
        $currencies['SEK'] = array(2,',','.');          //  Swedish Krona
        $currencies['CHF'] = array(2,'.','\'');         //  Swiss Franc 
        $currencies['TZS'] = array(2,'.',',');          //  Tanzanian Shilling
        $currencies['THB'] = array(2,'.',',');          //  Thailand, Baht
        $currencies['TOP'] = array(2,'.',',');          //  Tonga, Paanga
        $currencies['AED'] = array(2,'.',',');          //  UAE Dirham
        $currencies['UAH'] = array(2,',',' ');          //  Ukraine, Hryvnia
        $currencies['USD'] = array(2,'.',',');          //  US Dollar
        $currencies['VUV'] = array(0,'',',');           //  Vanuatu, Vatu
        $currencies['VEF'] = array(2,',','.');          //  Venezuela Bolivares Fuertes
        $currencies['VEB'] = array(2,',','.');          //  Venezuela, Bolivar
        $currencies['VND'] = array(0,'','.');           //  Viet Nam, Dong
        $currencies['ZWD'] = array(2,'.',' ');          //  Zimbabwe Dollar    $returnname = $returnname ? ' ( '.$curr.' )' : '';
        if(is_numeric($floatcurr)){if ($curr == "INR"){        return formatinr($floatcurr).$returnname;} else {    return number_format($floatcurr,$currencies[$curr][0],$currencies[$curr][1],$currencies[$curr][2]).$returnname;}
        }
    }
}



