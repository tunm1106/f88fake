<?php
require_once(APPPATH.'libraries/ip_3g.php');
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


function check_telco(){
	$ipaddress = get_client_ip();
	if($ipaddress == "::1"){
		return '5';
	}
	//     	Check telco Viettel
	if(check_telco_viettel($ipaddress)){
		return '1';
	}
	 
	//     	Check telco Vinaphone
	$IP_3G_VINAPHONE = "/(^(10)(\\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$)|(^(113\\.185\\.)([1-9]|1[0-9]|2[0-9]|3[0-1])(\\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])))/";
	if(preg_match($IP_3G_VINAPHONE, $ipaddress, $matches, PREG_OFFSET_CAPTURE))
		return '2';
	 
	//     	Check telco MobiFone
	$list_ip = explode(',', IP_3G_MOBIFONE);
	foreach ($list_ip as $ip){
		$m_ip = explode('.', $ip);
		$m_ip = $m_ip[0].'.'.$m_ip[1];
		$ipaddress = explode('.', $ipaddress);
		$ipaddress = $ipaddress[0].'.'.$ipaddress[1];
		if($m_ip == $ipaddress){
			return '3';
		}
	}
	 
	//     	Check telco VietnamMobile
	$list_ip = explode(',', IP_3G_VNM);
	foreach ($list_ip as $ip){
		$m_ip = explode('.', $ip);
		$m_ip = $m_ip[0].'.'.$m_ip[1];
		$ipaddress = explode('.', $ipaddress);
		$ipaddress = $ipaddress[0].'.'.$ipaddress[1];
		if($m_ip == $ipaddress){
			return '4';
		}
	}
	return '5';
}

function check_telco_viettel($clientip){
	$iplist = array();
	
	$iplist["22.67.0.0"] = 16;
	$iplist["27.64.0.0"] = 16;
	$iplist["27.64.0.0"] = 16;
	$iplist["27.65.0.0"] = 16;
	$iplist["27.66.0.0"] = 16;
	$iplist["27.67.0.0"] = 16;
	$iplist["27.76.0.0"] = 16;
	$iplist["27.77.0.0"] = 16;
	$iplist["27.71.64.0"] = 18;
	$iplist["27.71.128.0"] = 17;
	/*$iplist["171.224.0.0"] = 16;
	$iplist["171.225.0.0"] = 16;
	$iplist["171.228.0.0"] = 16;
	$iplist["171.230.0.0"] = 16;
	$iplist["171.231.0.0"] = 16;
	$iplist["171.232.0.0"] = 16;
	$iplist["171.233.0.0"] = 16;
	$iplist["171.234.0.0"] = 16;
	$iplist["171.236.0.0"] = 16;
	$iplist["171.238.0.0"] = 16;
	$iplist["171.240.0.0"] = 16;
	$iplist["171.241.0.0"] = 16;
	$iplist["171.241.128.0"] = 17;
	$iplist["171.243.0.0"] = 16;
	$iplist["171.244.0.0"] = 16;
	$iplist["171.246.0.0"] = 16;
	$iplist["171.247.0.0"] = 16;
	$iplist["171.248.0.0"] = 16;
	$iplist["171.249.0.0"] = 16;
	$iplist["171.250.0.0"] = 16;
	$iplist["171.251.64.0"] = 18;
	$iplist["171.251.128.0"] = 17;
	$iplist["171.255.0.0"] = 16;
	$iplist["171.255.64.0"] = 18;
	$iplist["171.255.128.0"] = 17;*/

	$clientip16 = explode('.', $clientip);
	$clientip16 = $clientip16[0].'.'.$clientip16[1].'.0.0';
	$clientip1718 = explode('.', $clientip);
	$clientiplast = $clientip1718[3];
	$clientiplast = intval($clientiplast);
	$clientip1718 = $clientip1718[0].'.'.$clientip1718[1].'.'.$clientip1718[2].'.0';

	foreach ($iplist as $key=>$list){
		if($list == 16){
			if($clientip16 == $key){
				return true;
			}
		}
			
		if($list == 17){
			if($clientip1718 == $key){
				if($clientiplast > 0 && $clientiplast <= 128)
					return true;
			}
		}
			
		if($list == 18){
			if($clientip1718 == $key){
				if($clientiplast > 0 && $clientiplast <= 64)
					return true;
			}
		}
	}
	return false;
}

function get_age($birthdate = '0000-00-00') {
	if ($birthdate == '0000-00-00') return 'Unknown';

	$bits = explode('-', $birthdate);
	$age = date('Y') - $bits[0] - 1;
	return $age;
}