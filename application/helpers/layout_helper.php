<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('custom_url')){
	function custom_url($uri = ''){
		$CI =& get_instance();
		$possition = $CI->config->item('current_area') ? $CI->config->item('current_area'). '/' : '';
		$_template_dir = $CI->config->item($possition.'_template_dir') ? $CI->config->item($possition.'_template_dir'). '/' : '';
		return $CI->config->base_url().'themes/'.$possition.$_template_dir.$uri;
	}
}

if ( ! function_exists('css_url')){
	function css_url($uri = ''){
		$CI =& get_instance();
		$possition = $CI->config->item('current_area') ? $CI->config->item('current_area'). '/' : '';
		$_template_dir = $CI->config->item($possition.'_template_dir') ? $CI->config->item($possition.'_template_dir'). '/' : '';
		return $CI->config->base_url().'themes/'.$possition.$_template_dir.'css/'.$uri;
	}
}

if ( ! function_exists('js_url')){
	function js_url($uri = ''){
		$CI =& get_instance();
		$possition = $CI->config->item('current_area') ? $CI->config->item('current_area'). '/' : '';
		$_template_dir = $CI->config->item($possition.'_template_dir') ? $CI->config->item($possition.'_template_dir'). '/' : '';
		return $CI->config->base_url().'themes/'.$possition.$_template_dir.'js/'.$uri;
	}
}

if ( ! function_exists('my_url')){
	function my_url($uri = ''){
		$CI =& get_instance();
		$possition = $CI->config->item('current_area') ? $CI->config->item('current_area'). '/' : '';
		$_template_dir = $CI->config->item($possition.'_template_dir') ? $CI->config->item($possition.'_template_dir'). '/' : '';
		return $CI->config->base_url().'themes/'.$possition.$_template_dir.'my/'.$uri;
	}
}

if ( ! function_exists('plugins_url')){
	function plugins_url($uri = ''){
		$CI =& get_instance();
		$possition = $CI->config->item('current_area') ? $CI->config->item('current_area'). '/' : '';
		$_template_dir = $CI->config->item($possition.'_template_dir') ? $CI->config->item($possition.'_template_dir'). '/' : '';
		return $CI->config->base_url().'themes/'.$possition.$_template_dir.'plugins/'.$uri;
	}
}

if ( ! function_exists('img_url')){
	function img_url($uri = ''){
		$CI =& get_instance();
		$possition = $CI->config->item('current_area') ? $CI->config->item('current_area'). '/' : '';
		$_template_dir = $CI->config->item($possition.'_template_dir') ? $CI->config->item($possition.'_template_dir'). '/' : '';
		return $CI->config->base_url().'themes/'.$possition.$_template_dir.'img/'.$uri;
	}
}

if ( ! function_exists('backend_url')){
	function backend_url($uri = ''){
		$CI =& get_instance();
		return $CI->config->base_url().$CI->config->item('link_backend').'/'.$uri;
	}
}


if ( ! function_exists('general_theme_url')){
	function general_theme_url($uri = ''){
		$CI =& get_instance();
		$possition = $CI->config->item('current_area') ? $CI->config->item('current_area'). '/' : '';
		$_template_dir = $CI->config->item($possition.'_template_dir') ? $CI->config->item($possition.'_template_dir'). '/' : '';
		return $CI->config->base_url().'themes/general/'.$_template_dir.$uri;
	}
}

if ( ! function_exists('theme_image')){
	function theme_image(){
		$CI =& get_instance();
		return $CI->config->base_url().'media/themes/';
	}
}