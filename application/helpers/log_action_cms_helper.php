<?php
	function log_action_cms($action) {
		$ci = get_instance();
		$username = $ci->session->userdata('username');
		$data = array(
				'action' => $action,
				'date'   => date('Y-m-d H:i:s',time()),
				'username' => $username
		);
		
		$ci->load->database();
		$ci->db->insert('log_action_cms', $data);
	}