<?php
if(!function_exists('ckeditor'))
{
	function ckeditor($name, $content, $toolbar = 'standard', $language = 'vi', $width = 'auto', $height = 200)
	{
		$CI =& get_instance();
		global $ckeditor_loaded;
		$code = '';
		if(!$ckeditor_loaded)
		{
			$code.= '<script type="text/javascript" src="'.plugins_url('ckeditor/ckeditor.js').'"></script>';
			$ckeditor_loaded = true;
		}
 
		$code.= '<textarea name="'.$name.'" id="'.$name.'">'.htmlentities($content).'</textarea>';
		$code.= "<script type=\"text/javascript\">
				config  = {};
				config.entities_latin = false;
				config.language = '".$language."';
				config.width = '".$width."';
				config.height = '".$height."';
				config.filebrowserBrowseUrl 		= '".plugins_url('ckfinder2/ckfinder.html')."';
				config.filebrowserImageBrowseUrl 	= '".plugins_url('ckfinder2/ckfinder.html')."';
				";
 
		if($toolbar == 'basic')
		{
			$code.= "config.toolbarGroups = [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
			{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'links', groups: [ 'links' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
			{ name: 'insert', groups: [ 'insert' ] },
			{ name: 'tools', groups: [ 'tools' ] },
			{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
			{ name: 'styles', groups: [ 'styles' ] },
			{ name: 'about', groups: [ 'about' ] },
			{ name: 'forms', groups: [ 'forms' ] },
			{ name: 'colors', groups: [ 'colors' ] },
			{ name: 'others', groups: [ 'others' ] }
		];
		config.removeButtons = 'Checkbox,Radio,TextField,Form,Textarea,Select,Button,ImageButton,HiddenField,SelectAll,Replace,Find,Smiley,Iframe,PageBreak,Flash,ShowBlocks,Save,NewPage,Preview,Print,Templates,Underline,Subscript,Superscript,Language,BidiRtl,BidiLtr,CreateDiv,JustifyCenter,JustifyRight,FontSize,Font,TextColor,BGColor,Cut,Undo,Redo,Copy,Paste,PasteText,PasteFromWord,Scayt,Strike,RemoveFormat,Outdent,Indent,Blockquote,JustifyLeft,JustifyBlock,Image,Table,SpecialChar,HorizontalRule,Styles,Format,About';
		config.extraPlugins = 'autoembed,embedsemantic,image2,uploadimage,uploadfile,embedbase,autolink,clipboard,html5audio,html5video';";
		}
		elseif ($toolbar == 'standard')
		{
			$code.= "config.toolbarGroups = [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
			{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
			{ name: 'links', groups: [ 'links' ] },
			{ name: 'insert', groups: [ 'insert' ] },
			{ name: 'tools', groups: [ 'tools' ] },
			{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
			'/',
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
			{ name: 'styles', groups: [ 'styles' ] },
			{ name: 'about', groups: [ 'about' ] },
			{ name: 'forms', groups: [ 'forms' ] },
			{ name: 'colors', groups: [ 'colors' ] },
			{ name: 'others', groups: [ 'others' ] },
			{ name: 'video', groups: [ 'insert' ] },
		];
		config.removeButtons = 'Checkbox,Radio,TextField,Form,Textarea,Select,Button,ImageButton,HiddenField,SelectAll,Replace,Find,Iframe,PageBreak,ShowBlocks,Save,NewPage,Preview,Print,Templates,Subscript,Superscript,Language,BidiRtl,BidiLtr,PasteFromWord,Scayt,RemoveFormat,Outdent,JustifyBlock,About';
		config.extraPlugins = 'autoembed,embedsemantic,image2,uploadimage,uploadfile,embedbase,autolink,clipboard,html5audio,html5video';";
		}
 
		$code.= 'CKEDITOR.replace(\''.$name.'\', config);';
		$code.= '</script>';
 
		return $code;
	}
}
