<?php
	function get_form($type='text', $name, $value = array(''), $attr = '', $data = array()) {
			switch ($type){
				case 'checkbox':
					return "<input type='checkbox' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;
				case 'button':
					return "<input type='button' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";	
					break;	
				case 'radio':
					return "<input type='radio' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;		
				case 'file':
					$string_file = "";
					$string_file.= "<input type='file' name='".$name."[]' id='".$name."' value='".$value[0]."' ".$attr." /><br/>";
					$string_file .= "<span style='color:blue;font-style: italic;'>(Định dạng: pdf,doc,docx,xls,xlsx)</span><br/>";
					return $string_file;
					break;	
				case 'image':
					$string_img = "";
					$string_img .= "<input type='file' name='".$name."[]' style='' id='".$name."' ".$attr." /><br/>";
					$string_img .= "<span style='color:blue;font-style: italic;'>(Định dạng: jpg,png,gif,JPG,PNG,GIF)</span><br/>";
					if($value[0]){
						$string_img .= "<div style='display:block;margin-left: 1px;cursor: move;margin-top:5px;'>";
						$string_img .= "<img  width='300' src='".base_url().$value[0]."'>";
						$string_img .= "</div>";
					}
					return $string_img;
					break;	
				case 'image_product_project':
					$string_img = "";
					$string_img .= "<input type='file' name='".$name."[]' style='' id='".$name."' ".$attr." /><br/>";
					$string_img .= "<span style='color:blue;font-style: italic;'>(Định dạng: jpg,png,gif,JPG,PNG,GIF ) Kích thước : 460x300 </span><br/>";
					if($value[0]){
						$string_img .= "<div style='display:block;margin-left: 1px;cursor: move;margin-top:5px;'>";
						$string_img .= "<img  width='300' src='".base_url().$value[0]."'>";
						$string_img .= "</div>";
					}
					return $string_img;
					break;
				case 'image_category':
					$string_img = "";
					$string_img .= "<input type='file' name='".$name."[]' style='' id='".$name."' ".$attr." /><br/>";
					$string_img .= "<span style='color:blue;font-style: italic;'>(Định dạng: jpg,png,gif,JPG,PNG,GIF ) Kích thước : 300x200 </span><br/>";
					if($value[0]){
						$string_img .= "<div style='display:block;margin-left: 1px;cursor: move;margin-top:5px;'>";
						$string_img .= "<img  width='300' src='".base_url().$value[0]."'>";
						$string_img .= "</div>";
					}
					return $string_img;
					break;
				case 'image_gallery':
					$string_img = "";
					$string_img .= "<input type='file' name='".$name."[]' style='' id='".$name."' ".$attr." /><br/>";
					$string_img .= "<span style='color:blue;font-style: italic;'>(Định dạng: jpg,png,gif,JPG,PNG,GIF ) Kích thước : 620x500 </span><br/>";
					if($value[0]){
						$string_img .= "<div style='display:block;margin-left: 1px;cursor: move;margin-top:5px;'>";
						$string_img .= "<img  width='300' src='".base_url().$value[0]."'>";
						$string_img .= "</div>";
					}
					return $string_img;
					break;	
				case 'image_multiple':
					return "<input type='file' name='".$name."[]' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;					
				case 'reset':
					return "<input type='reset' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;		
				case 'submit':
					return "<input type='submit' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;	
				case 'text':
					return "<input type='text' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;
				case 'password':
					return "<input autocomplete = 'off' type='password' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";	
					break;
				case 'hidden':
					return "<input type='hidden' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;	
				case 'image':
					return "<input type='image' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;
				case 'old_password':
					return "<input autocomplete = 'off' type='password' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";	
					break;	
				case 're_password':
					return "<input type='password' name='".$name."' id='".$name."' value='".$value[0]."' ".$attr." />";
					break;			
					
				case 'textarea':
					return "<textarea name=" . $name . ">" . $value[0] . "</textarea>";
					break;
				case 'select':
					$str_select = "";
					$str_select.= "<select name='".$name."' id='".$name."' ".$attr." >";
					$str_select.= "<option value=''>-----------Select----------</option>";
					foreach($data as $key => $val){
						$str_select.= "<option value='" . $key . "' ". ($key == $value[0]? "selected = 'selected'" :"" ) .">" . $val . "</option>";
					}
					$str_select.= "</select>";
					return $str_select;
					break;
				
				case 'select_tree':
						$str_select = "";
						$str_select.= "<select name='".$name."' id='".$name."' ".$attr." >";
						$str_select.= "<option value=''>-----------Select----------</option>";
						
						foreach($data as $name => $childs){
							
							$str_select.= '<optgroup label="'.$name.'">';
							
							foreach($childs as $cate){
								$str_select.= "<option value='" . $cate['id'] . "' ". ($cate['id'] == $value[0]? "selected = 'selected'" :"" ) .">" . $cate['name'] . "</option>";
							}
							
							$str_select.= '</optgroup>';
						}
						
						$str_select.= "</select>";
						
						return $str_select;
						break;
						
				case 'small_editor':
					$path = realpath('./fckeditor/fckeditor.php');
					require_once($path);
					$FCKeditor = new FCKeditor($name);
					$FCKeditor->BasePath = base_url().'fckeditor/';
					$FCKeditor->Value = $value[0];
					$FCKeditor->Height = 200;
					$FCKeditor->Width = 700;
					$FCKeditor->ToolbarSet = 'Question';
					$FCKeditor->Create();
					break;
				case 'large_editor':
					$path = realpath('./fckeditor/fckeditor.php');
					require_once($path);
					$FCKeditor = new FCKeditor($name);
					$FCKeditor->BasePath = base_url().'fckeditor/';
					$FCKeditor->Value = $value[0];
					$FCKeditor->Height = 500;
					$FCKeditor->Width = 700;
					$FCKeditor->Create();
					break;		
				case 'date_time':
					if($value[0] == '')
						$value[0] = date('Y-m-d',time());
					$date_split = explode('-', date('Y-m-d',strtotime($value[0])));
					$str_date_time = "";
					$str_date_time .= "<table border='0' cellpadding='0' cellspacing='0'>";
					$str_date_time .= "<tr  valign='top'>";
					$str_date_time .= "<td>";
					$str_date_time .= "<select id='"."d_".$name."' name='"."d_".$name."' class='styledselect-day'>";
					for($k=1;$k<=31;$k++){
						$str_date_time.= "<option value='" . $k . "' ". ($k == $date_split[2]? "selected = 'selected'" :"" ) .">" . $k . "</option>";
					}
					$str_date_time .= "</select></td>";
					$str_date_time .= "<td><select id='"."m_".$name."' name='"."m_".$name."' class='styledselect-month'>";
					$str_date_time.= "<option value='1'". (1 == $date_split[1]? "selected = 'selected'" :"" ) .">Jan</option>";
					$str_date_time.= "<option value='2'". (2 == $date_split[1]? "selected = 'selected'" :"" ) .">Feb</option>";
					$str_date_time.= "<option value='3'". (3 == $date_split[1]? "selected = 'selected'" :"" ) .">Mar</option>";
					$str_date_time.= "<option value='4'". (4 == $date_split[1]? "selected = 'selected'" :"" ) .">Apr</option>";
					$str_date_time.= "<option value='5'". (5 == $date_split[1]? "selected = 'selected'" :"" ) .">May</option>";
					$str_date_time.= "<option value='6'". (6 == $date_split[1]? "selected = 'selected'" :"" ) .">Jun</option>";
					$str_date_time.= "<option value='7'". (7 == $date_split[1]? "selected = 'selected'" :"" ) .">Jul</option>";
					$str_date_time.= "<option value='8'". (8 == $date_split[1]? "selected = 'selected'" :"" ) .">Aug</option>";
					$str_date_time.= "<option value='9'". (9 == $date_split[1]? "selected = 'selected'" :"" ) .">Sep</option>";
					$str_date_time.= "<option value='10'". (10 == $date_split[1]? "selected = 'selected'" :"" ) .">Oct</option>";
					$str_date_time.= "<option value='11'". (11 == $date_split[1]? "selected = 'selected'" :"" ) .">Nov</option>";
					$str_date_time.= "<option value='12'". (12 == $date_split[1]? "selected = 'selected'" :"" ) .">Dec</option>";
					$str_date_time .= "</select></td>";
					$str_date_time .= "<td><select  id='"."y_".$name."' name='"."y_".$name."'  class='styledselect-year'>";
					$str_date_time .= "<option value=''>yyyy</option>";
					for($j=1900;$j<=date('Y');$j++){
						$str_date_time.= "<option value='" . $j . "' ". ($j == $date_split[0]? "selected = 'selected'" :"" ) .">" . $j . "</option>";
					}
					$str_date_time .= "</select></td>";
					$str_date_time .= "<td><a href='' id='"."date_pick_".$name."' name='"."date-pick_".$name."'><img src='".base_url()."images/forms/icon_calendar.jpg"."'   alt='' /></a></td>";
					$str_date_time .= "<td>";
					$str_date_time .= "</tr>";
					$str_date_time .= "</table>";
					return $str_date_time;
					break;	
				case 'time':
					if($value[0] == '')
						$value[0] = '08:30:00';
					$date_split = explode(':', $value[0]);
					$str_time = "";
					$str_time .= "<table border='0' cellpadding='0' cellspacing='0'>";
					$str_time .= "<tr  valign='top'>";
					$str_time .= "<td>";
					$str_time .= "<select id='"."h_".$name."' name='"."h_".$name."' class='styledselect-day'>";
					for($k=0;$k<=24;$k++){
						if($k<10)
							$k = '0'.$k;
						$str_time.= "<option value='" . $k . "' ". ($k == $date_split[0]? "selected = 'selected'" :"" ) .">" . $k . "</option>";
					}
					$str_time .= "</select></td>";
					$str_time .= "<td><div style='margin-top:5px;margin-right:5px;'>Giờ<div></td>";
					$str_time .= "<td><select id='"."p_".$name."' name='"."p_".$name."' class='styledselect-month'>";
					$str_time.= "<option value='0'". (0 == $date_split[1]? "selected = 'selected'" :"" ) .">00</option>";
					$str_time.= "<option value='15'". (15 == $date_split[1]? "selected = 'selected'" :"" ) .">15</option>";
					$str_time.= "<option value='30'". (30 == $date_split[1]? "selected = 'selected'" :"" ) .">30</option>";
					$str_time.= "<option value='45'". (45 == $date_split[1]? "selected = 'selected'" :"" ) .">45</option>";
					$str_time .= "</select></td>";
					$str_time .= "<td><div style='margin-top:5px;'>Phút<div></td>";
					$str_time .= "</tr>";
					$str_time .= "</table>";
					return $str_time;
					break;

				case 'datetime':
					if($value[0] == '')
						$value[0] = date('Y-m-d',time());
					$date_split = explode('-', date('Y-m-d',strtotime($value[0])));
					$str_date_time = "";
					$str_date_time .= "<table border='0' cellpadding='0' cellspacing='0'>";
					$str_date_time .= "<tr  valign='top'>";
					$str_date_time .= "<td>";
					$str_date_time .= "<select id='"."d_".$name."' name='"."d_".$name."' class='styledselect-day'>";
					for($k=1;$k<=31;$k++){
						$str_date_time.= "<option value='" . $k . "' ". ($k == $date_split[2]? "selected = 'selected'" :"" ) .">" . $k . "</option>";
					}
					$str_date_time .= "</select></td>";
					$str_date_time .= "<td><select id='"."m_".$name."' name='"."m_".$name."' class='styledselect-month'>";
					$str_date_time.= "<option value='1'". (1 == $date_split[1]? "selected = 'selected'" :"" ) .">Jan</option>";
					$str_date_time.= "<option value='2'". (2 == $date_split[1]? "selected = 'selected'" :"" ) .">Feb</option>";
					$str_date_time.= "<option value='3'". (3 == $date_split[1]? "selected = 'selected'" :"" ) .">Mar</option>";
					$str_date_time.= "<option value='4'". (4 == $date_split[1]? "selected = 'selected'" :"" ) .">Apr</option>";
					$str_date_time.= "<option value='5'". (5 == $date_split[1]? "selected = 'selected'" :"" ) .">May</option>";
					$str_date_time.= "<option value='6'". (6 == $date_split[1]? "selected = 'selected'" :"" ) .">Jun</option>";
					$str_date_time.= "<option value='7'". (7 == $date_split[1]? "selected = 'selected'" :"" ) .">Jul</option>";
					$str_date_time.= "<option value='8'". (8 == $date_split[1]? "selected = 'selected'" :"" ) .">Aug</option>";
					$str_date_time.= "<option value='9'". (9 == $date_split[1]? "selected = 'selected'" :"" ) .">Sep</option>";
					$str_date_time.= "<option value='10'". (10 == $date_split[1]? "selected = 'selected'" :"" ) .">Oct</option>";
					$str_date_time.= "<option value='11'". (11 == $date_split[1]? "selected = 'selected'" :"" ) .">Nov</option>";
					$str_date_time.= "<option value='12'". (12 == $date_split[1]? "selected = 'selected'" :"" ) .">Dec</option>";
					$str_date_time .= "</select></td>";
					$str_date_time .= "<td><select  id='"."y_".$name."' name='"."y_".$name."'  class='styledselect-year'>";
					$str_date_time .= "<option value=''>yyyy</option>";
					for($j=1900;$j<=date('Y');$j++){
						$str_date_time.= "<option value='" . $j . "' ". ($j == $date_split[0]? "selected = 'selected'" :"" ) .">" . $j . "</option>";
					}
					$str_date_time .= "</select></td>";
					$str_date_time .= "<td><a href='' id='"."date_pick_".$name."' name='"."date-pick_".$name."'><img src='".base_url()."images/forms/icon_calendar.jpg"."'   alt='' /></a></td>";
					
					$date_split = explode(':',  date('H:i:s',strtotime($value[0])));
					$str_date_time .= "<tr  valign='top'>";
					$str_date_time .= "<td>";
					$str_date_time .= "<select id='"."h_".$name."' name='"."h_".$name."' class='styledselect-day'>";
					for($k=0;$k<=24;$k++){
						if($k<10)
							$k = '0'.$k;
						$str_date_time.= "<option value='" . $k . "' ". ($k == $date_split[0]? "selected = 'selected'" :"" ) .">" . $k . "</option>";
					}
					$str_date_time .= "</select></td>";
					$str_date_time .= "<td><div style='margin-top:5px;margin-right:5px;'>Giờ<div></td>";
					$str_date_time .= "<td><select id='"."p_".$name."' name='"."p_".$name."' class='styledselect-month'>";
					$str_date_time.= "<option value='0'". (0 == $date_split[1]? "selected = 'selected'" :"" ) .">00</option>";
					$str_date_time.= "<option value='15'". (15 == $date_split[1]? "selected = 'selected'" :"" ) .">15</option>";
					$str_date_time.= "<option value='30'". (30 == $date_split[1]? "selected = 'selected'" :"" ) .">30</option>";
					$str_date_time.= "<option value='45'". (45 == $date_split[1]? "selected = 'selected'" :"" ) .">45</option>";
					$str_date_time .= "</select></td>";
					$str_date_time .= "<td><div style='margin-top:5px;'>Phút<div></td>";
					$str_date_time .= "</tr>";
					$str_date_time .= "</table>";
					return $str_date_time;
					break;		
				case 'maps':
					$map_split = explode(',', $value[0]);
					if($map_split[0] && $map_split[0] > 0) {
						$long = $map_split[1];
						$lat = $map_split[0];
					}else {
						$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode('Hà Nội, Việt Nam').'&sensor=false');
						$output= json_decode($geocode);
						$lat = $output->results[0]->geometry->location->lat;
						$long = $output->results[0]->geometry->location->lng;
					}
					require('GoogleMapAPIv3.class.php');
				 	$gmap = new GoogleMapAPI();
					$gmap->setCenter($lat.','.$long);
					$gmap->setSize('700px','450px');
					$gmap->setZoom(14);
					$gmap->setLang('en');
					$coordtab = array();
					$coordtab[]= array("$lat","$long","","");
					$gmap->addArrayMarkerByCoords($coordtab,'');
					$gmap->generate();
	                echo $gmap->getGoogleMap();
					break;					
			}
			
	}
	
	function get_validate_message($validate_string){
		$string_message = "";
		$str_split = explode('|', $validate_string);
		foreach($str_split as $validate){
			$validate_split = explode(':', $validate);
			switch($validate_split[0]){
				case 'req':
					$string_message .= " Trường bắt buộc nhập. ";
					break;
				case 'min':
					$string_message .= "Phải ít nhất ".$validate_split[1]." kí tự.";
					break;
				case 'max':
					$string_message .= " Không quá ".$validate_split[1]." kí tự.";
					break;
				case 'rep':
					$string_message .= " Nhập lại ".$validate_split[1];
					break;	
			}
		}
		return $string_message;
	}
	
	function check_form($fields,$fields_validate,$data_form){
		$content  = "<script type=\"text/javascript\">\n"; 
		$content .= "$('#form_view').click(function () {\n";
		$content .= "var b = 1;\n";
		
		$str_message = "";
		$i=1;
		foreach($fields as $key=>$val){
			if($val == 'hidden') {
				continue;
			}
			$str_split = explode('|', $fields_validate[$key]);
			$content .= "var c = 1;\n";
			foreach($str_split as $validate){
				$validate_split = explode(':', $validate);
				if(isset($data_form['pass'])){
					if($val == 'password' || $val == 're_password' ){
							continue;
					}
				}
				switch($validate_split[0]){
					//Yêu cầu bắt buộc nhập đối với các trường
					case 'req':
						if($val == 'select'){
							$content .= "if ($('#$key').val() == '') {\n";
							$content .= "$('#error-inner_$i').html('Trường bắt buộc nhập.');\n";
							$content .="b = 0;\n";
							$content .="c = 0;\n";
							$content .= "}\n";
						}else{
							$content .= "if (!$('#$key').val()) {\n";
							$content .= "$('#error-inner_$i').html('Trường bắt buộc nhập.');\n";
							$content .="b = 0;\n";
							$content .="c = 0;\n";
							$content .= "}\n";
						}
						break;
					//Kiểm tra min length
					case 'min':
						$content .= "if ($('#$key').val() && $('#$key').val().length < $validate_split[1] ) {\n";
						$content .= "$('#error-inner_$i').html('Phải ít nhất $validate_split[1] kí tự');\n";
						$content .="b = 0;\n";
						$content .="c = 0;\n";
						$content .= "}\n";
						break;	
					//Kiểm tra max length
					case 'max':
						$content .= "if ($('#$key').val().length > $validate_split[1] ) {\n";
						$content .= "$('#error-inner_$i').html('Không quá $validate_split[1] kí tự');\n";
						$content .="b = 0;\n";
						$content .="c = 0;\n";
						$content .= "}\n";
						break;	
					case 'rep':
						$content .= "if ($('#password').val() != $('#re_password').val()) {\n";
						$content .= "$('#error-inner_$i').html('Mật khẩu nhập lại không đúng');\n";
						$content .="b = 0;\n";
						$content .="c = 0;\n";
						$content .= "}\n";
						break;	
					case 'image':
						$content .="var file = $('#$key').val();\n";
						$content .="var ext = file.substr( (file.lastIndexOf('.') +1) );\n";
						$content .="var ext_Array = ['jpg', 'png', 'gif','JPG','PNG','GIF'];\n";
						$content .= "if (jQuery.inArray(ext,ext_Array)== -1) {\n";
						$content .= "$('#error-inner_$i').html('Chưa chọn ảnh hoặc định dạng ảnh không đúng...');\n";
						$content .="b = 0;\n";
						$content .="c = 0;\n";
						$content .= "}\n";
						break;	
					case 'file':
						$content .="var file = $('#$key').val();\n";
						$content .="var ext = file.substr( (file.lastIndexOf('.') +1) );\n";
						$content .="var ext_Array = ['pdf', 'doc', 'docx','xls','xlsx'];\n";
						$content .= "if (jQuery.inArray(ext,ext_Array)== -1) {\n";
						$content .= "$('#error-inner_$i').html('Chưa chọn file hoặc định dạng file ảnh không đúng...');\n";
						$content .="b = 0;\n";
						$content .="c = 0;\n";
						$content .= "}\n";
						break;		
				}
				
			}
			$content .= "if (c==0) {\n";
			$content .= "$('#bubble-left_$i').attr('style','display: none'); \n";
			$content .= "$('#bubble-inner_$i').attr('style','display: none'); \n";
			$content .= "$('#bubble-right_$i').attr('style','display: none'); \n";
			$content .= "$('#error-left_$i').attr('style','display: block'); \n";
			$content .= "$('#error-inner_$i').attr('style','display: block'); \n";
			$content .= "}\n";
			$content .= "else {\n";
			$content .= "$('#bubble-left_$i').attr('style','display: none'); \n";
			$content .= "$('#bubble-inner_$i').attr('style','display: none'); \n";
			$content .= "$('#bubble-right_$i').attr('style','display: none'); \n";
			$content .= "$('#error-left_$i').attr('style','display: none'); \n";
			$content .= "$('#error-inner_$i').attr('style','display: none'); \n";
			$content .= "}\n";
			$i++;
		}
		$content .= "if (b==0) {\n";
		$content .= "return false; \n";
		$content .= "}\n";
		$content .= "});\n";
		$content .= "</script>";
		return $content;
	}
	
	
	
	function script_date($fields,$fields_validate,$data_form){
		$content  = "<script type=\"text/javascript\">\n"; 
		foreach($fields as $key=>$val){
			if($val == 'hidden' || $val =='password' || $val =='re_password' ) {
				continue;
			}
			if($val == 'date_time'){
				$content .= "$(function(){\n";	
				$content .= "$('#date_pick_$key')\n";
				$content .= ".datePicker(\n";	
				$content .= "{\n";
				$content .= "createButton:false,\n";	
				$content .= "startDate:'01/01/2005',\n";
				$content .= "endDate:'31/12/2020'\n";
				$content .= "}\n";
				$content .= ").bind(\n";	
				$content .= "'click',\n";
				$content .= "function()\n";
				$content .= "{\n";
				$content .= "updateSelects($(this).dpGetSelected()[0]);\n";
				$content .= "$(this).dpDisplay();\n";
				$content .= "return false;\n";
				$content .= "}\n";
				$content .= ").bind(\n";
				$content .= "'dateSelected',\n";
				$content .= "function(e, selectedDate, td, state){\n";
				$content .= "updateSelects(selectedDate);\n";
				$content .= "}\n";
				$content .= ").bind(\n";
				$content .= "'dpClosed',\n";
				$content .= "function(e, selected){\n";
				$content .= "updateSelects(selected[0]);\n";
				$content .= "}\n";
				$content .= ");\n";
				$content .= "var updateSelects = function (selectedDate){\n";
				$content .= "var selectedDate = new Date(selectedDate);\n";
				$content .= "$('#d_$key option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');\n";
				$content .= "$('#m_$key option[value=' + (selectedDate.getMonth()+1) + ']').attr('selected', 'selected');\n";
				$content .= "$('#y_$key option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');\n";
				$content .= "}\n";
				$content .= "$('#d_$key, #m_$key, #y_$key')\n";
				$content .= ".bind(\n";
				$content .= "'change',\n";
				$content .= "function(){\n";
				$content .= "var d = new Date(\n";
				$content .= "$('#y_$key').val(),\n";
				$content .= "$('#m_$key').val()-1,\n";
				$content .= "$('#d_$key').val()\n";
				$content .= ");\n";
				$content .= "$('#date_pick_$key').dpSetSelected(d.asString());\n";
				$content .= "}\n";
				$content .= ");\n";
				$content .= "var today = new Date();\n";
				if(isset($data_form['info'][$key]) && $data_form['info'][$key] == '0000-00-00 00:00:00'){
					$content .= "updateSelects(today.getTime());\n";
				}
				$content .= "$('#d_$key').trigger('change');\n";
				$content .= "});\n";
			}
		}
		$content .= "</script>";
		return $content;
	}
