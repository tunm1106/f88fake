<input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
<table class="footable table table-stripped" data-filter=#filter>
    <thead>
        <tr>
            <th width="30%">Image</th>
            <th width="50%">Link</th>
            <th width="20%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($banner as $img){?>
        <tr class="gradeX">
            <td><img style="width: 80%;height: auto;" src="<?php echo base_url($img->images);?>"></td>
            <td><a target="_blank" href="<?php echo $img->link;?>"><?php echo $img->link;?></a></td>
            <td>
                <?php if($img->status) {?>
                    <a title="Show" href="<?php echo base_url("administrator/banner/update_status/".$img->id.'/'.$img->status);?>">
                        <button class="btn btn-danger"><i class="fa fa-close"></i></button>
                    </a>
                <?php } else { ?>
                    <a title="Hide" href="<?php echo base_url("administrator/banner/update_status/".$img->id.'/'.$img->status);?>">
                        <button class="btn btn-primary"><i class="fa fa-check"></i></button>
                    </a>
                <?php } ?>
                <a href="<?php echo base_url().'administrator/banner/show_form/'.$img->id;?>">
                    <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                </a>

                <a href="<?php echo base_url("administrator/banner/delete/".$img->id);?>">
                    <button class="btn btn-danger"><i class="fa fa-trash "></i></button>
                </a>
            </td>
        </tr>
        
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <ul class="custom-pagination pull-right">
                    <?php echo $banner_pagination; ?>
                </ul>
            </td>
        </tr>
    </tfoot>
</table>