<link href="<?php echo base_url("css/be_css/plugins/footable/footable.core.css");?>" rel="stylesheet">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tin tức</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a href="<?php echo base_url();?>administrator/news/show_form">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <?php $this->load->view('backend/message_view');?>
                <div class="ibox-content">
                    <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
                    <table class="footable table table-stripped" data-filter=#filter>
                        <thead>
                            <tr>
                                <th width="25%">Tiêu đề</th>
                                <th width="40%">Nội dung</th>
                                <th width="15%">Ngày đăng</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($news as $new){?>
                            <tr class="gradeX">
                                <td><?php echo $new->title;?></td>
                                <td><?php echo $new->short_content;?></td>
                                <td><?php echo date('d-m-Y (h:i:s)',strtotime($new->create_time));?></td>
                                <td>
                                    <a href="<?php echo base_url().'administrator/news/show_form/'.$new->id;?>">
                                        <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                                    </a>
                                    <a href="<?php echo base_url();?>administrator/news/delete/<?php echo $new->id?>">
                                        <button class="btn btn-danger"><i class="fa fa-trash "></i></button>
                                    </a>
                                </td>
                            </tr>
                            <?php }?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="custom-pagination pull-right">
                                        <?php echo $news_pagination; ?>
                                    </ul>
                                </td>
                            </tr>
                        </tfoot>
                        <script>
                            $(document).ready(function() {
                                $('.footable').footable();
                                $(".custom-pagination a").on('click',function(){
                                    $.ajax({
                                        type: "GET",
                                        url: $(this).get(),
                                        success: function(html){
                                            $(".ibox-content").html(html);
                                        }
                                    });
                                    return false;
                                });  
                            });
                        </script>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
