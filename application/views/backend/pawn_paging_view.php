<input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
    <table class="footable table table-stripped" data-filter=#filter>
        <thead>
            <tr>
                <th width="3%">ID</th>
                <th width="10%">Khách hàng</th>
                <th width="10%">Vay</th>
                <th width="6%">Thời gian</th>
                <th width="10%">Tài sản thế chấp</th>
                <th width="6%">Hãng</th>
                <th width="5%">Năm sản xuất</th>
                <th width="15%">Mô tả</th>
                <th width="5%">Điện thoại</th>
                <th width="10%">Email</th>
                <th width="6%">Thành phố</th>
                <th width="5%">Giới tính</th>
                <th width="5%">Trạng thái</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pawns as $pawn){?>
            <tr class="gradeX">
                <td><?php echo $pawn->id;?></td>
                <td><?php echo $pawn->customer_name;?></td>
                <td><?php echo number_format($pawn->loan_money, 0, '', ',');?></td>
                <td><?php echo $pawn->loan_long.' ngày';?></td>
                <td><?php echo $pawn->collateral_name;?></td>
                <td><?php echo $pawn->collateral_trademark;?></td>
                <td><?php echo $pawn->collateral_year;?></td>
                <td><?php echo $pawn->collateral_description;?></td>
                <td><?php echo $pawn->customer_phone;?></td>
                <td><?php echo $pawn->customer_mail;?></td>
                <td><?php echo $pawn->province_name;?></td>
                <td><?php echo $pawn->sex ? 'Nam' : 'Nữ';?></td>
                <td>
                    <?php if(!$pawn->status) {?>
                        <a title="Chưa duyệt" href="<?php echo base_url();?>administrator/pawn/update_status/<?php echo $pawn->id.'/'.$pawn->status;?>">
                            <button class="btn btn-danger"><i class="fa fa-close"></i></button>
                        </a>
                    <?php } else { ?>
                        <a title="Đã duyệt" href="<?php echo base_url();?>administrator/pawn/update_status/<?php echo $pawn->id.'/'.$pawn->status;?>">
                            <button class="btn btn-primary"><i class="fa fa-check"></i></button>
                        </a>
                    <?php } ?>
                    <!-- <a href="<?php echo base_url().'administrator/pawn/show_form/'.$pawn->id;?>">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                    </a>
                    <a href="<?php echo base_url("administrator/pawn/delete/".$pawn->id);?>">
                        <button class="btn btn-danger"><i class="fa fa-trash "></i></button>
                    </a> -->
                </td>
            </tr>
            <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">
                    <ul class="custom-pagination pull-right">
                        <?php echo $pawns_pagination; ?>
                    </ul>
                </td>
            </tr>
        </tfoot>
        <script>
            $(document).ready(function() {
                $('.footable').footable();
                $(".custom-pagination a").on('click',function(){
                    $.ajax({
                        type: "GET",
                        url: $(this).get(),
                        success: function(html){
                            $(".ibox-content").html(html);
                        }
                    });
                    return false;
                });  
            });
        </script>
    </table>