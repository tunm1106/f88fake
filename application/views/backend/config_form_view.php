<link rel="stylesheet" href="<?php echo base_url();?>css/be_css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo base_url();?>css/be_css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/be_css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<script src="<?php echo base_url();?>js/be_js/plugins/summernote/summernote.min.js"></script>
<script src="<?php echo base_url();?>js/be_js/bootstrap-fileupload.js"></script>
<script src="<?php echo base_url();?>js/be_js/jquery-migrate-1.2.1.js"></script>

<div class="wrapper wrapper-content">
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cấu hình</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content m-b-sm border-bottom">
                    <?php $this->load->view('backend/message_view');?>
                    <form class="cmxform form-horizontal" enctype="multipart/form-data" action="<?php echo base_url()?>administrator/config/save" method="post">
						<div class="form-group">
							<label class="col-sm-2 control-label">Tên công ty</label>
							<div class="col-sm-6">
								<input type="hidden" name="img" id="img" value="<?php if($config) echo $config->logo;?>"/>
								<input type="hidden" name="id" id="id" value="<?php if($config) echo $config->id;?>"/>
								<input class="form-control" id="company" name="company" minlength="2" type="text" value="<?php if($config) echo $config->owner_name;?>" required/>
							</div>
						</div>

						<div class="form-group">
							<label for="cname" class="col-sm-2 control-label">Logo</label>
							<div class="controls col-sm-3">
								<div data-provides="fileupload" class="fileupload fileupload-new">
                                	<div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
                                		<?php if($config && $config->logo){?>
                                			<img src="<?php echo base_url().$config->logo;?>">
                                		<?php } else {?>
                                    		<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                                    		<?php }?>
                                    </div>
                                    <div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                    <div>
                                    	<span class="btn-file">
	                                    	<span class="btn btn-primary fileupload-new">Select image</span>
	                                        <span class="fileupload-exists btn btn-primary">Change</span>
	                                        <input type="file" class="default" name="userfile" id="userfile">
                                        </span>
                                        <span class="btn-file">
                                        	<a data-dismiss="fileupload" class="btn fileupload-exists btn-danger" href="#">Remove</a>
                                        </span>
                                    </div>
                                </div>
							</div>
						</div>

						<div class="form-group ">
                        	<label class="col-sm-2 control-label">Điện thoại</label>
                        	<div class="controls col-sm-4">
                        		<input class="form-control" id="phone" name="phone" minlength="2" type="text" value="<?php if($config) echo $config->phone;?>" required/>
                        	</div>
                        </div>

                        <div class="form-group ">
                        	<label class="col-sm-2 control-label">Email</label>
                        	<div class="controls col-sm-4">
                        		<input class="form-control" id="email" name="email" minlength="2" type="text" value="<?php if($config) echo $config->email;?>" required/>
                        	</div>
                        </div>

                        <div class="form-group ">
                        	<label class="col-sm-2 control-label">Địa chỉ</label>
                        	<div class="controls col-sm-6">
                        		<input class="form-control" id="address" name="address" minlength="2" type="text" value="<?php if($config) echo $config->address;?>" required/>
                        	</div>
                        </div>

                        <div class="form-group ">
                        	<label class="col-sm-2 control-label">Giờ làm việc</label>
                        	<div class="controls col-sm-10">
                        		<input class="form-control" id="workhourse" name="workhourse" minlength="2" type="text" value="<?php if($config) echo $config->workhourse;?>" required/>
                        	</div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">Lưu lại</button>
                                <button class="btn btn-outline btn-primary" type="button">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
        $('.summernote').summernote({
          onImageUpload: function(files, editor, $editable) {
          sendFile(files[0],editor,$editable);
          }  
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("userfile", file);
            data.append("summernote", true);
            $.ajax({
            url: "<?php echo base_url("uploader.html");?>",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                $('#summernote').summernote("insertImage", data, 'filename');
            },
           error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus+" "+errorThrown);
          }
        });
       }
	});
</script>
