<?php if($this->session->flashdata('error')){?>
<div class="alert alert-danger alert-dismissable">
	<button class="close" type="button" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $this->session->flashdata('error');?>
</div>
<?php }?>

<?php if($this->session->flashdata('success')){?>
<div class="alert alert-success alert-dismissable">
	<button class="close" type="button" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $this->session->flashdata('success');?>
</div>
<?php }?>