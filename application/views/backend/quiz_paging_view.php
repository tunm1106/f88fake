<input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
<table class="footable table table-stripped" data-filter=#filter>
    <thead>
        <tr>
            <th width="25%">Câu hỏi</th>
            <th width="40%">Nội dung</th>
            <th width="10%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($quiz as $new){?>
        <tr class="gradeX">
            <td><?php echo $new->title;?></td>
            <td><?php echo $new->content;?></td>
            <td>
                <a href="<?php echo base_url().'administrator/quiz/show_form/'.$new->id;?>">
                    <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                </a>
                <a href="<?php echo base_url();?>administrator/quiz/delete/<?php echo $new->id?>">
                    <button class="btn btn-danger"><i class="fa fa-trash "></i></button>
                </a>
            </td>
        </tr>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <ul class="custom-pagination pull-right">
                    <?php echo $quiz_pagination; ?>
                </ul>
            </td>
        </tr>
    </tfoot>
    <script>
        $(document).ready(function() {
            $('.footable').footable();
            $(".custom-pagination a").on('click',function(){
                $.ajax({
                    type: "GET",
                    url: $(this).get(),
                    success: function(html){
                        $(".ibox-content").html(html);
                    }
                });
                return false;
            });  
        });
    </script>
</table>