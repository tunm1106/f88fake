<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lozo - Login</title>
    <link href="<?php echo base_url("css/be_css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("font-awesome/css/font-awesome.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("css/be_css/animate.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("css/be_css/style.css");?>" rel="stylesheet">
</head>

<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">
                    <img class="center" alt="logo" style="width: 220px;" src="<?php echo base_url();?>images/be_images/logo.png">
                </h1>
            </div>
            <form class="m-t" role="form" action="<?php echo base_url();?>administrator/login/check" autocomplete="off" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" required="" name="username">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url("js/be_js/jquery-2.1.1.js");?>"></script>
    <script src="<?php echo base_url("js/be_js/bootstrap.min.js");?>"></script>
</body>
</html>
