<link rel="stylesheet" href="<?php echo base_url();?>css/be_css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo base_url();?>css/be_css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/be_css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<script src="<?php echo base_url();?>js/be_js/plugins/summernote/summernote.min.js"></script>
<script src="<?php echo base_url();?>js/be_js/bootstrap-fileupload.js"></script>
<script src="<?php echo base_url();?>js/be_js/jquery-migrate-1.2.1.js"></script>

<div class="wrapper wrapper-content">
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Danh mục</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a href="<?php echo base_url();?>administrator/category/show_form">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content m-b-sm border-bottom">
                    <?php $this->load->view('backend/message_view');?>
                    <form class="cmxform form-horizontal" enctype="multipart/form-data" action="<?php echo base_url()?>administrator/category/save" method="post">
						<div class="form-group">
							<label class="col-sm-2 control-label">Tên (required)</label>
							<div class="col-sm-6">
								<input type="hidden" name="id" id="id" value="<?php if($category) echo $category->id;?>"/>
								<input class="form-control" id="name" name="name" minlength="2" type="text" value="<?php if($category) echo $category->name;?>" required/>
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Loại</label>
                            <div class="col-sm-3">
                                <select class="form-control" tabindex="1" name="type" id="type">
                                    <option value="0" <?php if($category && !$category->type) echo "selected='selected'";?>>Tin tức</option>
                                    <option value="1" <?php if($category && $category->type == 1) echo "selected='selected'";?>>Page</option>
                                    <option value="2" <?php if($category && $category->type == 2) echo "selected='selected'";?>>Quiz</option>
                                </select>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">Lưu lại</button>
                                <button class="btn btn-outline btn-primary" type="button">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
        $('.summernote').summernote({
          onImageUpload: function(files, editor, $editable) {
          sendFile(files[0],editor,$editable);
          }  
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("userfile", file);
            data.append("summernote", true);
            $.ajax({
            url: "<?php echo base_url("uploader.html");?>",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                $('#summernote').summernote("insertImage", data, 'filename');
            },
           error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus+" "+errorThrown);
          }
        });
       }
	});
</script>
