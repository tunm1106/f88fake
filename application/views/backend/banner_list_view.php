<link href="<?php echo base_url("css/be_css/plugins/footable/footable.core.css");?>" rel="stylesheet">
<link href="<?php echo base_url("css/be_css/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
<link href="<?php echo base_url("css/be_css/plugins/blueimp/css/blueimp-gallery.min.css"); ?>" rel="stylesheet">
<script src="<?php echo base_url("js/be_js/plugins/dropzone/dropzone.js");?>"></script>
<script src="<?php echo base_url("js/be_js/plugins/blueimp/jquery.blueimp-gallery.min.js");?>"></script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Banner</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a href="<?php echo base_url();?>administrator/banner/show_form">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <?php $this->load->view('backend/message_view');?>
                <div class="ibox-content banner-content">
                </div>
            </div>
        </div>
    </div>
</div> 

<script>
    $(document).ready(function(){
        get_data('<?php echo base_url("administrator/banner/banner_paging/");?>');
        $(document).on('click','.custom-pagination a',function(){
            get_data($(this).get());
            return false;
        });
    });

    function get_data(url){
        $.ajax({
            type: "GET",
            url: url,
            success: function(html){
                $(".banner-content").html(html);
                $('.footable').footable();
            }
        });
    };
</script>