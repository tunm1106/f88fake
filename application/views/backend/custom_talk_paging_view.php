<input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
<table class="footable table table-stripped" data-filter=#filter>
    <thead>
        <tr>
            <th width="25%">Khách hàng</th>
            <th width="25%">Image</th>
            <th width="40%">Nội dung</th>
            <th width="10%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($custom_talk as $value){?>
        <tr class="gradeX">
            <td><?php echo $value->name;?></td>
            <td>
                <?php if($value->image){ ?>
                    <img style="width: 80%;height: auto;" src="<?php echo base_url($value->image);?>">
                <?php } ?>
            </td>
            <td><?php echo $value->content;?></td>
            <td>
                <?php if($value->status) {?>
                    <a title="Show" href="<?php echo base_url("administrator/custom_talk/update_status/".$value->id.'/'.$value->status);?>">
                        <button class="btn btn-danger"><i class="fa fa-close"></i></button>
                    </a>
                <?php } else { ?>
                    <a title="Hide" href="<?php echo base_url("administrator/custom_talk/update_status/".$value->id.'/'.$value->status);?>">
                        <button class="btn btn-primary"><i class="fa fa-check"></i></button>
                    </a>
                <?php } ?>
                <a href="<?php echo base_url().'administrator/custom_talk/show_form/'.$value->id;?>">
                    <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                </a>

                <a href="<?php echo base_url("administrator/custom_talk/delete/".$value->id);?>">
                    <button class="btn btn-danger"><i class="fa fa-trash "></i></button>
                </a>
            </td>
        </tr>
        
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <ul class="custom-pagination pull-right">
                    <?php echo $custom_talk_pagination; ?>
                </ul>
            </td>
        </tr>
    </tfoot>
</table>