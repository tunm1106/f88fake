<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Lozo - CMS</title>

    <link href="<?php echo base_url("css/be_css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("font-awesome/css/font-awesome.css");?>" rel="stylesheet">

    <!-- FooTable -->
    <link href="<?php echo base_url("css/be_css/plugins/footable/footable.core.css");?>" rel="stylesheet">

    <link href="<?php echo base_url("css/be_css/animate.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("css/be_css/style.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("css/be_css/custom.css");?>" rel="stylesheet">
    <script src="<?php echo base_url("js/be_js/jquery-2.1.1.js");?>"></script>
    <script src="<?php echo base_url("js/be_js/bootstrap.min.js");?>"></script>
    <script src="<?php echo base_url("js/be_js/plugins/metisMenu/jquery.metisMenu.js");?>"></script>
    <script src="<?php echo base_url("js/be_js/plugins/slimscroll/jquery.slimscroll.min.js");?>"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url("js/be_js/inspinia.js");?>"></script>
    <script src="<?php echo base_url("js/be_js/plugins/pace/pace.min.js");?>"></script>
</head>

<body>

    <div id="wrapper">

    <?php $this->load->view('backend/template/sidebar');?>

        <div id="page-wrapper" class="gray-bg">
            <?php $this->load->view('backend/template/top_navigator');?>
            <?php $this->load->view($main_page); ?>
            <?php $this->load->view('backend/template/footer');?>

        </div>
        </div>
    <!-- FooTable -->
    <script src="<?php echo base_url("js/be_js/plugins/footable/footable.all.min.js");?>"></script>
</body>

</html>
