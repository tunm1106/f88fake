<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <span>
                        <img style="width: 100px;" src="<?php echo base_url();?>images/be_images/logo.png">
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> <strong class="font-bold"><?php if($this->session->userdata('partner')) echo $this->session->userdata('partner'); else echo "Admin";?></strong><b class="caret"></b></span> 
                            <!-- <span class="text-muted text-xs block">Art Director <b class="caret"></b></span>  -->
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="<?php echo base_url("administrator/user/change_pass_form");?>">Change Password</a></li>
                        <li><a href="<?php echo base_url("logout.html");?>">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    <img class="center" alt="logo" style="width: 48px;" src="<?php echo base_url();?>images/be_images/logo-dark.png">
                </div>
            </li>

            <li <?php if($index == 'config') echo 'class="active"';?>>
                <a href="<?php echo base_url("administrator/config"); ?>">
                    <i class="fa fa-cogs"></i>
                    <span class="nav-label">Config</span>
                </a>
            </li>

            <li <?php if($index == 'news') echo 'class="active"';?>>
                <a><i class="fa fa-newspaper-o"></i> <span class="nav-label">Tin tức</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="<?php if($menu == 'list_news') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/news">Danh sách tin tức</a></li>
                    <li class="<?php if($menu == 'new_news') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/news/show_form">Thêm mới tin tức</a></li>
                </ul>
            </li>
            <li <?php if($index == 'category') echo 'class="active"';?>>
                <a><i class="fa fa-newspaper-o"></i> <span class="nav-label">Danh mục</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="<?php if($menu == 'list_category') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/category">Danh sách danh mục</a></li>
                    <li class="<?php if($menu == 'new_category') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/category/show_form">Thêm mới danh mục</a></li>
                </ul>
            </li>
            <li <?php if($index == 'store') echo 'class="active"';?>>
                <a><i class="fa fa-newspaper-o"></i> <span class="nav-label">Cửa hàng</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="<?php if($menu == 'list_store') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/store">Danh sách cửa hàng</a></li>
                    <li class="<?php if($menu == 'new_store') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/store/show_form">Thêm mới cửa hàng</a></li>
                </ul>
            </li>
            <li <?php if($index == 'custom_talk') echo 'class="active"';?>>
                <a><i class="fa fa-newspaper-o"></i> <span class="nav-label">Khách hàng nói về chúng tôi</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="<?php if($menu == 'list_custom_talk') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/custom_talk">Danh sách ý kiến</a></li>
                    <li class="<?php if($menu == 'new_custom_talk') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/custom_talk/show_form">Thêm mới ý kiến</a></li>
                </ul>
            </li>
            <li <?php if($index == 'quiz') echo 'class="active"';?>>
                <a><i class="fa fa-newspaper-o"></i> <span class="nav-label">Quiz</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="<?php if($menu == 'list_quiz') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/quiz">Danh sách câu hỏi</a></li>
                    <li class="<?php if($menu == 'new_quiz') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/quiz/show_form">Thêm mới câu hỏi</a></li>
                </ul>
            </li>
            <li <?php if($index == 'banner') echo 'class="active"';?>>
                <a><i class="fa fa-newspaper-o"></i> <span class="nav-label">Banner</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="<?php if($menu == 'list_banner') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/banner">Danh sách banner</a></li>
                    <li class="<?php if($menu == 'new_banner') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/banner/show_form">Thêm mới banner</a></li>
                </ul>
            </li>
            <li <?php if($index == 'pawn') echo 'class="active"';?>>
                <a><i class="fa fa-newspaper-o"></i> <span class="nav-label">Cầm đồ</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="<?php if($menu == 'list_pawn') echo 'active';?>"><a class="" href="<?php echo base_url();?>administrator/pawn">Danh sách đăng ký</a></li>
                </ul>
            </li>
        </ul>

    </div>
</nav>