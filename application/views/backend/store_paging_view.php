<input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
    <table class="footable table table-stripped" data-filter=#filter>
        <thead>
            <tr>
                <th width="25%">Cửa hàng</th>
                <th width="25%">Địa chỉ</th>
                <th width="10%">Điện thoại</th>
                <th width="15%">Giờ làm viẹc</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($stores as $store){?>
            <tr class="gradeX">
                <td><?php echo $store->name;?></td>
                <td><?php echo $store->address;?></td>
                <td><?php echo $store->phone;?></td>
                <td><?php echo $store->workhourse;?></td>
                <td>
                    <?php if($store->status) {?>
                        <a title="Show" href="<?php echo base_url();?>administrator/store/update_status/<?php echo $store->id.'/'.$store->status;?>">
                            <button class="btn btn-danger"><i class="fa fa-close"></i></button>
                        </a>
                    <?php } else { ?>
                        <a title="Hide" href="<?php echo base_url();?>administrator/store/update_status/<?php echo $store->id.'/'.$store->status;?>">
                            <button class="btn btn-primary"><i class="fa fa-check"></i></button>
                        </a>
                    <?php } ?>
                    <a href="<?php echo base_url().'administrator/store/show_form/'.$store->id;?>">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                    </a>
                    <a href="<?php echo base_url("administrator/store/delete/".$store->id);?>">
                        <button class="btn btn-danger"><i class="fa fa-trash "></i></button>
                    </a>
                </td>
            </tr>
            <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">
                    <ul class="custom-pagination pull-right">
                        <?php echo $stores_pagination; ?>
                    </ul>
                </td>
            </tr>
        </tfoot>
        <script>
            $(document).ready(function() {
                $('.footable').footable();
                $(".custom-pagination a").on('click',function(){
                    $.ajax({
                        type: "GET",
                        url: $(this).get(),
                        success: function(html){
                            $(".ibox-content").html(html);
                        }
                    });
                    return false;
                });  
            });
        </script>
    </table>