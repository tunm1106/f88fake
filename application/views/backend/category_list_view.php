<link href="<?php echo base_url("css/be_css/plugins/footable/footable.core.css");?>" rel="stylesheet">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Danh mục</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a href="<?php echo base_url();?>administrator/category/show_form">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <?php $this->load->view('backend/message_view');?>
                <div class="ibox-content">
                    <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
                    <table class="footable table table-stripped" data-filter=#filter>
                        <thead>
                            <tr>
                                <th width="25%">Tên danh mục</th>
                                <th width="25%">Loại</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($category as $cate){?>
                            <tr class="gradeX">
                                <td><?php echo $cate->name;?></td>
                                <td>
                                    <?php 
                                        switch ($cate->type) {
                                            case "1":
                                                echo "Page";
                                                break;
                                            case "2":
                                                echo "Quiz";
                                                break;
                                            default:
                                                echo "Tin tức";
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php if($cate->status) {?>
                                        <a title="Show" href="<?php echo base_url();?>administrator/category/update_status/<?php echo $cate->id.'/'.$cate->status;?>">
                                            <button class="btn btn-danger"><i class="fa fa-close"></i></button>
                                        </a>
                                    <?php } else { ?>
                                        <a title="Hide" href="<?php echo base_url();?>administrator/category/update_status/<?php echo $cate->id.'/'.$cate->status;?>">
                                            <button class="btn btn-primary"><i class="fa fa-check"></i></button>
                                        </a>
                                    <?php } ?>
                                    <a href="<?php echo base_url().'administrator/category/show_form/'.$cate->id;?>">
                                        <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                                    </a>
                                    <a href="<?php echo base_url("administrator/category/delete/".$cate->id);?>">
                                        <button class="btn btn-danger"><i class="fa fa-trash "></i></button>
                                    </a>
                                </td>
                            </tr>
                            <?php }?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="custom-pagination pull-right">
                                        <?php echo $category_pagination; ?>
                                    </ul>
                                </td>
                            </tr>
                        </tfoot>
                        <script>
                            $(document).ready(function() {
                                $('.footable').footable();
                                $(".custom-pagination a").on('click',function(){
                                    $.ajax({
                                        type: "GET",
                                        url: $(this).get(),
                                        success: function(html){
                                            $(".ibox-content").html(html);
                                        }
                                    });
                                    return false;
                                });  
                            });
                        </script>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
