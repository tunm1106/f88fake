<link href="<?php echo base_url("css/be_css/plugins/iCheck/custom.css");?>" rel="stylesheet">
<script src="<?php echo base_url("js/be_js/plugins/validate/jquery.validate.min.js");?>"></script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Change Password</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php $this->load->view('backend/message_view');?>
                    <form id="form" class="form-horizontal" method="post" action="<?php echo base_url("update-pass.html");?>">
                        <input type="hidden" class="span6" name="partner" value="<?php echo $this->session->userdata('partner');?>">
                        <div class="form-group">
                            <div class="col-sm-8">
                                 <label>Current Password *</label>
                                <input class="form-control required" type="password" name="cur_pass">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                 <label>New Password *</label>
                                <input class="form-control required" type="password" name="password" id="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                 <label>Confirm Password *</label>
                                <input class="form-control required" type="password" name="confirm" id="confirm">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-white" type="submit">Cancel</button>
                                <button class="btn btn-primary" type="submit">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#form").validate({
            errorPlacement: function (error, element)
            {
                element.before(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
   });
</script>