<div class="container-fluid" id="section-content">
    <div class="row">
        <div class="container">
            <div class="box-wrapper">
                <div class="nav_about_new">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach($list_category as $category){?>
                            <li role="presentation" class="<?php echo $category->slug == $tab ? 'active' : ''?>">
                                <a href="<?php echo base_url("tin-tuc/".$category->slug.".html");?>"><?php echo $category->name;?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="body_about_new body_news">
                    <!-- Tab panes -->
                    <div class="row">
                        <div class="col-md-8 content_news_left">
                            <div class="box-details-news">
                                <figure>
                                    <img src="<?php echo base_url($news->image); ?>" alt="<?php echo $news->title; ?>">
                                </figure>
                                <h1><?php echo $news->title; ?></h1>
                                <div class="date_details">
                                    <span class="text-left"><?php echo date('d/m/Y h:i:s',strtotime($news->create_time)); ?></span>
                                    <span class="tag pull-right">#F88</span>
                                </div>
                                <div class="details-content">
                                    <?php echo $news->content; ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4 sidebar_news_right">
                            <div class="sidebar-top">
                                <div class="box">
                                    <div class="box-header">
                                        Bạn cần tiền ?
                                    </div>
                                    <div class="box-content">
                                        <div class="title">Hãy đăng ký cầm đồ online F88 sẽ gọi ngay cho bạn để hỗ trợ nhanh nhất.</div>
                                        <a href="<?php echo base_url('cam-do.html'); ?>" class="btn-camdo"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-bottom sidebar-bottom-2">
                                <?php $this->load->view('frontend/news_talk_to_us_view'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>