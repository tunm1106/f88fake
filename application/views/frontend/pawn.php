<script type="text/javascript" src="<?php echo base_url('js/PawnJS/jquery.filer.min.js'); ?>"></script>
<link href="<?php echo base_url('css/PawnJS/jquery.steps.css')?>"></link>
<link href="<?php echo base_url('css/PawnJS/jquery-ui.css')?>"></link>
<script type="text/javascript" src="<?php echo base_url('js/PawnJS/jquery.steps.min.js'); ?>"></script>
<link href="<?php echo base_url('css/PawnJS/jquery.filer-dragdropbox-theme.css')?>"></link>
<link href="<?php echo base_url('css/PawnJS/jquery.filer.css')?>"></link>
<link href="<?php echo base_url('css/PawnJS/ModifyStyle.css')?>"></link>
<script src="<?php echo base_url('js/jquery.validate.min.js')?>"></script>

<link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('css/popup.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('css/PawnJS/jquery.filer.css'); ?>" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url('css/PawnJS/jquery.filer-dragdropbox-theme.css'); ?>" type="text/css" rel="stylesheet" />
<!--owl carousel css-->
<link rel="stylesheet" href="<?php echo base_url('css/PawnJS/owl.carousel.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/PawnJS/owl.theme.css'); ?>" />
<!-- jquery-ui -->
<link href="<?php echo base_url('css/PawnJS/jquery-ui.css'); ?>" rel="stylesheet" />
<script src="<?php echo base_url('js/jquery-1.10.2.min.js'); ?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>

<script src="<?php echo base_url('js/PawnJS/jquery.filer.min.js'); ?>"></script>

<link href="<?php echo base_url('css/PawnJS/jquery.steps.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('css/PawnJS/jquery-ui.css'); ?>" rel="stylesheet" />
<script src="<?php echo base_url('js/PawnJS/jquery.steps.min.js'); ?>"></script>

<link href="<?php echo base_url('css/PawnJS/jquery.filer-dragdropbox-theme.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('css/PawnJS/jquery.filer.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('css/PawnJS/jquery-filer.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('css/PawnJS/ModifyStyle.css'); ?>" rel="stylesheet" />
<script src="<?php echo base_url('js/jquery.validate.min.js'); ?>"></script>

<div class="container-fluid" id="section-content">
    <div class="row">
        <div class="container">
            <div class="regis_credit_onlne" role="tablist">
                <form id="formInfo" action="#" class="nav nav-tabs" role="tablist">
                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="col-xs-12"><span class="span_name">Thiết lập khoản vay cầm đồ</span></a>

                    <div class="content_reg_credict" id="Product">
                        <div class="heading_credict">
                            <span>01. Thiết lập khoản vay cầm đồ <h8>(Bạn vui lòng nhập đầy đủ thông tin để chúng tôi có thể hỗ trợ bạn tốt nhất)</h8> </span>
                        </div>

                        <div class="body_credict">

                            <div class="row margin-bottom-20">
                                <div class="col-md-3">
                                    <p>Bạn muốn vay bao nhiêu ?</p>
                                </div>
                                <div class="col-md-6">
                                    <div id="slider-range-max" class="slider-range-max"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group ipt_process">
                                        <input type="text" id="ID_TotalMoney" class="form-control  " value="1000000">
                                        <div class="input-group-addon">VNĐ</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row margin-bottom-20">
                                <div class="col-md-3">
                                    <p>Bạn muốn vay bao lâu ?</p>
                                </div>
                                <div class="col-md-6">
                                    <div id="slider-range-date" class="slider-range-date"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group ipt_process">
                                        <input type="text" id="ID_TotalDays" class="form-control  " value="10">
                                        <div class="input-group-addon">Ngày</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-bottom-20 custom_form">
                                <div class="col-md-3">
                                    <p>Tài sản thế chấp</p>
                                    <select class="form-control" id="ID_Asset">
                                        <?php foreach ($collaterals as $value) {?>
                                            <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <p>Thương hiệu</p>
                                            <input type="text" class="form-control" id="ID_Trademark" name="Trademark" class="required" class="required">
                                        </div>
                                        <div class="col-md-4">
                                            <p>Năm sản xuất</p>
                                            <select class="form-control" id="ID_Year" name="Year"></select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row margin-bottom-20">
                                <div class="box-process" style="display: none">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div>
                                                <p class="lead color_dark_green"><strong>Lãi suất</strong> </p>
                                            </div>
                                            <div>
                                                <div class="input-group ipt_process">
                                                    <input type="text" class="form-control" id="ID_Interest" value="6" readonly="true" />
                                                    <div class="input-group-addon">%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-md-offset-1">
                                            <div>
                                                <p class="lead color_dark_green"><strong>Tổng lãi</strong> </p>
                                            </div>
                                            <div>
                                                <div class="input-group ipt_process">
                                                    <input type="text" class="form-control" id="ID_TotalInterest" value="60,000" readonly="true" />
                                                    <div class="input-group-addon">VNĐ</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-md-offset-1">
                                            <div>
                                                <p class="lead color_dark_green"><strong>Tổng tiền phải trả</strong> </p>
                                            </div>
                                            <div>
                                                <div class="input-group ipt_process">
                                                    <input type="text" class="form-control" id="ID_TotalMoneyInterest" value="1,060,000" readonly="true" />
                                                    <div class="input-group-addon">VNĐ</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <p>Mô tả tài sản cầm cố:</p>
                                    <textarea class="form-control" rows="4" id="ID_Description"></textarea>
                                </div>
                            </div>

                            <div class="row margin-bottom-20">
                                <div class="col-md-3">
                                    <input type="button" class="btn btn-submit-custom form-control" id="btnAppcet" value="Xác nhận">
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                        <span class="span_name">Thông tin khách hàng</span>

                    </a>

                    <div class="content_reg_credict needRemove" id="CustomerInfor" style="display: none">
                        <div class="heading_credict">
                            <span>02. Thông tin khách hàng <h8>(Cố lên bạn còn nốt bước này thôi sẽ hoàn thành đăng ký vay rồi)</h8></span>
                        </div>

                        <div class="body_credict">

                            <div class="row margin-bottom-20">
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <p>Tên khách hàng</p>
                                            <input type="text" class="form-control required" id="CustomerName" name="CustomerName">
                                        </div>
                                        <div class="col-md-3">
                                            <p>Giới tính</p>
                                            <select class="form-control" id="ID_Gender">
                                                <option value="1">Nam</option>
                                                <option value="0">Nữ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group" style="display: none">
                                <label class="col-sm-2 control-label no-padding">Hộ khẩu thường trú</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="ID_HouseHold" name="HouseHold" value="Hộ khẩu thường trú" />
                                </div>
                            </div>
                            <div class="form-group" style="display: none">
                                <label class="col-sm-2 control-label no-padding">Số CMND</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control mt_20" id="ID_Identity" name="Identity" value="999999999" />
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control mt_20" placeholder="01-01-1970" id="ID_IdentityDate" name="IdentityDate" value="01-01-1970" />
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control mt_20" placeholder="Nơi cấp" id="ID_IdentityPlace" name="IdentityPlace" value="Nơi cấp" />
                                </div>
                            </div>
                            <div class="row margin-bottom-20">
                                <div class="col-md-9">
                                    <p>Chọn tỉnh thành</p>
                                    <select class="form-control required" id="ID_Province" name="Province">
                                        <option value="">--Chọn--</option>
                                        <?php foreach ($provinces as $value) {?>
                                            <option value="<?php echo $value->code; ?>"><?php echo $value->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>

                            <div class="row margin-bottom-20">
                                <div class="col-md-9">
                                    <p>Số di động</p>
                                    <input type="text" class="form-control" id="ID_Mobile" name="Mobile" class="required">
                                </div>

                            </div>
                            <div class="row margin-bottom-20">
                                <div class="col-md-9">
                                    <p>Địa chỉ mail</p>
                                    <input type="text" class="form-control" id="ID_Email" name="Email" class="required">
                                </div>

                            </div>

                            <div class="row margin-bottom-20">
                                <div class="col-md-3">
                                    <input type="button" class="btn btn-submit-custom form-control" id="btnSendRequire" value="Gửi cho chúng tôi">
                                </div>
                            </div>

                        </div>
                    </div>

                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="col-xs-12"><span class="span_name">Hoàn thành khoản vay cầm đồ</span></a>

                    <div class="content_reg_credict needRemove" id="Message" style="display: none">
                        <div class="heading_credict">
                            <span>03. Gửi thiết lập </span>
                        </div>

                        <div class="body_credict">
                            <div class="row margin-bottom-20">
                                <div class="col-md-12">
                                    <div class="box_alert">
                                        <span class="icon_success">
                                            <img src="<?php echo base_url('images/');?>/index/icon/check_success.png" alt="">
                                        </span>
                                        <h4 class="text-center color_success">Hoàn tất thiết lập khoản vay</h4>
                                        <p class="text-center content-notify">
                                            <span>Chúng tôi sẽ liên lạc với bạn thông qua </span>
                                            <span class="color_success">Email và số điện thoại</span>
                                            <span>sau</span>
                                            <span class="color_success">30 phút</span>
                                            <span>(Trong giờ hành chính) để hỗ trợ bạn về khoản vay</span>
                                        </p>
                                        <div class=" margin-bottom-20">
                                            <div class=" btn-submit-auto">
                                                <a href="<?php echo base_url() ?>" class="btn btn-submit-custom  form-control" style="padding-top: 9px;">Trở về trang chủ</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <script type="text/javascript">
                var form = $("#formInfo");
                var myStep = 0;
                $(document).ready(function() {
                    $(".needRemove").css("style", "display:block");
                    myStep = 0;
                    var c1 = form.validate({
                        rules: {
                            Trademark: {
                                required: true
                            },
                            CustomerName: {
                                required: true
                            },
                            Province: {
                                required: true
                            },
                            Mobile: {
                                required: true,
                                number: true,
                                minlength: 10,
                                maxlength: 11
                            },
                            Email: {
                                required: true,
                                email: true
                            }

                        },
                        messages: {
                            Trademark: {
                                required: "Nhập vào thương hiệu sản phẩm!"
                            },
                            CustomerName: "Nhập vào tên khách hàng!",
                            Province: "Chọn một tỉnh thành!",
                            Address: "Nhập vào địa chỉ của bạn!",
                            Mobile: "Nhập vào số điện thoại của bạn!",
                            Email: "Nhập vào email của bạn!"
                        }
                    });

                    var myFormStep = form.steps({
                        enablePagination: false,
                        //stepsContainerTag:"input",
                        headerTag: "a",
                        titleTemplate: "<span class=\"stt_span\">0#index#</span> #title#",
                        bodyTag: "div",
                        forceMoveForward: false,
                        transitionEffect: "slideLeft",
                        onStepChanging: function(event, currentIndex, newIndex) {

                            if (newIndex < currentIndex) {
                                return false;
                            } else {
                                form.validate().settings.ignore = ":disabled,:hidden";
                                return form.valid();
                            }
                        },
                        //onFinished:{}
                        onFinishing: function(event, currentIndex) {

                            form.validate().settings.ignore = ":disabled";

                            return form.valid();
                        },
                        labels: {
                            cancel: "Huỷ",
                            current: "Xác nhận:",
                            pagination: "Phân trang",
                            finish: "Xong",
                            next: "Gửi cho chúng tôi",
                            previous: "Trước",
                            loading: "Đang khởi động ..."
                        }
                    });

                    $("#btnAppcet").click(function() {
                        $("#formInfo-p-0").removeClass("activeFm");
                        $("#formInfo-p-1").addClass("activeFm");
                        myStep++;
                        if (myFormStep.steps("next") == true) {
                            // ga('send', 'event', 'f88.vn', 'Formsubmit', 'ThietLap', 0);
                        }

                    });
                    $("#btnSendRequire").click(function() {
                        myStep++;
                        $("#formInfo-p-1").removeClass("activeFm");
                        $("#formInfo-p-2").addClass("activeFm");
                        SendInfo();
                        if (myFormStep.steps("next") == true) {
                            // ga('send', 'event', 'f88.vn', 'Formsubmit', 'TTKH', 0);
                        }

                    });
                    $("#ID_Year").empty();
                    for (var year = 2006; year <= new Date().getFullYear(); year++) {
                        var option = $("<option>").attr("value", year).text(year);

                        $("#ID_Year").append(option);
                    }
                });
            </script>
            <script type="text/javascript" src="<?php echo base_url('js/PawnJS/jquery.mousewheel-3.0.6.pack.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url('js/PawnJS/myscript.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url('js/PawnJS/custom.js'); ?>"></script>
            <script type="text/javascript">
                function Calc() {

                    var TotalInterest = 0;
                    var TotalMoneyInterest = 0;
                    var TotalMoney = $("#ID_TotalMoney").val().replace(/,/g, "") * 1;
                    var TotalDays = $("#ID_TotalDays").val().replace(/,/g, "");
                    var Interest = $("#ID_Interest").val().replace(/,/g, "");

                    TotalInterest = TotalMoney * (Interest / 100) * (TotalDays / 30);
                    TotalMoneyInterest = TotalMoney + TotalInterest;

                    $("#ID_TotalInterest").val(TotalInterest);
                    $("#ID_TotalInterest").formatCurrency({
                        symbol: "",
                        roundToDecimalPlace: 0
                    });
                    $("#ID_TotalMoneyInterest").val(TotalMoneyInterest);
                    $("#ID_TotalMoneyInterest").formatCurrency({
                        symbol: "",
                        roundToDecimalPlace: 0
                    });
                }

                function SendInfo() {
                    var result = form.valid();
                    if (result == true) {
                        if (myStep == 2) {
                            $(this).off("click");
                            var strIdentityDate = '';
                            $.ajax({
                                type: "POST",
                                url: "send-info.html",
                                data: {
                                    CustomerName: $("#CustomerName").val(),
                                    Gender: $("#ID_Gender").val(),
                                    Province: $("#ID_Province").val(),
                                    Mobile: $("#ID_Mobile").val(),
                                    Email: $("#ID_Email").val(),
                                    Marital: $("#ID_Marital").val(),
                                    Asset: $("#ID_Asset").val(),
                                    Trademark: $("#ID_Trademark").val(),
                                    ProductionYear: $("#ID_Year").val(),
                                    Description: $("#ID_Description").val(),
                                    Money: $("#ID_TotalMoney").val().replace(/,/g, ""),
                                    Days: $("#ID_TotalDays").val(),
                                    Interest: $("#ID_Interest").val(),
                                    TotalInterest: $("#ID_TotalInterest").val().replace(/,/g, ""),
                                    TotalMoney: $("#ID_TotalMoneyInterest").val().replace(/,/g, ""),
                                },
                                success: function(data) {
                                }
                            });
                        }
                    }
                }
            </script>
            <script type="text/javascript" src="<?php echo base_url('js/PawnJS/jquery-ui.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url('js/PawnJS/jquery.formatCurrency-1.4.0.min.js'); ?>"></script>
            <script>
                $(function() {
                    $("#ID_TotalMoney").formatCurrency({
                        symbol: "",
                        roundToDecimalPlace: 0
                    });
                    $("#ID_TotalMoney").on("keyup", function() {
                        $("#ID_TotalMoney").formatCurrency({
                            symbol: "",
                            roundToDecimalPlace: 0
                        });
                    });
                    $("#ID_TotalDays").formatCurrency({
                        symbol: "",
                        roundToDecimalPlace: 0
                    });

                    $("#slider-range-max").slider({
                        range: "max",
                        min: 1000000,
                        max: 80000000,
                        step: 1000000,
                        value: 1000000,
                        slide: function(event, ui) {
                            $("#ID_TotalMoney").val(ui.value);
                            $("#ID_TotalMoney").formatCurrency({
                                symbol: "",
                                roundToDecimalPlace: 0
                            });
                            Calc();
                        }
                    });
                    $("#ID_TotalMoney").on("blur", function() {
                        var TotalMoney = $("#ID_TotalMoney").val().replace(/,/g, '');
                        if (isNaN(TotalMoney)) {
                            TotalMoney = 1000000;
                        } else {
                            if (TotalMoney < 1000000) {
                                TotalMoney = 1000000;
                            } else if (TotalMoney > 80000000) {
                                TotalMoney = 100000000;
                            } else {
                                TotalMoney = parseInt(TotalMoney / 1000000) * 1000000;
                            }
                        }
                        $("#ID_TotalMoney").val(TotalMoney);
                        $("#ID_TotalMoney").formatCurrency({
                            symbol: "",
                            roundToDecimalPlace: 0
                        });
                        $("#ID_Money").slider("value", TotalMoney);
                        Calc();
                    });

                    $("#slider-range-date").slider({
                        range: "max",
                        min: 10,
                        max: 70,
                        step: 5,
                        value: 10,
                        slide: function(event, ui) {
                            $("#ID_TotalDays").val(ui.value);
                            $("#ID_TotalDays").formatCurrency({
                                symbol: "",
                                roundToDecimalPlace: 0
                            });
                            Calc();
                        }
                    });
                    $("#ID_TotalDays").on("blur", function() {
                        var TotalDays = $("#ID_TotalDays").val().replace(/,/g, '');
                        if (isNaN(TotalDays)) {
                            TotalDays = 10;
                        } else {
                            if (TotalDays < 10) {
                                TotalDays = 10;
                            } else if (TotalDays > 70) {
                                TotalDays = 70;
                            } else {
                                TotalDays = parseInt(TotalDays / 5) * 5;
                            }
                        }
                        $("#ID_TotalDays").val(TotalDays);
                        $("#ID_TotalDays").formatCurrency({
                            symbol: "",
                            roundToDecimalPlace: 0
                        });
                        $("#ID_Days").slider("value", TotalDays);
                        Calc();
                    });
                });
            </script>

        </div>
    </div>
</div>