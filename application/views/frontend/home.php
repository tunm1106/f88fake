<div class="container-fluid" id="main-slider">
    <div class="row">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                <li data-target="#carousel-example-generic" data-slide-to="5"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php foreach ($banners as $key => $banner) {?>
                    <div class="item <?php echo $key == 0 ? 'active' : '' ?>">
                        <a href="<?php echo $banner->link; ?>">
                            <img src="<?php echo base_url($banner->images); ?>" />
                        </a>
                    </div>
                <?php } ?>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left glyphicon glyphicon-menu-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
<!-- End Slider Show -->
<div class="container-fluid" id="main_action_select">
    <section class="want-to fluid" style="width: auto;">
        <span class="want-to-span">Tôi muốn …</span>
        <div class="dropdown dropdown-i-want-to">
            <select class="selectpicker" id="my-dropdown">
                <option data-icon="icon--1" value="1">Đăng ký vay tiền ngay</option>
                <option data-icon="icon--2" value="2">Danh sách cửa hàng</option>
            </select>
            <script type="text/javascript">$('#my-dropdown').selectpicker('render');</script>
        </div>
        <a href="#" class="btn btn-default btn-go" id="go-btn" target="_self">Chọn</a>
    </section>
</div>

<div class="container-fluid" id="main-category">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-category-group">
                        <ul class="list-inline">
                            <li>
                                <a href="https://dinhgia.f88.vn/" class="btn-dinh-gia"></a>
                            </li>
                            <li><a href="pawn.html" class="btn-cam-do-online"></a></li>
                            <li><a href="Danh-sach-cua-hang.html" class="btn-mua-do-thanh-ly"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container box_category">
            <div class="row">
                <div class="col-md-12">
                    <div class="text_box_euro_4 text-center wow fadeIn" data-wow-duration="0s" data-wow-delay="0s">Dịch vụ tin dùng nhất của F88</div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-duration="2s" data-wow-delay="0s">
                    <div class="box_cate_2">
                        <a href="<?php echo base_url('tin-tuc.html'); ?>">
                            <div class="media_cate">
                                <img src="<?php echo base_url('images/index/img/cam-do-xe-may.png'); ?>" class="img-responsive" alt="">
                            </div>
                            <div class="box_item_euro_2_caption ">
                                <p class="text_1_caption text-center text-uppercase">cầm đồ xe máy & đăng ký</p>
                                <p class="text_2_caption text-center ">Bảo quản cẩn thận</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-duration="2s" data-wow-delay="0s">
                    <div class="box_cate_2">
                        <a href="<?php echo base_url('tin-tuc.html'); ?>">
                            <div class="media_cate">
                                <img src="<?php echo base_url('images/index/img/cam-do-o-to.png'); ?>" class="img-responsive" alt="">
                            </div>
                            <div class="box_item_euro_2_caption ">
                                <p class="text_1_caption text-center text-uppercase">cầm ô tô hoặc đăng ký</p>
                                <p class="text_2_caption text-center">Có tiền vẫn có xe đi </p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-duration="2s" data-wow-delay="0s">
                    <div class="box_cate_2">
                        <a href="<?php echo base_url('tin-tuc.html'); ?>">
                            <div class="media_cate">
                                <img src="<?php echo base_url('images/index/img/cam-do-dien-thoai-lap-top.png'); ?>" class="img-responsive" alt="">
                            </div>
                            <div class="box_item_euro_2_caption ">
                                <p class="text_1_caption text-center text-uppercase">cầm đồ điện thoại laptop</p>
                                <p class="text_2_caption text-center ">Tài sản được niêm phong</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Category -->
<!-- End List item -->
<!-- End album -->
<div class="container-fluid" id="main-certificate">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="box-heading">
                    <div class="col-md-12">
                        <h4 class="text-center">100% cửa hàng cầm đồ F88 có chứng nhận</h4>
                    </div>
                </div>
                <div class="box-content">
                    <div class="col-md-4">
                        <div class="item-certificate">
                            <div class="media">
                                <img src="<?php echo base_url('images/index/img/chungchicamdo.png'); ?>" alt="">
                            </div>
                            <div class="description">
                                <p class="text-center">Kinh doanh cầm đồ</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-certificate">
                            <div class="media">
                                <img src="<?php echo base_url('images/index/img/chungchianninh.png'); ?>" alt="">
                            </div>
                            <div class="description">
                                <p class="text-center">An ninh trật tự</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-certificate">
                            <div class="media">
                                <img src="<?php echo base_url('images/index/img/chungchiantoan.png'); ?>" alt="">
                            </div>
                            <div class="description">
                                <p class="text-center">Phòng cháy chữa cháy</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End certificate -->
<div class="container-fluid" id="main-contact">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="box-heading">
                    <div class="col-md-12">
                        <h4 class="text-center">Gọi ngay <span class="phone-f88"><?php echo $configs->phone; ?></span> để được tư vấn miễn phí</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End contact -->
<div class="container-fluid" id="main-comment">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="box-heading">
                    <div class="col-md-12">
                        <h4 class="text-center">Khách hàng đã rất hài lòng với những dịch vụ của chúng tôi</h4>
                    </div>
                </div>
                <div class="box-content">
                    <div id="carousel-customer-comment" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php if($custom_talk){ ?>
                                <?php for ($index = count($custom_talk); $index > 0; $index-=2) {?>
                                    <div class="item <?php echo $index == count($custom_talk) ? 'active' : '' ?>">
                                        <?php 
                                            $index1 = $index -1;
                                            if($index > 1) {
                                                $index2 = $index -2;
                                            }
                                        ?>
                                        <?php if($index1) {?>
                                            <div class="col-md-6">
                                                <div class="box-comment">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="media">
                                                                <img src="<?php echo base_url($custom_talk[$index1]->image ? $value->image : $configs->logo); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="article">
                                                                <div class="content_comment">
                                                                    <p><?php echo $custom_talk[$index1]->content ?></p>
                                                                </div>
                                                                <div class="footer_comment">
                                                                    <p><?php echo $custom_talk[$index1]->name ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if($index2) {?>
                                            <div class="col-md-6">
                                                <div class="box-comment">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="media">
                                                                <img src="<?php echo base_url($custom_talk[$index2]->image ? $value->image : $configs->logo); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="article">
                                                                <div class="content_comment">
                                                                    <p><?php echo $custom_talk[$index2]->content ?></p>
                                                                </div>
                                                                <div class="footer_comment">
                                                                    <p><?php echo $custom_talk[$index2]->name ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control hidden-xs hidden-sm" href="#carousel-customer-comment" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left glyphicon glyphicon-menu-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control hidden-xs hidden-sm" href="#carousel-customer-comment" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right glyphicon glyphicon-menu-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<!-- End comment -->
<div class="container-fluid" id="main-news">
    <div class="row">
        <div class="container">
            <div class="bg_owl_slider">
                <div class="box-header">
                    <span>báo chí nói về chúng tôi</span>
                </div>
                <div class="box-content">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <?php foreach ($talk_to_us as $value) {?>
                            <?php  
                                $value->origin_url ? $url = $value->origin_url : $url = base_url('tin-tuc/').$value->cate_slug.'/'.$value->slug.'.html';
                            ?>
                            <div class="item">
                                <div class="news_item">
                                    <div class="media-news">
                                        <a href="<?php echo $url; ?>" target="_blank" rel="nofollow">
                                            <img src="<?php echo base_url($value->image); ?>" height="126px" width="100%">
                                        </a>
                                    </div>
                                    <div class="news_task" style="margin-top:4px">
                                        <div class="logo_news" style="margin-top:3px">
                                            <!-- <img width="66px" height="25px" src="./images/news/logo-top.png" alt=""> -->
                                        </div>
                                        <div class="date_news">
                                            <span><?php echo date('d/m/Y h:i',strtotime($value->create_time)); ?></span>
                                        </div>
                                    </div>
                                    <div class="des_news">
                                        <p>
                                            <a href="<?php echo $url; ?>">
                                                <?php echo $value->title; ?>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="customNavigation">
                        <a class="btn prev"><i class="glyphicon glyphicon-menu-left"></i></a>
                        <a class="btn next"><i class="glyphicon glyphicon-menu-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var owl = $("#owl-demo");
        owl.owlCarousel({
            // itemsCustom: [
            //   [0, 1],
            //   [450, 2],
            //   [600, 3],
            //   [700, 3],
            //   [1000, 5],
            //   [1200, 5],
            //   [1400, 5],
            //   [1600, 5]
            // ],
            pagination: false
        });
        $(".next").click(function () {
            owl.trigger('owl.next');
        })
        $(".prev").click(function () {
            owl.trigger('owl.prev');
        })
        $('#go-btn').on('click', function () {
            var selected = $('#my-dropdown option:selected');
            switch (selected.val()) {
                case "1":
                    window.location.href = 'cam-do.html';
                    break;
                case "2":
                    window.location.href = 'danh-sach-cua-hang.html'
                    break;
            }
        });
    });
</script>