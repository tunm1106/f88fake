<div class="container-fluid" id="section-content">
    <div class="row">
        <div class="container">
            <div class="box-wrapper">
                <div class="nav_about_new">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach($list_category as $category){?>
                            <?php if(!$category->type){ ?>
                                <li role="presentation" class="<?php echo $category->slug == $tab ? 'active' : ''?>">
                                    <a href="<?php echo base_url("tin-tuc/".$category->slug.".html");?>"><?php echo $category->name;?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
                <div class="body_about_new body_news">
                    <!-- Tab panes -->
                    <div class="row">
                        <div class="col-md-8 content_news_left" id="news-data">
                            <?php if($news && count($news) >= 5) {?>
                                <div class='box-news-top'>
                                    <div class='row'>
                                        <div class='col-md-8'>
                                            <div class='news-top-langer'>
                                                <div class='media'>
                                                    <a href="<?php echo base_url('tin-tuc/').$news[0]->cate_slug.'/'.$news[0]->slug.'.html';?>">
                                                        <img src='<?php echo base_url($news[0]->image);?>' alt='<?php echo $news[0]->title?>'>
                                                    </a>
                                                </div>
                                                <div class='news-top-title'>
                                                    <h2>
                                                        <a href="<?php echo base_url('tin-tuc/').$news[0]->cate_slug.'/'.$news[0]->slug.'.html';?>"><?php echo $news[0]->title?></a>
                                                    </h2>
                                                </div>
                                                <div class='news-top-description'><?php echo $news[0]->short_content?></div>
                                            </div>
                                        </div>
                                        <div class='col-md-4'>
                                            <div class='news-top-medium'>
                                                <div class='media'>
                                                    <a href="<?php echo base_url('tin-tuc/').$news[1]->cate_slug.'/'.$news[1]->slug.'.html';?>">
                                                        <img src='<?php echo base_url($news[1]->image);?>' alt='<?php echo $news[1]->title?>'>
                                                    </a>
                                                </div>
                                                <div class='description'>
                                                    <h3>
                                                        <a href="<?php echo base_url('tin-tuc/').$news[1]->cate_slug.'/'.$news[1]->slug.'.html';?>"><?php echo $news[1]->title?></a>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class='news-top-medium'>
                                                <div class='media'>
                                                    <a href="<?php echo base_url('tin-tuc/').$news[2]->cate_slug.'/'.$news[2]->slug.'.html';?>">
                                                        <img src='<?php echo base_url($news[2]->image);?>' alt='<?php echo $news[2]->title?>'>
                                                    </a>
                                                </div>
                                                <div class='description'>
                                                    <h3>
                                                        <a href="<?php echo base_url('tin-tuc/').$news[2]->cate_slug.'/'.$news[2]->slug.'.html';?>"><?php echo $news[2]->title?></a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class='line-news-bottom'></div>
                            <?php array_splice($news, 0, 3); }?>
                            <div class='box-news-list'>
                                <?php if($news) { foreach($news as $new){?>
                                    <div class='list-items'>
                                        <div class='row'>
                                            <div class='col-md-4'>
                                                <div class='media-left-item'>
                                                    <a href="<?php echo base_url('tin-tuc/').$new->cate_slug.'/'.$new->slug.'.html';?>">
                                                        <img src='<?php echo base_url($new->image);?>' alt='<?php echo $new->title;?>'>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class='col-md-8'>
                                                <div class='title-news-item'>
                                                    <h3>
                                                        <a href="<?php echo base_url('tin-tuc/').$new->cate_slug.'/'.$new->slug.'.html';?>"><?php echo word_limiter($new->title,18)?></a>
                                                    </h3>
                                                </div>
                                                <div class='date-news-item'>
                                                    <span><?php echo date('d/m/Y h:i',strtotime($new->create_time));?></span>
                                                </div>
                                                <div class='description-news-item'><?php echo $new->short_content;?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }}?>
                            </div>
                            <div class="pagination_cumtom">
                                <nav aria-label="Page navigation">
                                    <div class="pagination-container">
                                        <ul class="pagination">
                                            <?php echo $news_pagination ;?>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="col-md-4 sidebar_news_right">
                            <div class="sidebar-top">
                                <div class="box">
                                    <div class="box-header">
                                        Bạn cần tiền ?
                                    </div>
                                    <div class="box-content">
                                        <div class="title">Hãy đăng ký cầm đồ online F88 sẽ gọi ngay cho bạn để hỗ trợ nhanh nhất.</div>
                                        <a href="<?php echo base_url('cam-do.html');?>" class="btn-camdo"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-mid">
                                <div class="box">
                                    <div class="box-header">
                                        Hỏi - đáp
                                    </div>
                                    <div class="box-content">
                                        <ul class="list-unstyled">
                                            <?php foreach($list_category as $category){?>
                                                <?php if($category->type == 2){ ?>
                                                    <li><a href="<?php echo base_url('hoi-dap.html#'.$category->slug) ?>" class="page-scroll"><?php echo $category->name; ?></a></li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-bottom">
                                <?php $this->load->view('frontend/news_talk_to_us_view'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>