<div class="container-fluid" id="section-content">
        <div class="row">
            <div class="container">
                <div class="box-wrapper">
                    <div class="nav_about_new">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#">Hỏi đáp</a></li>
                        </ul>
                    </div>
                    <div class="body_about_new body_news">
                        <!-- Tab panes -->
                        <div class="row">
                            <div class="col-md-8 content_news_left">
                                <div class="box-details-news pages-static">
                                    <?php foreach ($quiz as $key=>$value) {?>
                                        <?php $stt = $key+1; ?>
                                        <h2 class="text-uppercase" id="<?php echo $value->slug ?>"><?php echo $stt.'.'.$value->name; ?></h2>
                                        <?php foreach ($value->quiz as $quiz_value) {?>
                                            <div class="panel">
                                                <a class="panel__title collapsed" data-toggle="collapse" href="<?php echo '#'.$quiz_value->id; ?>" aria-expanded="false"><?php echo $quiz_value->title; ?></a>
                                                <div class="collapse" id="<?php echo $quiz_value->id; ?>" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel__body">
                                                        <p class="p_question"><?php echo $quiz_value->content; ?></p>
                                                        <ul></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-4 sidebar_news_right">
                                <div class="sidebar-top">
                                    <div class="box">
                                        <div class="box-header">
                                            Bạn cần tiền ?
                                        </div>
                                        <div class="box-content">
                                            <div class="title">Hãy đăng ký cầm đồ online F88 sẽ gọi ngay cho bạn để hỗ trợ nhanh nhất.</div>
                                            <a href="<?php echo base_url('cam-do.html');?>" class="btn-camdo"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="sidebar-bottom sidebar-bottom-2" style="height:660px">
                                    <?php $this->load->view('frontend/news_talk_to_us_view'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>