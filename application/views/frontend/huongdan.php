<div class="container-fluid" id="section-content">
        <div class="row">
            <div class="container">
                <div class="box-wrapper">

                    <div class="body_about_new">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="content_tab_about_new">
                                    <?php echo $content; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>