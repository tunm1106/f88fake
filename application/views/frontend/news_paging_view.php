<div class="users-list">
    <?php foreach ($blogs as $blog) :?>
        <div class="item blog-img-item">
            <div class="col-lg-3">
                <a href="<?php echo base_url().$alias.$blog->id.'/'.strtolower(url_title(loc_dau_tv($blog->title))).'.html';?>">
                    <img src="<?php echo base_url().$blog->image?>" />
                </a>
            </div>
            <div class="col-lg-9">
                <p class="title-bala"><a href="<?php echo base_url().$alias.$blog->id.'/'.strtolower(url_title(loc_dau_tv($blog->title))).'.html';?>"><?php echo $blog->title; ?></a></p>
                <?php if($blog->create_date != "" && $blog->create_date != "0000-00-00 00:00:00"):?>
                    <p class="date-created"><?php echo date('M d,Y',strtotime($blog->create_date));?></p>
                <?php endif;?>
                <p class="blog-content"><?php echo strip_tags(word_limiter($blog->short_content,60));?></p>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="blog-pagination">
        <div class="pages blogpages">
            <?php echo $news_pagination ;?>
        </div>
    </div>
</div>