<div class="container-fluid" id="section-content">
    <div class="row">
        <div class="container">
            <div class="box-wrapper">
                <div class="nav_about_new">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Giới thiệu</a></li>
                    </ul>
                </div>
                <div class="body_about_new">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="content_tab_about_new">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>