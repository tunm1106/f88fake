<div class="container-fluid" id="section-content">
    <div class="row">
        <div class="container">
            <div class="box-wrapper">

                <div class="body_about_new">
                    <!-- Tab panes -->
                    <div class="content_tab_about_new list_store_page">
                        <h1 class="text-center">
                        <span>Danh sách các cửa hàng của F88</span>
                    </h1>
                        <div class="line_br hidden-xs"></div>
                        <div id="searchResult">
                            <div class="box_sale">
                                <div class="list_sale" id="ID_FindStores">
                                    <?php foreach ($stores as $store) {?>
                                        <div class="row_item">
                                            <div class="list_item list_item_store list_item_store_1">
                                                <p>
                                                    <span class="color_green "><?php echo $store->name; ?></span>
                                                </p>
                                            </div>
                                            <div class="list_item list_item_store list_item_store_2">
                                                <p>
                                                    <span class="color_green "><i class="glyphicon glyphicon-map-marker"></i></span>
                                                    <a href="#" class="color_green"><?php echo $store->address; ?></a>
                                                </p>
                                            </div>
                                            <div class="list_item list_item_store list_item_store_3">
                                                <p><span class="color_green "><i class="glyphicon glyphicon-earphone"></i></span><?php echo $store->phone; ?></p>
                                            </div>
                                            <div class="list_item list_item_store list_item_store_4">
                                                <p><span class="color_green small"><i class="glyphicon glyphicon-time"></i></span><?php echo $store->workhourse; ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>