
<style>
</style>
<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from f88.vn/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Nov 2018 16:49:51 GMT -->
    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo base_url('images/icon.png');?>" rel="SHORTCUT ICON" />
        <title><?php echo $title ? $title : ''; ?></title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url('css/bootstrap.min.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('css/style.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('css/popup.css');?>" rel="stylesheet" />
        <!-- OWL slider -->
        <link href="<?php echo base_url('css/PawnJS/owl.carousel.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('css/PawnJS/owl.theme.css');?>" rel="stylesheet" />
        <!-- Bootstrap -->
        
        <link href="<?php echo base_url('css/bootstrap-select.min.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('css/custom.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('js/sweetalert/sweetalert.min.css')?>"></link>
        
        <script src="<?php echo base_url('js/jquery.min.js')?>"></script>
        <script src="<?php echo base_url('js/jquery-1.10.2.min.js')?>"></script>
        <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
    </head>
    <body>
        <?php $this->load->view('template/header');?>
        <!-- End Header -->
        <?php $this->load->view($main_page);?>
        <!-- End list-news -->
        <?php $this->load->view('template/footer');?>
        <!-- End footer -->
        
        <script src="<?php echo base_url('js/bootstrap-select.js')?>"></script>
        <script src="<?php echo base_url('js/conversion.js')?>"></script>
        <script src="<?php echo base_url('js/PawnJS/owl.carousel.min.js')?>"></script>
        <script src="<?php echo base_url('js/sweetalert/sweetalert.min.js')?>"></script>
        <!-- Add mousewheel plugin (this is optional) -->
        
        <script type="text/javascript">
            window.$zopim || (function (d, s) {
                var z = $zopim = function (c) { z._.push(c) }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
                            z.set.
                                _.push(o)
                        }; z._ = []; z.set._ = []; $.async = !0; $.setAttribute('charset', 'utf-8');
                $.src = 'https://v2.zopim.com/?5yJK2SVwSFdsbtARByKuG3RWi0zyHFnT'; z.t = +new Date; $.
                    type = 'text/javascript'; e.parentNode.insertBefore($, e)
            })(document, 'script');
        </script>
        <script language='javascript'>
            var f_chat_vs = "Version 2.1";
            var f_chat_domain = "index.html\/\/f88.vn";
            var f_chat_name = "Hỗ trợ trực tuyến";
            var f_chat_star_1 = "Chào bạn!";
            var f_chat_star_2 = "Chúng tôi có thể giúp gì được cho bạn không?";
            var f_chat_star_3 = "<a href='javascript:;' id='f_bt_start_chat' onclick='f_bt_start_chat()'>Bắt đầu Chat</a>";
            var f_chat_star_4 = "Chú ý: Bạn phải đăng nhập <a href='http://facebook.com/' rel='nofollow' target='_blank'>Facebook</a> mới có thể trò chuyện.";
            var f_chat_fanpage = "f88shop"; /* Đây là địa chỉ Fanpage*/
            var f_chat_background_title = "#1092da"; /* Lấy mã màu tại đây http://megapixelated.com/tags/ref_colorpicker.asp */
            var f_chat_color_title = "#fff";
            var f_chat_cr_vs = 21; /* Version ID */
            var f_chat_vitri_manhinh = "right:10px;"; /* Right: 10px; hoặc left: 10px; hoặc căn giữa left:45% */
        </script>
    </body>
</html>