<div class="container-fluid" id="main-footer">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-heading">
                        <h4>Hệ thống cửa hàng</h4>
                    </div>
                    <div class="box-content">
                        <ul class="list-unstyled list_store">
                            <li style="width: 359.333px;">
                                <a href="Danh-sach-cua-hang.html">Hà Nội</a><span class="pull-right"><a href="Danh-sach-cua-hang.html">Thanh H&oacute;a</a></span>
                            </li>
                            <li style="width: 359.333px;">
                                <a href="Danh-sach-cua-hang.html">Hải Phòng</a><span class="pull-right"><a href="Danh-sach-cua-hang.html">Bắc Giang</a></span>
                            </li>
                            <li style="width: 359.333px;">
                                <a href="Danh-sach-cua-hang.html">Vĩnh Yên</a><span class="pull-right"><a href="Danh-sach-cua-hang.html">Bắc Ninh</a></span>
                            </li>
                            <li style="width: 359.333px;">
                                <a href="Danh-sach-cua-hang.html">Hồ Chí Minh</a><span class="pull-right"><a href="Danh-sach-cua-hang.html">Tất cả cửa hàng</a></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 center-ft--2 no-right">
                    <div class="box-heading">
                        <h4>Hotline</h4>
                    </div>
                    <div class="box-content">
                        <ul class="list-unstyled">
                            <li class="numberphone-f88">
                                <span>
                                <img src="<?php echo base_url('images/index/icon/icon_phone_green.png') ?>">
                                </span><?php echo $configs->phone ?>
                            </li>
                        </ul>
                    </div>
                    <div class="box-heading-2">
                        <h4>Mạng xã hội</h4>
                    </div>
                    <div class="box-content-2">
                        <ul class="list-unstyled">
                            <li>
                                <a href="https://www.facebook.com/f88shop/">
                                <img src="./images/index/icon/icon_facebook.png" alt="">
                                </a>
                                <a href="https://www.youtube.com/channel/UCZsqvA0Ddrf_McTnIz0-cGw">
                                <img src="./images/index/icon/icon_youtube.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 right-ft--2">
                    <div class="box-heading heading--2">
                        <div class="left_ct">
                            <h4>Chứng nhận bảo mật bởi</h4>
                        </div>
                        <div class="right_ct">
                            <span class="logo_right_bot">
                            <a href="http://vsec.com.vn/" target="_blank" rel="nofollow">
                            <img src="./images/vsec.png" alt="">
                            </a>
                            </span>
                        </div>
                    </div>
                    <div class="box-heading heading--2">
                        <div class="left_ct">
                            <h4>ĐẦU TƯ BỞI MEKONG CAPITAL</h4>
                        </div>
                        <div class="right_ct">
                            <span class="logo_right_bot">
                            <a href="http://www.mekongcapital.com/vi" target="_blank" rel="nofollow">
                            <img src="./images/mekongcapital.png" alt="">
                            </a>
                            </span>
                        </div>
                    </div>
                    <div class="box-heading heading--2">
                        <div class="left_ct">
                            <h4>CUP VÀNG GIẢI THƯỞNG</h4>
                            <p> Sản phẩm tin cậy, Dịch vụ hoàn hảo. <br> Nhãn hiệu ưa dùng  </p>
                        </div>
                        <div class="right_ct">
                            <span class="logo_right_bot">
                            <a href="#" target="_blank" rel="nofollow">
                            <img src="./images/cupvang.png" alt="">
                            </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-bottom">
            <div class="container">
                <ul class="list-inline">
                    <li>
                        <a href="tuyen-dung-f88.html">
                        <span>
                        <img src="./images/index/icon/icon_tuyendung.png" alt="">
                        </span>Tuyển dụng
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <span>
                        <img src="./images/index/icon/icon_dieukhoandieukien.png" alt="">
                        </span>Điều Kiện Và Điều Khoản
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <span>
                        <img src="./images/index/icon/icon_chinhsachbaomat.png" alt="">
                        </span>Chính Sách Bảo Mật Thông Tin
                        </a>
                    </li>
                    <li>
                        <a href="cam-do/bao-quan-tai-san.html">
                        <span>
                        <img src="./images/index/icon/icon_chinhsachbaoquan.png" alt="">
                        </span>Chính Sách Bảo Quản Tài Sản
                        </a>
                    </li>
                    <li>
                        <a href="Danh-sach-cua-hang.html">
                        <span>
                        <img src="./images/index/icon/icon_danhsachcuahang.png" alt="">
                        </span>Các Cửa Hàng Của F88
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>