<div class="container-fluid" id="main-header">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-md-3  col-lg-2 wp-logo">
                    <div class="logo" style="background: url(<?php echo base_url($configs->logo)?>) no-repeat scroll left top transparent;">
                        <h1>
                            <a href="<?php echo base_url();?>" title="">Logo</a>
                        </h1>
                    </div>
                </div>
                <div class="col-sm-10 col-md-9 col-lg-10 wp-navbar">
                    <div class="navbar_top">
                        <nav class="navbar navbar-default navbar_top_custom">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand hidden" href="#">Brand</a>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class=""><a href="<?php echo base_url('about.html');?>">Giới thiệu </a></li>
                                    <li><a href="<?php echo base_url('huong-dan.html');?>">Hướng dẫn cầm đồ</a></li>
                                    <li><a href="<?php echo base_url('danh-sach-cua-hang.html');?>">Danh sách cửa hàng</a></li>
                                    <li><a href="<?php echo base_url('tin-tuc.html');?>">Tin tức</a></li>
                                    <li><a href="<?php echo base_url('hoi-dap.html');?>">Hỏi đáp</a></li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </nav>
                    </div>
                </div>
                <div class="col-lg-3 wp-numberphone hidden-xs hidden-sm hidden-md">
                    <div class="numberphonef88">
                        <span class="icon_phone"></span>
                        <span><?php echo $configs->phone ?></span>
                    </div>
                    <div class="btn_menu_top">
                        <a href="<?php echo base_url('cam-do.html');?>"><img src="<?php echo base_url("images/index/button/vaytienngay.png"); ?>" class="btn_menu_top_img" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>