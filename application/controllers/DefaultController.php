<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class DefaultController extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model( 'frontend/model_home' , 'm_home' );
		$configs = $this->m_home->get_config();
		$this->load->vars(array('configs'=>$configs));
	}
}