<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/DefaultController.php';
class Home extends DefaultController {

	function __construct() {
        parent::__construct();
        $this->load->model ( 'frontend/model_home' , 'm_home' );
        $this->load->model ( 'frontend/model_news' , 'm_news' );
	}

	public function index(){
        $data['title'] = 'Trang chủ';
        $data['banners'] = $this->m_home->get_banner();
        $data['talk_to_us'] = $this->m_news->get_talk_to_us();
        $data['custom_talk'] = $this->m_home->get_custom_talk();
        $data['main_page'] = 'frontend/home';
        $this->load->view('template/template', $data);
	}

	function about(){
        $data['title'] = 'Giới thiệu';
        $data['content'] = $this->m_home->get_content_about();
        $data['main_page'] = 'frontend/about';
        $this->load->view('template/template', $data);
	}

	function huongdan(){
        $data['title'] = 'Hướng dẫn cầm đồ';
        $data['content'] = $this->m_home->get_content_huong_dan();
        $data['main_page'] = 'frontend/huongdan';
        $this->load->view('template/template', $data);
	}

	function hoidap(){
        $data['title'] = 'Hỏi đáp';
        $data['talk_to_us'] = $this->m_news->get_talk_to_us();
        $category = $this->m_home->get_quiz_category();
        foreach ($category as $key=>$value) {
            $category[$key]->quiz = $this->m_home->get_quiz_by_cate_id($value->id);
        }
        $data['quiz'] = $category;
        $data['main_page'] = 'frontend/hoidap';
        $this->load->view('template/template', $data);
	}

	function danhsachcuahang(){
        $data['title'] = 'Danh sách cửa hàng';
        $data['stores'] = $this->m_home->get_all_store();
        $data['main_page'] = 'frontend/danhsachcuahang';
        $this->load->view('template/template', $data);
	}
}
