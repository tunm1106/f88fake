<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'controllers/DefaultController.php';
class news extends DefaultController {
	function __construct() {
		parent::__construct();
		$this->load->model ( 'frontend/model_news' , 'm_news' );
		$this->load->library("pagination");
	}
	
	function index() {
		$this->display();
	}
	
	function display($cate = ''){
		$this->input->get('page') ? $page = $this->input->get('page') : $page = 1;
		$uri_part = '';
		if(!$cate){
			$uri_part = 'tin-tuc.html';
		} else {
			$uri_part = 'tin-tuc/'.$cate.'.html';
		}
		$data['tab'] = $cate ? $cate : '';
		$data['title'] = 'Tin tức';
		$data['list_category'] = $this->m_news->get_list_category();
		$data['talk_to_us'] = $this->m_news->get_talk_to_us();
		
		$config['base_url']   = base_url().$uri_part;
		$config['total_rows'] = $this->m_news->count_all_news($cate);
		$this->pagination->initialize($config);
		$data['news_pagination'] = $this->pagination->create_links();
		//end paging
		$data['news'] = $this->m_news->get_all_news($page,$cate);
		$data['main_page'] = "frontend/news_list_view";
		$this->load->view('template/template', $data);
	}

	function detail($slug){
		$data['news'] = $this->m_news->get_news_by_slug($slug);
		if($data['news']){
			$data['title'] = 'Tin tức';
			$data['og_title'] = strip_tags($data['news']->title);
			$data['og_description'] = strip_tags($data['news']->short_content);
			$data['og_url'] = base_url('tin-tuc/').$data['news']->cate_slug.'/'.strtolower(url_title(loc_dau_tv($data['news']->title))).'.html';
			$data['og_image'] = base_url().$data['news']->image;
			$data['tab'] = $data['news']->cate_slug;
			$data['list_category'] = $this->m_news->get_list_category();
			$data['talk_to_us'] = $this->m_news->get_talk_to_us();
			$data['main_page'] = "frontend/news_detail_view";
			$this->load->view('template/template',$data);
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */