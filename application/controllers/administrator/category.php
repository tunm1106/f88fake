<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class category extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("paginationbackend", NULL,"pagination");
		$this->load->model ( 'backend/model_category' , 'm_category' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->display();
	}

	function display($page = 1){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}

		$config['base_url']   = base_url()."administrator/category/category_paging/";
		$config['total_rows'] = $this->m_category->count_all_category();
		$this->pagination->initialize($config);
		$data['category_pagination'] = $this->pagination->create_links();

		$data['index'] = 'category';
		$data['menu'] = 'list_category';
		$data['category'] = $this->m_category->get_all_category($page);
		$data['main_page'] = "backend/category_list_view";
		$this->load->view('backend/template/template', $data);
	}
	
	function category_paging($page = 0) {
		if(!$page){
			$page = 1;
		}
		
		$data['category'] = $this->m_category->get_all_category($page);
		$config['total_rows'] = $this->m_category->count_all_category();
		$config['uri_segment'] = 4;
		$config['base_url']   = base_url()."administrator/category/category_paging/";
		$this->pagination->initialize($config);
		$data['category_pagination'] = $this->pagination->create_links();
		$this->load->view('backend/category_paging_view', $data);
	}

	function show_form($id=0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		
		$data['index'] = 'category';
		$data['menu'] = 'new_category';
		$data['category'] = $this->m_category->get_category($id);
		$data['main_page']= 'backend/category_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$data = array(
			'id'=>$id,
			'name'=>$name,
			'slug'=>strtolower(url_title(loc_dau_tv($name))),
			'type'=>$type,
			'updatedate' => date('Y-m-d h:i:s',time())
		);
		if(!$id){
			$data['createdate'] = date('Y-m-d h:i:s',time());
		}
		
		$success = $this->m_category->save($data);
		if (!$success) {
			$this->session->set_flashdata ( 'error', 'Lưu danh mục không thành công, hãy kiểm tra lại.' );
			redirect('administrator/category/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu danh mục thành công!' );
			redirect('administrator/category/show_form');
		}
	}

	function delete($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if (!$this->m_category->delete_category($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa danh mục không thành công.' );
			redirect('administrator/category');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/category');
		}
	}
	
	function update_status(){
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'valid'=>$status);
		if(!$this->m_category->update_category($data)){
			$this->session->set_flashdata ( 'error', 'Publish danh mục không thành công.' );
			redirect('administrator/category');
		} else {
			if(!$value)
				$mes = 'Publish danh mục thành công!';
			else $mes = 'Unpublish danh mục thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/category');
		}
	}

}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */