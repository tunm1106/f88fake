<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class store extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("paginationbackend", NULL,"pagination");
		$this->load->model ( 'backend/model_store' , 'm_store' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->display();
	}

	function display($page = 1){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$config['base_url']   = base_url()."administrator/store/store_paging/";
		$config['total_rows'] = $this->m_store->count_all_store();
		$this->pagination->initialize($config);
		$data['stores_pagination'] = $this->pagination->create_links();

		$data['index'] = 'store';
		$data['menu'] = 'list_store';
		$data['stores'] = $this->m_store->get_all_store($page);
		$data['main_page'] = "backend/store_list_view";
		$this->load->view('backend/template/template', $data);
	}
	
	function store_paging($page = 0) {
		if(!$page){
			$page = 1;
		}
		$data['stores'] = $this->m_store->get_all_store($page);
		$config['total_rows'] = $this->m_store->count_all_store();
		$config['uri_segment'] = 4;
		$config['base_url']   = base_url()."administrator/store/store_paging/";
		$this->pagination->initialize($config);
		$data['stores_pagination'] = $this->pagination->create_links();
		$this->load->view('backend/store_paging_view', $data);
	}

	function show_form($id=0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		
		$data['index'] = 'store';
		$data['menu'] = 'new_store';
		$data['store'] = $this->m_store->get_store($id);
		$data['main_page']= 'backend/store_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$phone = $this->input->post('phone');
		$workhourse = $this->input->post('workhourse');

		$data = array(
			'id'=>$id,
			'name'=>$name,
			'address'=>$address,
			'phone'=>$phone,
			'workhourse'=>$workhourse,
		);
		
		$success = $this->m_store->save($data);
		if (!$success) {
			$this->session->set_flashdata ( 'error', 'Lưu không thành công, hãy kiểm tra lại.' );
			redirect('administrator/store/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu thành công!' );
			redirect('administrator/store/show_form');
		}
	}

	function delete($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if (!$this->m_store->delete_store($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa không thành công.' );
			redirect('administrator/store');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/store');
		}
	}
	
	function update_status(){
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'valid'=>$status);
		if(!$this->m_store->update_store($data)){
			$this->session->set_flashdata ( 'error', 'Publish không thành công.' );
			redirect('administrator/store');
		} else {
			if(!$value)
				$mes = 'Publish thành công!';
			else $mes = 'Unpublish thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/store');
		}
	}
	

}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */