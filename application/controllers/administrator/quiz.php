<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class quiz extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("paginationbackend", NULL,"pagination");
		$this->load->model ( 'backend/model_quiz' , 'm_quiz' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->display();
	}

	function display($page = 1){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}

		$config['base_url']   = base_url()."administrator/quiz/quiz_paging/";
		$config['total_rows'] = $this->m_quiz->count_all_quiz();
		$this->pagination->initialize($config);
		$data['quiz_pagination'] = $this->pagination->create_links();

		$data['index'] = 'quiz';
		$data['menu'] = 'list_quiz';
		$data['quiz'] = $this->m_quiz->get_all_quiz($page);
		$data['main_page'] = "backend/quiz_list_view";
		$this->load->view('backend/template/template', $data);
	}
	
	function quiz_paging($page = 0) {
		if(!$page){
			$page = 1;
		}
		
		$data['quiz'] = $this->m_quiz->get_all_quiz($page);
		$config['total_rows'] = $this->m_quiz->count_all_quiz();
		$config['uri_segment'] = 4;
		$config['base_url']   = base_url()."administrator/quiz/quiz_paging/";
		$this->pagination->initialize($config);
		$data['quiz_pagination'] = $this->pagination->create_links();
		$this->load->view('backend/quiz_paging_view', $data);
	}

	function show_form($id=0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$data['categories'] = $this->m_quiz->get_all_category();
		$data['index'] = 'quiz';
		$data['menu'] = 'new_quiz';
		$data['quiz'] = $this->m_quiz->get_quiz($id);
		$data['main_page']= 'backend/quiz_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$cateid = $this->input->post('category_id');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$data = array(
			'id'=>$id,
			'title'=>$title,
			'category_id'=>$cateid,
			'content'=>$content,
		);
		$success = $this->m_quiz->save($data,'quiz');
		if (!$success) {
			$this->session->set_flashdata ( 'error', 'Lưu tin tức không thành công, hãy kiểm tra lại.' );
			redirect('administrator/quiz/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu tin tức thành công!' );
			redirect('administrator/quiz/show_form');
		}
	}

	function delete($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if (!$this->m_quiz->delete_quiz($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa quiz không thành công.' );
			redirect('administrator/quiz');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/quiz');
		}
	}
	
	function update_status(){
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'valid'=>$status);
		if(!$this->m_quiz->update_quiz($data,'fmc_quiz')){
			$this->session->set_flashdata ( 'error', 'Publish quiz không thành công.' );
			redirect('administrator/quiz');
		} else {
			if(!$value)
				$mes = 'Publish quiz thành công!';
			else $mes = 'Unpublish quiz thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/quiz');
		}
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */