<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class banner extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("paginationbackend", NULL,"pagination");
		$this->load->model ( 'backend/model_banner' , 'm_banner' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->display();
	}

	function display($page = 1){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$config['base_url']   = base_url()."administrator/banner/banner_paging/";
		$config['total_rows'] = $this->m_banner->count_all_banner();
		$this->pagination->initialize($config);
		$data['banner_pagination'] = $this->pagination->create_links();

		$data['index'] = 'banner';
		$data['menu'] = 'list_banner';
		$data['banner'] = $this->m_banner->get_all_banner($page);
		$data['main_page'] = "backend/banner_list_view";
		$this->load->view('backend/template/template', $data);
	}
	
	function banner_paging($page = 0) {
		if(!$page){
			$page = 1;
		}
		$data['banner'] = $this->m_banner->get_all_banner($page);
		$config['total_rows'] = $this->m_banner->count_all_banner();
		$config['uri_segment'] = 4;
		$config['base_url']   = base_url()."administrator/banner/banner_paging/";
		$this->pagination->initialize($config);
		$data['banner_pagination'] = $this->pagination->create_links();
		$this->load->view('backend/banner_paging_view', $data);
	}

	function show_form($id=0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		
		$data['index'] = 'banner';
		$data['menu'] = 'new_banner';
		$data['banner'] = $this->m_banner->get_banner($id);
		$data['main_page']= 'backend/banner_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$link = $this->input->post('link');
		$data = array(
			'id'=>$id,
			'link'=>$link
		);
		$data['images'] = "";
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if($file_name){
			$result_arr = $this->file_upload();
			if(isset($result_arr['message'])) {
				$this->session->set_flashdata ( 'error', $result_arr['message'] );
				redirect('administrator/banner/show_form');
			}
			if(isset($result_arr['file_path'])) {
				$file_path = $result_arr['file_path'];
			}
			if($file_path){
				$data['images'] = $file_path;
			}
		} else if($this->input->post('img')) {
			$data['images'] = $this->input->post('img');
		}
		$success = $this->m_banner->save($data);
		if (!$success) {
			$this->session->set_flashdata ( 'error', 'Lưu không thành công, hãy kiểm tra lại.' );
			redirect('administrator/banner/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu thành công!' );
			redirect('administrator/banner/show_form');
		}
	}

	function file_upload(){
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if(!is_dir('./upload/banner/')){
			mkdir('./upload/banner/');
		}
		
		$config['upload_path'] = './upload/banner/';
		$config['allowed_types'] = 'jpg|png|JPG|PNG|jpeg|JPEG';
		$config['max_size']	= '100000';
		$random = rand(0, 10000000);
		$config['file_name']	= $random.time().'-'.$file_name;
		$this->load->library("upload",$config);
		
		if (!$this->upload->do_upload()){
			return array('message'=>$this->upload->display_errors('<p>', '</p>'));
		}else {
			if($this->input->post("summernote")){
				echo base_url($image_config['new_image']);
			} else {
				return array('file_path'=>$config['upload_path'].$config['file_name']);
			}
		}
	}

	function delete($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if (!$this->m_banner->delete_banner($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa không thành công.' );
			redirect('administrator/banner');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/banner');
		}
	}
	
	function update_status(){
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'valid'=>$status);
		if(!$this->m_banner->update_banner($data)){
			$this->session->set_flashdata ( 'error', 'Publish không thành công.' );
			redirect('administrator/banner');
		} else {
			if(!$value)
				$mes = 'Publish thành công!';
			else $mes = 'Unpublish thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/banner');
		}
	}
	

}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */