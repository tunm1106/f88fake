<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class custom_talk extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("paginationbackend", NULL,"pagination");
		$this->load->model ( 'backend/model_custom_talk' , 'm_custom_talk' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->display();
	}

	function display($page = 1){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$config['base_url']   = base_url()."administrator/custom_talk/custom_talk_paging/";
		$config['total_rows'] = $this->m_custom_talk->count_all_custom_talk();
		$this->pagination->initialize($config);
		$data['custom_talk_pagination'] = $this->pagination->create_links();

		$data['index'] = 'custom_talk';
		$data['menu'] = 'list_custom_talk';
		$data['custom_talk'] = $this->m_custom_talk->get_all_custom_talk($page);
		$data['main_page'] = "backend/custom_talk_list_view";
		$this->load->view('backend/template/template', $data);
	}
	
	function custom_talk_paging($page = 0) {
		if(!$page){
			$page = 1;
		}
		$data['custom_talk'] = $this->m_custom_talk->get_all_custom_talk($page);
		$config['total_rows'] = $this->m_custom_talk->count_all_custom_talk();
		$config['uri_segment'] = 4;
		$config['base_url']   = base_url()."administrator/custom_talk/custom_talk_paging/";
		$this->pagination->initialize($config);
		$data['custom_talk_pagination'] = $this->pagination->create_links();
		$this->load->view('backend/custom_talk_paging_view', $data);
	}

	function show_form($id=0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		
		$data['index'] = 'custom_talk';
		$data['menu'] = 'new_custom_talk';
		$data['custom_talk'] = $this->m_custom_talk->get_custom_talk($id);
		$data['main_page']= 'backend/custom_talk_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$content = $this->input->post('content');
		$data = array(
			'id'=>$id,
			'name'=>$name,
			'content'=>$content
		);
		$data['image'] = "";
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if($file_name){
			$result_arr = $this->file_upload();
			if(isset($result_arr['message'])) {
				$this->session->set_flashdata ( 'error', $result_arr['message'] );
				redirect('administrator/custom_talk/show_form');
			}
			if(isset($result_arr['file_path'])) {
				$file_path = $result_arr['file_path'];
			}
			if($file_path){
				$data['image'] = $file_path;
			}
		} else if($this->input->post('img')) {
			$data['image'] = $this->input->post('img');
		}
		$success = $this->m_custom_talk->save($data);
		if (!$success) {
			$this->session->set_flashdata ( 'error', 'Lưu không thành công, hãy kiểm tra lại.' );
			redirect('administrator/custom_talk/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu thành công!' );
			redirect('administrator/custom_talk/show_form');
		}
	}

	function file_upload(){
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if(!is_dir('./upload/custom_talk/')){
			mkdir('./upload/custom_talk/');
		}
		
		$config['upload_path'] = './upload/custom_talk/';
		$config['allowed_types'] = 'jpg|png|JPG|PNG|jpeg|JPEG';
		$config['max_size']	= '100000';
		$random = rand(0, 10000000);
		$config['file_name']	= $random.time().'-'.$file_name;
		$this->load->library("upload",$config);
		
		if (!$this->upload->do_upload()){
			return array('message'=>$this->upload->display_errors('<p>', '</p>'));
		}else {
			if($this->input->post("summernote")){
				echo base_url($image_config['new_image']);
			} else {
				return array('file_path'=>$config['upload_path'].$config['file_name']);
			}
		}
	}

	function delete($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if (!$this->m_custom_talk->delete_custom_talk($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa không thành công.' );
			redirect('administrator/custom_talk');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/custom_talk');
		}
	}
	
	function update_status(){
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'valid'=>$status);
		if(!$this->m_custom_talk->update_custom_talk($data)){
			$this->session->set_flashdata ( 'error', 'Publish không thành công.' );
			redirect('administrator/custom_talk');
		} else {
			if(!$value)
				$mes = 'Publish thành công!';
			else $mes = 'Unpublish thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/custom_talk');
		}
	}
	

}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */