<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class news extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("paginationbackend", NULL,"pagination");
		$this->load->model ( 'backend/model_news' , 'm_news' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->display();
	}

	function display($page = 1){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}

		$config['base_url']   = base_url()."administrator/news/news_paging/";
		$config['total_rows'] = $this->m_news->count_all_news();
		$this->pagination->initialize($config);
		$data['news_pagination'] = $this->pagination->create_links();

		$data['index'] = 'news';
		$data['menu'] = 'list_news';
		$data['news'] = $this->m_news->get_all_news($page);
		$data['main_page'] = "backend/news_list_view";
		$this->load->view('backend/template/template', $data);
	}
	
	function news_paging($page = 0) {
		if(!$page){
			$page = 1;
		}
		
		$data['news'] = $this->m_news->get_all_news($page);
		$config['total_rows'] = $this->m_news->count_all_news();
		$config['uri_segment'] = 4;
		$config['base_url']   = base_url()."administrator/news/news_paging/";
		$this->pagination->initialize($config);
		$data['news_pagination'] = $this->pagination->create_links();
		$this->load->view('backend/news_paging_view', $data);
	}

	function show_form($id=0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$data['categories'] = $this->m_news->get_all_category();
		$data['index'] = 'news';
		$data['menu'] = 'new_news';
		$data['news'] = $this->m_news->get_news($id);
		$data['main_page']= 'backend/news_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$cateid = $this->input->post('category_id');
		$title = $this->input->post('title');
		$short_content = $this->input->post('short_content');
		$content = $this->input->post('content');
		$talk_to_us = $this->input->post('talk_to_us');
		$origin = $this->input->post('origin');
		$origin_url = $this->input->post('origin_url');
		$type = $this->input->post('type');
		$data = array(
			'id'=>$id,
			'title'=>$title,
			'slug'=>strtolower(url_title(loc_dau_tv($title))),
			'category_id'=>$cateid,
			'short_content'=>$short_content,
			'content'=>$content,
			'type'=>$type,
			'talk_to_us'=>$talk_to_us,
			'origin'=>$origin,
			'origin_url'=>$origin_url,
			'update_time' => date('Y-m-d h:i:s',time())
		);
		if(!$id){
			$data['create_time'] = date('Y-m-d h:i:s',time());
		}
		$data['image'] = "";
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if($file_name){
			$result_arr = $this->file_upload();
			if(isset($result_arr['message'])) {
				$this->session->set_flashdata ( 'error', $result_arr['message'] );
				redirect('administrator/news/show_form');
			}
			if(isset($result_arr['file_path'])) {
				$file_path = $result_arr['file_path'];
			}
			if($file_path){
				$data['image'] = $file_path;
			}
		} else if($this->input->post('img')) {
			$data['image'] = $this->input->post('img');
		}
		$success = $this->m_news->save($data,'news');
		if (!$success) {
			$this->session->set_flashdata ( 'error', 'Lưu tin tức không thành công, hãy kiểm tra lại.' );
			redirect('administrator/news/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu tin tức thành công!' );
			redirect('administrator/news/show_form');
		}
	}

	function file_upload(){
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if(!is_dir('upload/news/')){
			mkdir('upload/news/');
		}
		
		$config['upload_path'] = 'upload/news/';
		$config['allowed_types'] = 'jpg|png|JPG|PNG|jpeg|JPEG';
		$config['max_size']	= '100000';
		$random = rand(0, 10000000);
		$config['file_name']	= $random.time().'-'.$file_name;
		$this->load->library("upload",$config);
		
		if (!$this->upload->do_upload()){
			return array('message'=>$this->upload->display_errors('<p>', '</p>'));
		}else {
			if($this->input->post("summernote")){
				echo base_url($config['upload_path'].$config['file_name']);
			} else {
				return array('file_path'=>$config['upload_path'].$config['file_name']);
			}
		}
	}

	function delete($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if (!$this->m_news->delete_news($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa news không thành công.' );
			redirect('administrator/news');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/news');
		}
	}
	
	function update_status(){
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'valid'=>$status);
		if(!$this->m_news->update_news($data,'fmc_news')){
			$this->session->set_flashdata ( 'error', 'Publish news không thành công.' );
			redirect('administrator/news');
		} else {
			if(!$value)
				$mes = 'Publish news thành công!';
			else $mes = 'Unpublish news thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/news');
		}
	}
	
	
// 	CATEGORY
	function list_category(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$data['index'] = 'news';
		$data['menu'] = 'list_category';
		$data['categories'] = $this->m_news->get_all_category();
		$data['main_page'] = "backend/category_list_view";
		$this->load->view('backend/template/template', $data);
	}

	function show_form_category($id=0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$data['index'] = 'news';
		$data['menu'] = 'list_category';
		$data['categories'] = $this->m_news->get_category($id);
		$data['main_page']= 'backend/category_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save_category(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$data = array(
				'id'=>$id,
				'name'=>$name
		);
	
		$data['image'] = "";
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if($file_name){
			$result_arr = $this->file_upload();
			if(isset($result_arr['message'])) {
				$this->session->set_flashdata ( 'error', $result_arr['message'] );
				redirect('administrator/news/show_form_category');
			}
			if(isset($result_arr['file_path'])) {
				$file_path = $result_arr['file_path'];
			}
			if($file_path){
				$data['image'] = $file_path;
			}
		} else if($this->input->post('img')) {
			$data['image'] = $this->input->post('img');
		}
		$success = $this->m_news->save($data,'fmc_category_news');
		if (!$success) {
			$this->session->set_flashdata ( 'error', 'Lưu chủ đề không thành công, hãy kiểm tra lại.' );
			redirect('administrator/news/show_form_category');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu chủ đề thành công!' );
			redirect('administrator/news/show_form_category');
		}
	}

	function delete_category($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if (!$this->m_news->delete_category($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa không thành công.' );
			redirect('administrator/news/list_category');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/news/list_category');
		}
	}

	function update_status_category(){
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'valid'=>$status);
		if(!$this->m_news->update_news($data,'fmc_category_news')){
			$this->session->set_flashdata ( 'error', 'Publish chủ đề không thành công.' );
			redirect('administrator/news');
		} else {
			if(!$value)
				$mes = 'Publish chủ đề thành công!';
			else $mes = 'Unpublish chủ đề thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/news/list_category');
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */