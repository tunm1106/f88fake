<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class pawn extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("paginationbackend", NULL,"pagination");
		$this->load->model ( 'backend/model_pawn' , 'm_pawn' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->display();
	}

	function display($page = 1){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$config['base_url']   = base_url()."administrator/pawn/pawn_paging/";
		$config['total_rows'] = $this->m_pawn->count_all_pawn();
		$this->pagination->initialize($config);
		$data['pawns_pagination'] = $this->pagination->create_links();

		$data['index'] = 'pawn';
		$data['menu'] = 'list_pawn';
		$data['pawns'] = $this->m_pawn->get_all_pawn($page);
		$data['main_page'] = "backend/pawn_list_view";
		$this->load->view('backend/template/template', $data);
	}
	
	function pawn_paging($page = 0) {
		if(!$page){
			$page = 1;
		}
		$data['pawns'] = $this->m_pawn->get_all_pawn($page);
		$config['total_rows'] = $this->m_pawn->count_all_pawn();
		$config['uri_segment'] = 4;
		$config['base_url']   = base_url()."administrator/pawn/pawn_paging/";
		$this->pagination->initialize($config);
		$data['pawns_pagination'] = $this->pagination->create_links();
		$this->load->view('backend/pawn_paging_view', $data);
	}

	function show_form($id=0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		
		$data['index'] = 'pawn';
		$data['menu'] = 'new_pawn';
		$data['pawn'] = $this->m_pawn->get_pawn($id);
		$data['main_page']= 'backend/pawn_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$phone = $this->input->post('phone');
		$workhourse = $this->input->post('workhourse');

		$data = array(
			'id'=>$id,
			'name'=>$name,
			'address'=>$address,
			'phone'=>$phone,
			'workhourse'=>$workhourse,
		);
		
		$success = $this->m_pawn->save($data);
		if (!$success) {
			$this->session->set_flashdata ( 'error', 'Lưu không thành công, hãy kiểm tra lại.' );
			redirect('administrator/pawn/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu thành công!' );
			redirect('administrator/pawn/show_form');
		}
	}

	function delete($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if (!$this->m_pawn->delete_pawn($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa không thành công.' );
			redirect('administrator/pawn');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/pawn');
		}
	}
	
	function update_status(){
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'valid'=>$status);
		if(!$this->m_pawn->update_pawn($data)){
			$this->session->set_flashdata ( 'error', 'Publish không thành công.' );
			redirect('administrator/pawn');
		} else {
			if(!$value)
				$mes = 'Publish thành công!';
			else $mes = 'Unpublish thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/pawn');
		}
	}
	

}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */