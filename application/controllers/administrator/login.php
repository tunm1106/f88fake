<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model ( 'backend/model_home' , 'm_home' );
	}
	
	function index()
	{
		$this->load->view('backend/login_view');
	}
	function check() {
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		if($username == "admin" && $password == "admin"){
			$this->session->set_userdata('login',true);
			$this->session->set_userdata('report_sub_admin',true);
			redirect('administrator/news');
		} else {
			$this->session->set_flashdata('message', 'Thông tin đăng nhập không đúng');
			redirect('administrator/login');
		}
	}
	
	function logout() {
		$this->session->sess_destroy();
		redirect('administrator/login');
	}
	

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */