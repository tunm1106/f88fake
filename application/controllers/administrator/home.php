<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model ( 'backend/model_home' , 'm_home' );
		$this->load->library("paginationbackend", NULL,"pagination");
	}

	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$tongdangky = $this->m_home->get_all_user();
		$usersub = 0;
		$userhuy = 0;
		foreach ($tongdangky as $tong){
			if($tong->status)
				$usersub ++;
			else $userhuy ++;
		}
		$data['tongdangky'] = count($tongdangky);
		$data['tonghuy'] = $userhuy;
		$data['tongdangsub'] = $usersub;
		$data['index'] = "dashboards";
		$data['menu'] = '';
		$data['main_page'] = "backend/dashboard";
		$this->load->view('backend/template/template', $data);
	}

	

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */