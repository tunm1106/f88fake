<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class config extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("paginationbackend", NULL,"pagination");
		$this->load->model ( 'backend/model_config' , 'm_config' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->show_form();
	}

	function show_form() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$data['config'] = $this->m_config->get_config();
		
		$data['index'] = 'config';
		$data['menu'] = 'config';

		$data['main_page']= 'backend/config_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$id = $this->input->post('id');
		$company = $this->input->post('company');
		$phone = $this->input->post('phone');
		$fax = $this->input->post('fax');
		$email = $this->input->post('email');
		$address = $this->input->post('address');
		$latitude = $this->input->post('latitude');
		$longitude = $this->input->post('longitude');
		$workhourse = $this->input->post('workhourse');
		$aboutus = $this->input->post('aboutus');
		$aboutus_footer = $this->input->post('aboutus_footer');
		
		$data = array(
			'id'=>$id,
			'company'=> $company,
			'phone'=> $phone,
			'fax'=> $fax,
			'email'=> $email,
			'address'=> $address,
			'latitude'=> $latitude,
			'longitude'=> $longitude,
			'workhourse'=> $workhourse,
			'aboutus'=> $aboutus,
			'aboutus_footer'=> $aboutus_footer,
		);
		$data['images'] = "";
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if($file_name){
			$result_arr = $this->file_upload();
			if(isset($result_arr['message'])) {
				$this->session->set_flashdata ( 'error', $result_arr['message'] );
				redirect('administrator/config/show_form');
			}
			if(isset($result_arr['file_path'])) {
				$file_path = $result_arr['file_path'];
			}
			if($file_path){
				$data['images'] = $file_path;
			}
		} else if($this->input->post('img')) {
			$data['images'] = $this->input->post('img');
		}
		$newid = $this->m_config->save($data);

		if (!$newid) {
			$this->session->set_flashdata ( 'error', 'Lưu cấu hình không thành công, hãy kiểm tra lại.' );
			redirect('administrator/config/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Lưu cấu hình thành công!' );
			redirect('administrator/config/show_form/');
		}
	}

	function file_upload(){
		$file_name = url_title (loc_dau_tv($_FILES ['userfile'] ['name']), 'dash', true );
		if(!is_dir('images/fe_images/')){
			mkdir('images/fe_images/');
		}
		$this->load->library("upload",$config);
		$config['upload_path'] = 'images/fe_images/';
		$config['allowed_types'] = 'jpg|png|JPG|PNG|jpeg|JPEG';
		$config['max_size']	= '100000';
		$random = rand(0, 10000000);
		$config['file_name']	= $random.time().'-'.$file_name;
		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload()){
			return array('message'=>$this->upload->display_errors('<p>', '</p>'));
		}else {
			
			return array('file_path'=>$config['upload_path'].$config['file_name']);
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */