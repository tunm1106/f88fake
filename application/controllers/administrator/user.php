<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class user extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model ( 'backend/model_user' , 'm_user' );
	}
	
	function index() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$this->display();
	}

	function display(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$data['index'] = 'user';
		$data['menu'] = 'list_user';
		$data['users'] = $this->m_user->get_all_user();
		$data['main_page'] = "backend/user_list_view";
		$this->load->view('backend/template/template', $data);
	}

	function show_form($id = 0) {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		
		$data['index'] = 'user';
		$data['menu'] = 'new_user';
		$data['user'] = '';
		if($id){
			$data['user'] = $this->m_user->get_user_by_id($id);
		}
		$data['main_page']= 'backend/user_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}

	function change_pass_form() {
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$data['index'] = 'user';
		$data['menu'] = 'new_user';
		$data['main_page']= 'backend/changepass_form_view';
		$this->load->view ( 'backend/template/template', $data );
	}
	
	function save(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if($this->session->userdata('admin_type')){
			redirect('administrator/report');
		}
		$id = $this->input->post('id');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$data = array(
				'id'=>$id,
				'username' => $username
				);
		if(!$id){
			$data['password'] = md5($password);
			
		}
		$success = $this->m_user->save($data);
		if (! $success) {
			$this->session->set_flashdata ( 'error', 'Tài khoản đã tồn tại, hãy kiểm tra lại.' );
			redirect('administrator/user/show_form');
		} else {
			$this->session->set_flashdata ( 'success', 'Tài khoản đã được lưu thành công!' );
			redirect('administrator/user/show_form');
		}
	}
	
	function delete($id){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		if($this->session->userdata('admin_type')){
			redirect('administrator/report');
		}
		if (!$this->m_user->delete($id)) {
			$this->session->set_flashdata ( 'error', 'Xóa tài khoản không thành công, hãy kiểm tra lại.' );
			redirect('administrator/user');
		} else {
			$this->session->set_flashdata ( 'success', 'Xóa thành công!' );
			redirect('administrator/user');
		}
	}

	function update_status(){
		if($this->session->userdata('admin_type')){
			redirect('administrator/report');
		}
		$id = $this->uri->segment(4);
		$value = $this->uri->segment(5);
		$status = 0;
		if(!$value)
			$status = 1;
		$data = array('id'=>$id,'status'=>$status);
		if(!$this->m_user->update($data)){
			$this->session->set_flashdata ( 'error', 'Publish user không thành công, hãy kiểm tra lại.' );
			redirect('administrator/user');
		} else {
			if(!$value)
				$mes = 'Publish user thành công!';
			else $mes = 'Unpublish user thành công!';
			$this->session->set_flashdata ( 'success', $mes );
			redirect('administrator/user');
		}
	}
	
	function update_pass(){
		$partner = $this->input->post('partner');
		$cur_pass = $this->input->post('cur_pass');
		$new_pass = $this->input->post('password');
		$rnew_pass = $this->input->post('confirm');
		
		$user = $this->m_user->get_user_by_partner($partner);
		if($user){
			if($user->pass == $cur_pass){
				$data = array('pass'=>$new_pass);
				$this->m_user->update($data,$partner);
				$this->session->set_flashdata ( 'success', 'Đổi mật khẩu thành công' );
			} else if($user->pass != $cur_pass) {
				$this->session->set_flashdata ( 'error', 'Mật khẩu hiện tại không đúng' );
			} else {
				$this->session->set_flashdata ( 'error', 'Đổi mật khẩu không thành công' );
			}
		}
		redirect(base_url("changepass.html"));
	}
	
	function reset_pass($id = 0){
		$password = md5('123456');
		$user = $this->m_user->get_user_by_id($id);
		if($user){
			$data = array('id'=>$id, 'password'=>$password);
			$this->m_user->update($data);
			$this->session->set_flashdata ( 'success', 'Reset mật khẩu thành công' );
		} else {
			$this->session->set_flashdata ( 'error', 'Reset mật khẩu không thành công' );
		}
		redirect('administrator/user');
	}
	
	function info(){
		if(!$this->session->userdata('login')){
			redirect('administrator/login');
		}
		$data['index'] = 'user';
		$data['menu'] = 'list_user';
		$data['user'] = $this->m_user->get_user_by_id($this->session->userdata('id'));
		$data['main_page'] = "backend/user_info_view";
		$this->load->view('backend/template/template', $data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */