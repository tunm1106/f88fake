<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/DefaultController.php';
class Pawn extends DefaultController {

	function __construct() {
		parent::__construct();
		$this->load->model ( 'frontend/model_pawn' , 'm_pawn' );
	}

	public function index(){
        $data['title'] = 'Cầm đồ';
        $data['collaterals'] = $this->m_pawn->get_collaterals();
        $data['provinces'] = $this->m_pawn->get_province();
        $data['main_page'] = 'frontend/pawn';
        $this->load->view('template/template', $data);
	}
	function send_info(){
		$data = array(
			'customer_name' => $this->input->post('CustomerName'),
			'customer_phone' => $this->input->post('Mobile'),
			'customer_mail' => $this->input->post('Email'),
			'sex' => $this->input->post('Gender'),
			'loan_money' => $this->input->post('Money'),
			'loan_long' => $this->input->post('Days'),
			'collateral_id' => $this->input->post('Asset'),
			'collateral_trademark' => $this->input->post('Trademark'),
			'collateral_year' => $this->input->post('ProductionYear'),
			'collateral_description' => $this->input->post('Description'),
			'province_id'=> $this->input->post('Province'),
			'createdate' => date('Y-m-d h:i:s',time())
		);
		$this->m_pawn->save_info($data);
	}
}
