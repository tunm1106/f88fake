<?php
$lang = [
	'Admin_group'=>[
		'title'=>'Admin Roles'
	],
	'Admins'=>[
		'title'=>'Admins'
	],
	'Black_words'=>[
		'title'=>'Black words'
	],
	'Group_post'=>[
		'title'=>'Group posts'
	],
	'Groups'=>[
		'title'=>'Community'
	],
	'Home'=>[
		'title'=>'Home'
	],
	'Posts'=>[
		'title'=>'Posts'
	],
	'Reaction'=>[
		'title'=>'Reaction'
	],
	'Reports'=>[
		'title'=>'Report Post'
	],
	'Super_admin'=>[
		'title'=>'Super Admin',
		'show_admin'=>'Admins',
		'show_addAdmins'=>'Add admin',
		'show_deleted_post'=>'Deleted post',
		'show_deleted_user'=>'Deleted user',
	],
	'Tags'=>[
		'title'=>'Tags'
	],
	'Config'=>[
		'title'=>'Cài đặt'
	],
	'Users'=>[
		'title'=>'Users',
		'show_foreign'=>'Users',
		'show_add_foreign'=>'Add Users',
	],
	'Tags'=>[
		'title'=>'Tags',
		'show_tagxh'=>'Tags xu hướng',
	],
	'Report'=>[
		'title'=>'Thống kê',
		'show_reportNRU'=>'Thống kê người dùng',
	],
	'Banners'=>[
		'title'=>'Banner'
	],
	'Gift'=>[
		'title'=>'Spin',
		'show_add'=>'Thêm mới phần thưởng',
		'index' => 'Danh sách phần thưởng',
		'show_log_gif' => 'Lịch sử quay',
		'show_voucher' => 'Danh sách voucher',
	],
	'index'=>'List',
	'show_add'=>'Add new',
	'edit'=>'Edit',
	'delete'=>'Delete',
	'save'=>'Save',
	'getForSearch'=>'Search',
	'conver_slug'=>'Conver slug',
	'upload_images'=>'Upload'
];