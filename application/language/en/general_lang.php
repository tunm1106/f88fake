<?php
$lang = [
	'btn'=>[
		'addnew'=>'Add New',
		'save'=>'Save',
		'submit'=>'submit',
		'confirm'=>'Confirm',
		'cancel'=>'Cancel',
		'clear'=>'Clear',
		'back'=>'Back',
		'next'=>'Next',
		'search'=>'Search',
		'view_later'=>'View later',
		'view_detail'=>'Detail',
		'register'=>'Register',
		'view'=>'View',
		'close'=>'Close',
		'preview'=>'Preview',
		'approve'=>'Approved',
		'export_excel'=>'Export to Excel',
		'export'=>'Export',
		'apply'=>'Apply',
		'reject'=>'Reject',
		'edit'=>'Edit',
		'reject'=>'Reject',
		'report'=>'Report',
		'delete'=>'Delete',
		'download'=>'Download',
		'reload'=>'Reload'
	],
	'lbl'=>[
		'user'=>[
			'username'=>'Username',
			'password_old'=>'Old Password',
			'password'=>'Password',
			're_password'=>'Re-Password',
			'nicename'=>'Display Name',
			'fullname'=>'Full name',
			'name'=>'Name',
			'email'=>'Email',
			'phone'=>'Phone',
			'website'=>'Website',
			'admin'=>'Administrator',
			'my_profile'=>'My Profile',
			'cardid'=>'CardID',
			'profile'=>'Profile',
			'group' => 'Group'
		],
		'general'=>[
			'status'=>'Status',
			'earn'=>'Earn',
			'pay'=>'Pay',
			'date_reg'=>'Register date',
			'traffic_info'=>'Traffic',
			'title_add'=>'Add new',
			'title_edit'=>'Edit',
			'not'=>'Not',
			'title'=>'Title',
			'description' => 'Description',
			'name'=>'Name',
			'change_lang'=>'Change language',
			'lang'=>'Language',
			'waiting'=>'Waiting',
			'system'=>'System',
			'log_out'=>'Log Out',
			'price'=>'Price',
			'parent'=>'Parent',
			'infomation'=>'Infomation',
			'show'=>'Show',
			'images'=>'Images',
			'upload_image' => 'Upload Images'
		],
		'menu'=>[
			'dashboard'=>'Dashboard',
			'contact_us'=>'Contact Us',
			'term_of_service'=>'Term of Service',
			'change_password'=>'Change Password',
			'profile'=>'Profile',
			'logout'=>'Logout',
			'ord'=>'Order',
			'latest_note'=>'Latest Notes',
		],
		'paging'=>[
			'per_page'=>'records on page',
			'showing'=>'Showing',
			'from'=>'from',
			'to'=>'to',
			'of'=>'of',
			'records'=>'record',
			'first'=>'First',
			'last'=>'Last',
			'next'=>'Next',
			'prev'=>'Prev',
			'total'=>'Total',
			'all'=>'all',
			'front'=>'front',
			'back'=>'back'
		],
		'action'=>[
			'on'=>'On',
			'off'=>'Off'
		],
		'filter'=>[
			'general'=>[
				'search_name'=>'Search by Title',
				'no_data'=>'No Data',
				'search_id' => 'Search by ID'
			],
			'user'=>[
				'search_name'=>'Search by username',
				'no_data'=>'No Data',
				'search_id' => 'Search by ID'
			],
			'group'=>[
				'search_name'=>'Search by title',
				'no_data'=>'No Data',
				'search_id' => 'Search by ID'
			]

		],
		'lang'=>[
			'vi'=>'Vietnamese',
			'en'=>'English'
		]
	],
	'message'=>[
		'alert'=>[
			'error'=>'Have error',
			'ok'=>'Successful',
			'delete_ok'=>'Deletion completed',
			'delete_error'=>'Delete error',
			'insert_ok'=>'Add new succeed',
			'insert_error'=>'Add new error',
			'update_ok'=>'Update succeed',
			'update_error'=>'Update error'
		],
		'notification'=>[
			'see_all_message'=>'See all messages',
			'see_all_notification'=>'See all notifications',
			'no_message'=>"You don't have message",
			'count_message'=>'You have %s new messages',
			'no_note'=>"You don't have note",
			'count_note'=>'You have %s new note',
			'no_notification'=>"You don't have notification",
			'count_notification'=>'You have %s new notifications',
			'title_all_notification'=>'All Notifications',
			'title_all_message'=>'All Messages'
		],
		'confirm'=>[
			'delete'=>'Are you sure to delete this data ?',
			'insert'=>'Are you sure to add new this data ?',
			'update'=>"Are you sure to edit this data ?",
			'reject'=>'Are you sure to reject this data ?'
		],
		'error_code'=>[
			'msg_404' => 'Not Found',
			'msg_500' => 'Server Error'
		]
	],
	'status'=>[
		'lbl'  => 'Status',
		'st_0' => 'UnActive',
		'st_1' => 'Active',
		'st_2' => 'UnVerify',
		'st_3' => 'Blocked'
	],
	'per'=>[
		'day' => 'per day',
		'week' => 'per week',
		'month' => 'per month',
		'year' => 'per year'
	],
	'date'=>[
		'january' => 'January',
		'february' => 'February',
		'march' => 'March',
		'april' => 'April',
		'may' => 'May',
		'june' => 'June',
		'july' => 'July',
		'august' => 'August',
		'september' => 'September',
		'october' => 'October',
		'november' => 'November',
		'december' => 'December',
		'today' => 'Today',
		'yesterday' => 'Yesterday',
		'thisweek' => 'This week',
		'lastweek' => 'Last week',
		'thismonth' => 'This month',
		'lastmonth' => 'Last month',
		'thisyear' => 'This year',
		'lastyear' => 'Last year',
		'year' => 'Year',
		'month' => 'Month',
		'day' => 'Day',
		'hour' => 'Hour',
		'second' => 'Second',
		'ago' => 'ago',
		'justnow' => 'just now',
		'time' => 'Time',
		'date' => 'Date',
		'daterange' => 'Date Range',
		'search_daterange' => 'Date Range'
	],
	'page'=>[
		'title'=>[
			'list'=>'List %s',
			'add'=>'Add new %s',
			'edit'=>"Edit %s",
			'index'=>"%s"
		]
	],
	'modal'=>[
		'change_password'=>'Change Password'
	]
];

$lang['upload_userfile_not_set'] = "Unable to find a post variable called userfile.";
$lang['upload_file_exceeds_limit'] = "The uploaded file exceeds the maximum allowed size.";
$lang['upload_file_exceeds_form_limit'] = "The uploaded file exceeds the maximum size allowed by the submission form.";
$lang['upload_file_partial'] = "The file was only partially uploaded.";
$lang['upload_no_temp_directory'] = "The temporary folder is missing.";
$lang['upload_unable_to_write_file'] = "The file could not be written to disk.";
$lang['upload_stopped_by_extension'] = "The file upload was stopped by extension.";
$lang['upload_no_file_selected'] = "You did not select a file to upload.";
$lang['upload_invalid_filetype'] = "The filetype you are attempting to upload is not allowed.";
$lang['upload_invalid_filesize'] = "The file you are attempting to upload is larger than the permitted size.";
$lang['upload_invalid_dimensions'] = "The image you are attempting to upload exceedes the maximum height or width.";
$lang['upload_destination_error'] = "A problem was encountered while attempting to move the uploaded file to the final destination.";
$lang['upload_no_filepath'] = "The upload path does not appear to be valid.";
$lang['upload_no_file_types'] = "You have not specified any allowed file types.";
$lang['upload_bad_filename'] = "The file name you submitted already exists on the server.";
$lang['upload_not_writable'] = "The upload destination folder does not appear to be writable.";