<?php
$lang = [
	'Admin_group'=>[
		'title'=>'Phân quyền'
	],
	'Admins'=>[
		'title'=>'Quản trị'
	],
	'Black_words'=>[
		'title'=>'Chặn từ'
	],
	'Group_post'=>[
		'title'=>'Bài viết cộng đồng'
	],
	'Groups'=>[
		'title'=>'Cộng đồng'
	],
	'Home'=>[
		'title'=>'Trang chủ'
	],
	'Posts'=>[
		'title'=>'Bài viết'
	],
	'Reaction'=>[
		'title'=>'Reaction'
	],
	'Reports'=>[
		'title'=>'Báo cáo bài viết'
	],
	'Super_admin'=>[
		'title'=>'Super Admin',
		'show_admin'=>'Quản trị viên',
		'show_addAdmins'=>'Thêm mới quản trị viên',
		'show_deleted_post'=>'Bài viết đã xóa',
		'show_deleted_user'=>'Người dùng đã xóa',
	],
	'Tags'=>[
		'title'=>'Tags'
	],
	'Config'=>[
		'title'=>'Cài đặt'
	],
	'Users'=>[
		'title'=>'Người dùng',
		'show_foreign'=>'Người dùng',
		'show_add_foreign'=>'Thêm người dùng',
	],
	'Tags'=>[
		'title'=>'Tags',
		'show_tagxh'=>'Tags xu hướng',
	],
	'Report'=>[
		'title'=>'Thống kê',
		'show_reportNRU'=>'Thống kê người dùng',
	],
	'Banners'=>[
		'title'=>'Banner'
	],
	'Gift'=>[
		'title'=>'Spin',
		'show_add'=>'Thêm mới phần thưởng',
		'index' => 'Danh sách phần thưởng',
		'show_log_gif' => 'Lịch sử quay',
		'show_voucher' => 'Danh sách voucher',
	],
	'index'=>'Danh sách',
	'show_add'=>'Thêm mới',
	'edit'=>'Sửa',
	'delete'=>'Xóa',
	'save'=>'Lưu lại',
	'getForSearch'=>'Tìm kiếm',
	'conver_slug'=>'CV',
	'upload_images'=>'Upload'
];
