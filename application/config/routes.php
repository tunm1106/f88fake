<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$_prefix = '_controller';
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['about.html'] = 'home/about';
$route['huong-dan.html'] = 'home/huongdan';
$route['hoi-dap.html'] = 'home/hoidap';
$route['danh-sach-cua-hang.html'] = 'home/danhsachcuahang';
$route['cam-do.html'] = 'pawn';
$route['tin-tuc.html'] = 'news';
$route['tin-tuc/(:any).html'] = 'news/display/$1';
$route['tin-tuc/(:any)/(:any).html'] = 'news/detail/$2';

$route['send-info.html'] = 'pawn/send_info';

$route['administrator']                           = 'administrator/home';
